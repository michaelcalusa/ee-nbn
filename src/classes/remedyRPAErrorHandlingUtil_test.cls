@isTest
private class remedyRPAErrorHandlingUtil_test{
    
    @testSetup
    static void insertMetaData(){
        insert new RemedyIntegrationErrorHandling__c(Name = 'action', JSONKey__c = 'action', SalesforceField__c = 'action__c');
        insert new RemedyIntegrationErrorHandling__c(Name = 'incidentId', JSONKey__c = 'incidentId', SalesforceField__c = 'incidentNumber__c');
        insert new RemedyIntegrationErrorHandling__c(Name = 'roboticUserName', JSONKey__c = 'roboticUserName', SalesforceField__c = 'roboticUserName__c');
        insert new RemedyIntegrationErrorHandling__c(Name = 'rpaStatus', JSONKey__c = 'rpaStatus', SalesforceField__c = 'RPA_Status__c');
        insert new RemedyIntegrationErrorHandling__c(Name = 'salesForceUser', JSONKey__c = 'salesForceUser', SalesforceField__c = 'salesForceUser__c');
        insert new RemedyIntegrationErrorHandling__c(Name = 'transactionId', JSONKey__c = 'transactionId', SalesforceField__c = 'transactionId__c');
        insert new RemedyIntegrationErrorHandling__c(Name = 'incidentIdentifier', JSONKey__c = 'incidentIdentifier', SalesforceField__c = 'incidentNumber__c');
        //Generic incident
        insert new Incident_Management__c (Incident_Number__c='INC000006920225', recordTypeId = schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Incident Management FTTN/B').getRecordTypeId(), Appointment_Start_Date__c=system.now(),Appointment_End_Date__c=system.now(), Target_Commitment_Date__c=system.now(),Reported_Date__c=system.now());
        insert new Incident_Management__c (Incident_Number__c='INC000006920226', recordTypeId = schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Incident Management FTTN/B').getRecordTypeId(), Appointment_Start_Date__c=system.now(),Appointment_End_Date__c=system.now(), Target_Commitment_Date__c=system.now(),Reported_Date__c=system.now(),RPA_Error_Occurred__c=true);
    }
    
    static testMethod void inboundJSONTest() {
        List<RemedyOutboundIntegration__e> evtList = new List<RemedyOutboundIntegration__e>();
        evtList.add(new RemedyOutboundIntegration__e(InboundJSONMessage__c='{ "transactionId":"ff087ade-2fcc-4e34-ba77-c33e7535e3b3", "incidentId": "INC000006920225", "action": "User Assigned", "roboticUserName": "anujyadav", "salesForceUser": "Bao", "rpaStatus": "Failed", "errors": [ { "errorType":"GenericError/TaskError/TechnicalError", "errorCode": "100000", "errorMessage": "Last Modified Date of incident is greater than the date recieved from consumer", "proposedAction": "Confirm the action again in remedy", "errorTimeStamp": "15/03/2018 2:35:57 PM" } ] }'));
        evtList.add(new RemedyOutboundIntegration__e(InboundJSONMessage__c='{ "transactionId":"ff087ade-2fcc-4e34-ba77-c33e7535e3b2", "incidentId": "INC000006920226", "action": "User Assigned", "roboticUserName": "anujyadav", "salesForceUser": "Bao", "rpaStatus": "Success" }'));
        Test.startTest();
        EventBus.publish(evtList);
        Test.stopTest();
        System.AssertEquals(2,[SELECT Id FROM RemedyIntegrationHandling__c].size(),'Size Check');
    }
    
    static testMethod void outboundJSONTest() {
        Incident_Management__c inc = new Incident_Management__c();
        inc = [SELECT User_Action__c,Id, summary__c  FROM Incident_Management__c LIMIT 1];
        //inc.User_Action__c = 'dispatchTechnician';
        //inc.summary__c = 'test disp in my test';
        //update inc;
        //System.Assert([SELECT Id, transactionId__c, incidentNumber__c FROM RemedyIntegrationHandling__c].size() > 0, 'captured outbound Message');
        inc.User_Action__c = 'reassignIncident';
        update inc;
        //inc.User_Action__c = 'dispatchTechnician';
        //update inc;
        List<RemedyIntegrationHandling__c> rihList= new List<RemedyIntegrationHandling__c>([SELECT Id, transactionId__c, incidentNumber__c, Status__c FROM RemedyIntegrationHandling__c WHERE Status__c =: 'Awaiting Response' LIMIT 1]);
        for(RemedyIntegrationHandling__c r : rihList)
            r.Status__c = 'Success';
        update rihList;
    }
}