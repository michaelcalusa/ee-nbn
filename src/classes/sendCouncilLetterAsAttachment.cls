public class sendCouncilLetterAsAttachment {
    @InvocableMethod(label='Send Council Letter' description='Send Council Letter')
    public static void sendCLEmailWithAttachment(List<id> stageAppIds)
    {
    	SendDeliverableDocstoDeveloper.SendDeliverableDocstoDeveloper(stageAppIds, 'CL');
    }

}