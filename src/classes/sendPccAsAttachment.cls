public class sendPccAsAttachment {
    @InvocableMethod(label='Send PCC email' description='Send PCC email')
    public static void sendPccEmailWithAttachment(List<id> stageAppIds)
    {
    	SendDeliverableDocstoDeveloper.SendDeliverableDocstoDeveloper(stageAppIds, 'PCC');
    }

}