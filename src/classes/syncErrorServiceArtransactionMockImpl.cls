@isTest
global class syncErrorServiceArtransactionMockImpl implements WebServiceMock { 
  
    global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {

            wwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponse respElement = new wwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponse();
               
               wwwNbnComAuDmmEnterpriseCommon.ResultStatus_element ResultStatus = new  wwwNbnComAuDmmEnterpriseCommon.ResultStatus_element();
               ResultStatus.status = 'fail';
               ResultStatus.errorCode = 'Some Error Code';
               ResultStatus.errorDescription = 'Some Error Description';
               
               respElement.ResultStatus = ResultStatus;
               system.debug(respElement);
               
              // Map<String, wwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponse> response_map_x = new Map<String, wwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponse>();
               response.put('response_x', respElement);
               system.debug(response+'insideMock');
                
            } 
        }