@isTest
global class syncServiceArtransactionMockImpl implements WebServiceMock { 
  
    global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {

            wwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponse respElement = new wwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponse();
               
               // Added By Hugo Pinto
               wwwNbncoComAuServiceArtransactionhi.Correlation_element Correlation = new wwwNbncoComAuServiceArtransactionhi.Correlation_element();
               Correlation.hostReferenceId = '1000457';
               
               // Added By Hugo Pinto
               wwwNbncoComAuServiceArtransactionhi.InvoiceHeader_element InvoiceHeader = new wwwNbncoComAuServiceArtransactionhi.InvoiceHeader_element();
               InvoiceHeader.sourceReferenceNumber = 'CW-00000356';
               InvoiceHeader.Correlation = Correlation;
               
               List<wwwNbncoComAuServiceArtransactionhi.InvoiceRecord_element> InvoiceRecord = new List<wwwNbncoComAuServiceArtransactionhi.InvoiceRecord_element>();
			   wwwNbncoComAuServiceArtransactionhi.InvoiceRecord_element InvRec = new wwwNbncoComAuServiceArtransactionhi.InvoiceRecord_element();
   			   InvRec.transactionDate = '2017-03-28';
   			   InvRec.InvoiceHeader = InvoiceHeader;
   			   
   			   // Added By Hugo Pinto
   			   wwwNbncoComAuServiceArtransactionhi.InvoiceRecord_element InvRec_2 = new wwwNbncoComAuServiceArtransactionhi.InvoiceRecord_element();
   			   InvRec_2.transactionDate = '2017-03-29';
   			   InvRec_2.InvoiceHeader = InvoiceHeader;
   			   
               InvoiceRecord.add(InvRec);
               InvoiceRecord.add(InvRec_2);
               system.debug('//////////'+InvoiceRecord);
               
               wwwNbncoComAuServiceArtransactionhi.InvoiceList_element InvoiceList = new wwwNbncoComAuServiceArtransactionhi.InvoiceList_element();
               InvoiceList.InvoiceRecord = InvoiceRecord;
               system.debug('********' +InvoiceList);
               
			   wwwNbncoComAuServiceArtransactionhi.AccountDetails_element AccountDetails = new wwwNbncoComAuServiceArtransactionhi.AccountDetails_element();
               AccountDetails.balance = '10409';
               AccountDetails.overdueAmount = '0';
               AccountDetails.status = 'Current';
               AccountDetails.InvoiceList = InvoiceList; // Added by Hugo Pinto
               
               wwwNbncoComAuServiceArtransactionhi.AccountIdentifier_element AccountIdentifier = new wwwNbncoComAuServiceArtransactionhi.AccountIdentifier_element();
               AccountIdentifier.sourceReferenceId = 'AYCA-3NS0Y8';
               
               List<wwwNbncoComAuServiceArtransactionhi.AccountRecord_element> AccountRecord = new List<wwwNbncoComAuServiceArtransactionhi.AccountRecord_element>();
               wwwNbncoComAuServiceArtransactionhi.AccountRecord_element AccRec = new wwwNbncoComAuServiceArtransactionhi.AccountRecord_element();
               AccRec.AccountIdentifier = AccountIdentifier;
               AccRec.AccountDetails = AccountDetails;
               AccountRecord.add(AccRec);
               
               wwwNbncoComAuServiceArtransactionhi.AccountList_element AccountList = new wwwNbncoComAuServiceArtransactionhi.AccountList_element();
               AccountList.AccountRecord = AccountRecord;
               
               wwwNbncoComAuServiceArtransactionhi.CustomerDetails_element CustomerDetails = new wwwNbncoComAuServiceArtransactionhi.CustomerDetails_element();
               CustomerDetails.balance = '10409';
               
               wwwNbncoComAuServiceArtransactionhi.CustomerIdentifier_element CustomerIdentifier = new wwwNbncoComAuServiceArtransactionhi.CustomerIdentifier_element();
               CustomerIdentifier.sourceReferenceId = '9792250';
               
               wwwNbncoComAuServiceArtransactionhi.Customer_element Customer = new wwwNbncoComAuServiceArtransactionhi.Customer_element();
               Customer.CustomerIdentifier = CustomerIdentifier;
               Customer.CustomerDetails = CustomerDetails;
               Customer.AccountList = AccountList;
               
               wwwNbnComAuDmmEnterpriseCommon.ResultStatus_element ResultStatus = new  wwwNbnComAuDmmEnterpriseCommon.ResultStatus_element();
               ResultStatus.status = 'successful';
               
               respElement.Customer = Customer;
               respElement.ResultStatus = ResultStatus;
               system.debug(respElement);
               
              // Map<String, wwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponse> response_map_x = new Map<String, wwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponse>();
               response.put('response_x', respElement);
               system.debug(response+'insideMock');
                
            } 
        }