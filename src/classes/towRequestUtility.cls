/***************************************************************************************************
Class Name:         towRequestUtility        
Version:            1.0 
Created Date:       06 September 2018
Function:           used for deserializing NBN Integration cache json payload and returns a map of 
                    work request parameters values.
Input Parameters:   triggers new, old and platform events
Output Parameters:  None
Description:        To be used as a service class
Modification Log:
* Developer          Date             Description
* --------------------------------------------------------------------------------------------------                 
* Murali Krishna    06/09/2018      Updated - Version 1.0
* Syed Moosa Nazir	05-April-2019	Updated - CUSTSA-30132 - created logic to parse the CIS bandwidthProfile dynamically
									instead of referring the config table.
									Class: Jigsaw_BandwidthProfileParser.formatBandwidthProfileForToW
****************************************************************************************************/
public with sharing class towRequestUtility
{
    Public Static Map<String,String> getTOWRequestInputs(String jsonPayload,List<String> lstConfigurationParameters)
    {
        Map<String,String> towRequestParameters = new Map<String,String>();
        if(!String.isBlank(jsonPayload))
        {
            NBNIntegrationTranslation__c instTrans = new NBNIntegrationTranslation__c();
            NBNIntegrationTranslation__c intTrans;
            integrationWrapper.RootObject RtServiceObject;
            RtServiceObject = (integrationWrapper.RootObject)JSON.deserialize(jsonPayload,integrationWrapper.RootObject.Class);
            If(RtServiceObject != null && RtServiceObject.data !=null && RtServiceObject.data.attributes!=null)
            {
                for(String strConfigurationParameter : lstConfigurationParameters)
                {
                    if(strConfigurationParameter.Equals('strMinimumRequiredLayer1UpstreamBitrate') || strConfigurationParameter.Equals('strMinimumRequiredLayer1DownstreamBitrate'))
                    {
                        if(RtServiceObject.data.attributes.avcConfig!=null ) {
                            If(strConfigurationParameter.Equals('strMinimumRequiredLayer1UpstreamBitrate')) {
                                towRequestParameters.put(strConfigurationParameter,String.valueOf(RtServiceObject.data.attributes.avcConfig.minLay1UpBitRate));
                            }
                            If(strConfigurationParameter.Equals('strMinimumRequiredLayer1DownstreamBitrate')) {
                                towRequestParameters.put(strConfigurationParameter,String.valueOf(RtServiceObject.data.attributes.avcConfig.minLay1DownBitRate));
                            }
                        }
                        continue;
                    }
                    if(strConfigurationParameter.Equals('strUpstreamSpeed') || strConfigurationParameter.Equals('strDownstreamSpeed'))
                    {
                        if(RtServiceObject.data.attributes.bandwidthProfile!=null ){
							List<string> formatedDsUsValue = Jigsaw_BandwidthProfileParser.formatBandwidthProfileForToW(String.valueOf(RtServiceObject.data.attributes.bandwidthProfile));
                            if(formatedDsUsValue <> null && !formatedDsUsValue.isEmpty()) {
                                if(strConfigurationParameter.Equals('strUpstreamSpeed')) {
                                    towRequestParameters.put(strConfigurationParameter, formatedDsUsValue.get(1));
                                }
                                if(strConfigurationParameter.Equals('strDownstreamSpeed')) {
                                    towRequestParameters.put(strConfigurationParameter, formatedDsUsValue.get(0));
                                }
                            }
                        }
                        continue;
                    }
                    if(strConfigurationParameter.Equals('strCopperpathId') || strConfigurationParameter.Equals('strPotsInterconnect'))
                    {
                        If(RtServiceObject.data.attributes.uniDsl != null) {
                            if(strConfigurationParameter.Equals('strCopperpathId') && RtServiceObject.data.attributes.uniDsl.uniDsLId != null) {
                                towRequestParameters.put(strConfigurationParameter, String.valueOf(RtServiceObject.data.attributes.uniDsl.uniDslId));
                            }
                            if(strConfigurationParameter.Equals('strPotsInterconnect'))
                            {
                                if(RtServiceObject.data.attributes.uniDsl.csll!=null) {
                                    If(RtServiceObject.data.attributes.uniDsl.csll.productStatus != Null && RtServiceObject.data.attributes.uniDsl.csll.productStatus.Equals('Terminated')) {
                                        towRequestParameters.put(strConfigurationParameter, '');
                                    }
                                    else
                                    {
                                        towRequestParameters.put(strConfigurationParameter, String.valueOf(RtServiceObject.data.attributes.uniDsl.potsInterconnect));
                                    }
                                }
                                else
                                {
                                    towRequestParameters.put(strConfigurationParameter, '');
                                }
                            }
                        }
                        continue;
                    }
                }
            }
            if(RtServiceObject.included != Null) {
                for(integer i=0; i < RtServiceObject.included.size(); i++) {
                    if(RtServiceObject.included[i].id != Null && RtServiceObject.included[i].attributes.locationId != Null) {
                        towRequestParameters.put('strLocId', String.valueOf(RtServiceObject.included[i].attributes.locationId));
                    }        
                }                 
            }
        }
        System.Debug(towRequestParameters);
        return towRequestParameters;
    }   
}