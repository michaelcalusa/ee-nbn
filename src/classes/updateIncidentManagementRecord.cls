/***************************************************************************************************
Class Name:         updateIncidentManagementRecord
Class Type:         Trigger handler class
Version:            2.0 
Created Date:       12 October 2018
Function:           Handle Incident Management Record Updates Based On Ticket Of Work Response From Maximo
Input Parameters:   List Of Incident Management Record Ids, Map of IncidentManagement Record Id and TicketOf WorkWrapper.
Output Parameters:  None
Description:        To be used as a Queueable class
Modification Log:
* Developer          Date             Description
* --------------------------------------------------------------------------------------------------                 
* Murali Krishna    12/10/2018      Created - Version 1.0
****************************************************************************************************/
public class updateIncidentManagementRecord implements Queueable 
{
    //List Varibale To Store Incident Management Record IDS;
    List<Id> lstIncidentManagementRecords;
    //Map Variable To Store Map of incident management record id and tickteofworkwrapper.
    Map<String,integrationWrapper.TicketOfWork> IncidentIdWrapperMap;
    //Constructor to intialize the global variables.
    public updateIncidentManagementRecord(List<Id> lstIncidentManagementRecords,Map<String,integrationWrapper.TicketOfWork> IncidentIdWrapperMap) 
    {
        this.lstIncidentManagementRecords = lstIncidentManagementRecords;
        this.IncidentIdWrapperMap = IncidentIdWrapperMap;
    }
    //Method to execute the queueable statements.
    public void execute(QueueableContext workOrderHandlerQContext) {
        try
        {
            //List used to store the list of incident management records to be updated.
            List<Incident_Management__c> listIncToUpdate = new List<Incident_Management__c>();
            //Map variable to store derived map values retrived from NBNDerivedFaultType__c custom settings with Fault Type as key.
            Map<String,String> derivedMap = new Map<String,String>();
            //Loop on NBNDerivedFaultType__c custom settings to prepare derivedMap.
            for(NBNDerivedFaultType__c cset : (NBNDerivedFaultType__c.getAll()).Values()) {
                if(!derivedMap.Keyset().Contains(cset.derivedValue__c))
                    derivedMap.put(cset.derivedValue__c,cset.Abbreviated_Value__c);
            }
            if(lstIncidentManagementRecords != Null && !lstIncidentManagementRecords.isEmpty())
            {
                //Temporay variable used for storing abbrivated value of fault type retrieved from custom settings.used in deriving WOSummary
                String faultType = '';
                //Temporay variable used for storing tech type retrieved from incident management record. used in deriving WOSummary
                String techType = '';
                //Temporay variable used for storing AVC retrieved from incident management record.used in deriving WOSummary
                String avc = '';
                //Temporay variable used for storing work order id retrieved from ticket of work record.used in deriving WOSummary
                String wrid = '';
                //Temporay variable used for storing Appointment type retrieved from ticket of work record.used in deriving WOSummary
                String apptType = '';
                //Temporay variable used for storing Appointment Date retrieved from ticket of work record.used in deriving WOSummary//Temporay variable used for preparing work order summary.
                String apptDate = '';
                //Temprary variable used for storing work order summary.
                String woSummary = '';
                DateTime apptStart;
                DateTime targCom;
                //Temporary varaible used to store ticket of work record retrieved from IncidentIdWrapperMap using Incident Management Record.
                integrationWrapper.TicketOfWork towRecord;
                //Loop the records queried from Database
                for(Incident_Management__c currentIncidentRecord : [Select Id,Name,Awaiting_Ticket_of_Work__c,WOR_ID__c,Summary__c,User_Action__c,Fault_Type__c,Prod_Cat__c,Current_work_order_status_reason__c,Status_Reason__c,AVC_Id__c,Target_Commitment_Date__c,Appointment_Start_Date__c,Engagement_Type__c,Current_Work_Order_Appointment_Id__c from Incident_Management__c Where Id In :lstIncidentManagementRecords and User_Action__c = 'createWorkRequest' and Awaiting_Ticket_of_Work__c = true For Update]) {
                    towRecord = IncidentIdWrapperMap.get(currentIncidentRecord.Name);
                    //check the action is performed in jigsaw based on the user action and Awaiting_Ticket_of_Work__c.
                    if(towRecord != Null && currentIncidentRecord.User_Action__c.Equals('createWorkRequest') && currentIncidentRecord.Awaiting_Ticket_of_Work__c)
                    {
                        if(derivedMap.containsKey(towRecord.faultType)) {
                            faultType = derivedMap.get(towRecord.faultType);
                        }
                        if(!String.isBlank(currentIncidentRecord.Prod_Cat__c) && currentIncidentRecord.Prod_Cat__c.Equals('NCAS-FTTN')) {
                            techType = 'FTTN';
                        }
                        if(!String.isBlank(currentIncidentRecord.Prod_Cat__c) && currentIncidentRecord.Prod_Cat__c.Equals('NCAS-FTTB')) {
                            techType = 'FTTB';
                        }
                        /*if(!String.isBlank(towRecord.appointmentId))
                        {
                            currentIncidentRecord.Current_Work_Order_Appointment_Id__c = towRecord.appointmentId;
                        }
                        if(!String.isBlank(towRecord.appointmentType))
                        {
                            currentIncidentRecord.Engagement_Type__c = towRecord.appointmentType;
                        }*/
                        if(currentIncidentRecord.Engagement_Type__c.equals('Commitment')) {
                            apptType = 'COM';
                            if(currentIncidentRecord.Target_Commitment_Date__c != null){
                                targCom = currentIncidentRecord.Target_Commitment_Date__c;
                                apptDate = String.valueOf(targCom.day())+'/'+String.valueOf(targCom.month());
                                apptDate = apptDate + ' ' + targCom.format('a', 'Australia/Sydney');
                            } else {
                                apptDate = 'No target date supplied';
                            }
                        }
                        if(!String.isBlank(currentIncidentRecord.Engagement_Type__c) && currentIncidentRecord.Engagement_Type__c.equals('Appointment')) {
                                apptType = 'APT';
                                if(currentIncidentRecord.Appointment_Start_Date__c != null) {
                                    system.debug('>>>>Inside Update Appointment start date ' + currentIncidentRecord.Appointment_Start_Date__c);
                                    apptStart = currentIncidentRecord.Appointment_Start_Date__c;
                                    apptDate = String.valueOf(apptStart.day())+'/'+String.valueOf(apptStart.month());
                                    apptDate = apptDate + ' ' + apptStart.format('a', 'Australia/Sydney');
                                }else{
                                    apptDate = 'No appointment start date supplied';
                                }
                        }
                        //Prepare WoSummary Values.
                        avc = currentIncidentRecord.AVC_Id__c;
                        wrid = towRecord.ticketOfWorkId;    
                        woSummary = techType + '-' + faultType + ' | *' + avc + '*' + wrid + ' ' + apptType + ' ' + apptDate; 
                        system.debug('>>>>Summary'+woSummary);
                        currentIncidentRecord.summary__c = woSummary;
                        /*currentIncidentRecord.WOR_ID__c = towRecord.ticketOfWorkId; 
                        currentIncidentRecord.Current_work_order_status__c = towRecord.ticketOfWorkStatus;
                        currentIncidentRecord.Current_work_order_status_reason__c = towRecord.ticketOfWorkStatusReason;*/
                        currentIncidentRecord.Awaiting_Ticket_of_Work__c = false;
                        currentIncidentRecord.Awaiting_Current_Incident_Status__c = true;
                        currentIncidentRecord.User_Action__c = 'dispatchTechnician';
                        listIncToUpdate.add(currentIncidentRecord);
                    }
                }
                update listIncToUpdate;
            }
            if(Test.isRunningTest())
            {
                Integer dividebyZero = 3 / 0; 
            }
        }
        catch(Exception ex) {
            //log the exception details to application log.
            GlobalUtility.logMessage(GlobalConstants.ERROR_RECTYPE_NAME,'WorkOrderHandler','updateIncident','','Tried to update an Incident after TOW created via integration','','',ex,0);
        }
    }
}