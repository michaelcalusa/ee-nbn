global class updateTask implements Schedulable{
  global string appId;
  global void execute (SchedulableContext sc) {
    //get the right task
        List<Task> tList = new List<Task>();
        NewDev_Application__c newDevApp = [SELECT Id, SF_Stage_ID__c FROM NewDev_Application__c WHERE Id = :appId];
        if (newDevApp.SF_Stage_ID__c <> null && newDevApp.SF_Stage_ID__c <> ''){
            tList = [Select Id, Deliverable_Status__c from Task Where WhatId = :newDevApp.SF_Stage_ID__c and Deliverable_Status__c = 'Pending Documents'];
            if (tList.size()==1){            
                //Move the web form uploads to stage related task
                List<ContentDocumentLink> conDocListToInsert = new List<ContentDocumentLink>();
                for (ContentDocumentLink con : [Select ContentDocumentId, LinkedEntityId from ContentDocumentLink Where LinkedEntityId = :appId]){
                    //Create ContentDocumentLink
                    ContentDocumentLink cDe = new ContentDocumentLink(ContentDocumentId = con.ContentDocumentId, LinkedEntityId = tList[0].Id, ShareType = 'V');
                    conDocListToInsert.add(cDe);
                }
                try{
                    insert conDocListToInsert;
                }catch (Exception e){GlobalUtility.logMessage('Error', 'updateTask', 'execute', '', '', '', '', e, 0);}
                newDevFileuploadctrl.updateTaskStatus(tList[0].Id);
            }
        }
        string myname = 'UpdateTask : Application Id:'+appId;
        List<CronJobDetail> cronDetailList = [SELECT Id FROM CronJobDetail WHERE Name = :myname];
        if (cronDetailList.size() > 0) {
            Id jobId = [SELECT Id from CronTrigger WHERE CronJobDetailId = :cronDetailList[0].Id].Id;
            System.abortJob(jobId);
        }
    }
  public updateTask(String aId){
    appId = (appId == null) ? aId : appId;
  }
}