@isTest
private class updateTask_Tests {
    static testmethod void test() {
        //test data here
        //CS case management Team queue
        Customer_Service_Team__c csmt = new Customer_Service_Team__c(
            Name = 'CS Case Management Team',
            Email__c = 'tesxtea@example.com',
            Task_Assignment_Notification__c = true
        );
        insert csmt;
        
        //create unassigned user    
        Profile p = [SELECT Id FROM Profile WHERE Name = 'NBN Contact Centre User' LIMIT 1];   
        User unassignedUser = TestDataUtility.createTestUser(false, p.Id);
        insert unassignedUser;
        System.debug('===unassignedUser:' + unassignedUser);

        //create Customer Service Setting
        Customer_Service_Setting__c css = new Customer_Service_Setting__c(
            SetupOwnerId = UserInfo.getOrganizationId(),
            Unassigned_User_Id__c = unassignedUser.Id,
            CS_Case_Management_Team_Id__c = csmt.Id,
            Task_Record_Type_Id_Task__c = schema.sobjecttype.Task.getrecordtypeinfosbyname().get('Task').getRecordTypeId(),
            Task_Record_Type_Id_Note__c = schema.sobjecttype.Task.getrecordtypeinfosbyname().get('Note').getRecordTypeId(),
            Task_Record_Type_Id_Interaction__c = schema.sobjecttype.Task.getrecordtypeinfosbyname().get('Interaction').getRecordTypeId()
        );
        System.debug('===Customer_Service_Setting__c:' + css);
        insert css;
                

        //create account
        Account account = new Account(
            Name = 'Test Account',
            RecordTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Household').getRecordTypeId(),
            Tier__c = '1',
            Type__c = 'RSP',
            Status__c = 'Active'
        );
        insert account;
        
        Contact contact = new Contact(
            LastName = 'Test Contact',
            RecordTypeId = schema.sobjecttype.Contact.getrecordtypeinfosbyname().get('External Contact').getRecordTypeId(),
            AccountId = account.Id,
            Email = 'teste@example.com',
            Preferred_Phone__c = '0288889999'
        );
        insert contact;
        
        Case caseObj = new Case(
            Subject = 'Test Case',
            Description = 'Test Case',
            RecordTypeId = schema.sobjecttype.Case.getrecordtypeinfosbyname().get('Query').getRecordTypeId(),
            Phase__c = 'Information',
            Category__c = '(I) Communications',
            Sub_Category__c = 'Discovery Centre',
            Origin = 'Email',
            Status = 'Open',
            Priority = '1-ASAP',
            ContactId = contact.Id,
            Primary_Contact_Role__c = 'General Public'
        );
        insert caseObj;
        
        //Create a Dveleopments object record
        Development__c devObj = new Development__c();      
        devObj.Name='testName';
        devObj.Development_ID__c='AKHS833';
        devObj.Account__c=account.Id;
        devObj.Primary_Contact__c=contact.Id;
        devObj.Suburb__c='testsuburd';
        insert devObj;
        
        SA_Auto_Reference_Number__c refNumber = new SA_Auto_Reference_Number__c(Name = 'SA001', Last_Sequence_Number__c = '000000000');
        insert refNumber;
        
        // Create Stage Application object record
        Stage_Application__c stageAppObj=new Stage_Application__c();
        stageAppObj.Name='testName';
        stageAppObj.Request_ID__c ='AKHS833';
        stageAppObj.Development__c=devObj.Id;
        stageAppObj.Account__c=account.Id;
        stageAppObj.Primary_Contact__c=contact.Id;
        insert stageAppObj; 
        
        //create new dev app
        NewDev_Application__c app = new NewDev_Application__c();
        app.SF_Stage_ID__c = stageAppObj.Id;
        insert app;
        
        Customer_Service_Team__c team = new Customer_Service_Team__c(
            Name = 'Test Team',
            Email__c = 'test@example.com',
            Task_Assignment_Notification__c = true
        );
        insert team;

        Task t = new Task(
            RecordTypeId = [SELECT Id, Name FROM RecordType WHERE Name = 'New Development' AND SobjectType = 'Task'].Id,
            Subject = 'Call',
            Priority = '3-General',
            WhatId = stageAppObj.Id,
            WhoId=contact.Id,
            //Team__c = team.Id,
            Due_Date__c = Date.Today(),
            Type_Custom__c = 'Deliverables',
            Deliverable_Status__c= 'Pending Documents'
        );
        insert t;
        Test.startTest(); 
            String jobId = System.schedule('updateTask', '0 0 0 3 9 ? 2022' , new updateTask(app.Id));
            // Get the information from the CronTrigger API object
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
                            NextFireTime
                            FROM CronTrigger WHERE id = :jobId];
            // Verify the expressions are the same
            System.assertEquals('0 0 0 3 9 ? 2022',ct.CronExpression);
            // Verify the job has not run
            System.assertEquals(0, ct.TimesTriggered);
        Test.stopTest();
    }
    
}