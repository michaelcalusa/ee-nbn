/***************************************************************************************************
Class Name:         viewServiceLocation_LC
Class Type:         Controller Class 
Version:            1.0 
Created Date:       15 Sept 2017
Function:           
Input Parameters:   None 
Output Parameters:  None
Description:        class to retrieve Service Information
Modification Log:
* Developer          Date             Description
* --------------------------------------------------------------------------------------------------                 
* Sanghita Das      15/11/2017      Created - Version 1.0 
****************************************************************************************************/ 

public class viewServiceLocation_LC{
    
    Public static integrationWrapper.RootObject RtObject ;
    public static integrationWrapper.RootProdObject RtPObject;
    Public static Map<String,String> servMap=new Map<String,String>(); 
    Public static Map<String,String> locMap=new Map<String,String>();   
    Public static List<LightningComponentConfigurations__c> lstLightningComponentConfig = new List<LightningComponentConfigurations__c>();
    Public static List<LightningComponentConfigurations__c> lstLightningComponentConfigbyUser = new List<LightningComponentConfigurations__c>();
    Public static String PRIid='';
    Public static String LocID='';  
    Public static HttpResponse ProdResp = new HttpResponse();  
    
    public static Map<String,String> viewSer(HttpResponse response,String incNum){    
        
        try{ 
            String ASI='';       
            if(response.getBody()!='{}'&& response.getBody()!='' && response.getStatusCode() == 200){   
                RtObject = (integrationWrapper.RootObject)JSON.deserialize(response.getBody(),integrationWrapper.RootObject.Class);       
                if(RtObject!=null){
                    if(RtObject.included !=null){
                        for(integer i=0; i<=RtObject.included.size()-1; i++){
                            if(RtObject.included[i].attributes!=null){
                                if(RtObject.included[i].attributes.accessSeekerId!=null)
                                {
                                    ASI=RtObject.included[i].attributes.accessSeekerId;
                                    String AccessSeekerName='';
                                    AccessSeekerName= getAccName(ASI);                        
                                    if(AccessSeekerName!='')
                                        servMap.put('AccessSeekerName', AccessSeekerName); 
                                }
                            }
                        }
                    }
                    if(RtObject.data.relationships.associatedWith.data!=null){
                    //requirement has change so that order completion date needs to be displayed on the location module. Leaving code here in case this changes but as of 12/01/2018 this isn't being used
                        servMap.put('priID',RtObject.data.relationships.associatedWith.data.id);
                         try{
                            ProdResp = integrationUtility.getHttpResponse(RtObject.data.relationships.associatedWith.data.id,'');
                            if(ProdResp.getBody()!='{}'&& ProdResp.getBody()!='' && ProdResp.getStatusCode() == 200){
                                RtPObject = (integrationWrapper.RootProdObject)JSON.deserialize(ProdResp.getBody(),integrationWrapper.RootProdObject.Class);
                                    for(integer j=0; j<=RtPObject.data.size()-1; j++){
                                        servMap.put('OrderCompletionDate', RtPObject.data[j].attributes.orderCompletionDate);
                                    }
                            }
                         }
                         catch (Exception ex){} 
                    }
                    if(RtObject.data !=null){
                        servMap.put('AVCID', RtObject.data.id);
                        servMap.put('Type',RtObject.data.type);
                        if(RtObject.data.attributes!=null){
                            String strValuetoDisplay = '';
                            String intkey = 'BandwidthProfile';
                            strValuetoDisplay = CustomSettingsUtility.getDisplayVal(strValuetoDisplay,String.valueOf(RtObject.data.attributes.bandwidthProfile),intkey);
                            if(strValuetoDisplay != null){
                                servMap.put('BandwidthProfile',strValuetoDisplay);
                            }else{
                                servMap.put('BandwidthProfile',RtObject.data.attributes.bandwidthProfile);
                            }
                            //TODO fix line below it is a fudge. The lightning component needs to be re-written to recieve the map and then assign them to lightning attributes 
                            //then display the attributes rather then just iterate through a map
                            if(RtObject.data.attributes.primaryAccessTechnology==null){
                                servMap.put('PrimaryAccessTechnology',Label.No_Information);
                            }else{
                                String techIntKey = 'PrimaryAccessTechnology';
                                String typeIntKey = 'Type';
                                String straccessTechValuetoDisplay = CustomSettingsUtility.getDisplayVal(strValuetoDisplay,String.valueOf(RtObject.data.attributes.primaryAccessTechnology),techIntKey);
                                String strTechValuetoDisplay = CustomSettingsUtility.getDisplayVal(strValuetoDisplay,String.valueOf(RtObject.data.type),typeIntKey);
                                if(straccessTechValuetoDisplay != null && strTechValuetoDisplay != null){
                                    servMap.put('PrimaryAccessTechnology&Type',straccessTechValuetoDisplay+' '+strTechValuetoDisplay);
                                }
                            }
                            if(RtObject.data.attributes.avcConfig!=null ){
                                servMap.put('MinimumL1BitrateUpstream',RtObject.data.attributes.avcConfig.minLay1UpBitRate);
                                servMap.put('MinimumL1BitrateDownstream',RtObject.data.attributes.avcConfig.minLay1DownBitRate);
                            }
                            if(RtObject.data.attributes.cvc!=null){
                                servMap.put('CVCID',RtObject.data.attributes.cvc.cvcid);
                                servMap.put('TC1BandwidthProfile',RtObject.data.attributes.cvc.tc1BandwidthProfile);
                                servMap.put('TC2BandwidthProfile',RtObject.data.attributes.cvc.tc2BandwidthProfile);
                                servMap.put('TC4BandwidthProfile',RtObject.data.attributes.cvc.tc4BandwidthProfile);
                            }
                            if(RtObject.data.attributes.uniDsl!=null){
                                servMap.put('CopperPathID',RtObject.data.attributes.uniDsl.uniDslId);
                                servMap.put('VLANMode',RtObject.data.attributes.uniDsl.uniDslVlanMode);
                                servMap.put('StabillityProfile',RtObject.data.attributes.uniDsl.dslStabilityProfile);
                                servMap.put('ExchangePairConnection',RtObject.data.attributes.uniDsl.exchangePairConnection);       
                            }
                        }
                    }
                    
                    if(RtObject.included !=null){
                        for(integer i=0; i<=RtObject.included.size()-1; i++){
                            if( RtObject.included [i].id!=null && RtObject.included[i].id.startsWith('SWO'))
                            {
                                servMap.put('DSLAMID',RtObject.included [i].id);
                                
                            }
                            if(RtObject.included [i].attributes !=null){                      
                                if( RtObject.included [i].attributes.port !=null )
                                {
                                    servMap.put('DSLAMPort',RtObject.included [i].attributes.port);                        
                                }
                                if(RtObject.included[i].attributes.dpboProfile !=null && !String.isblank(String.valueoF(RtObject.included[i].attributes.dpboProfile))){
                                    servMap.put('DBPOProfile','Yes');                       
                                } 
                                else{
                                    servMap.put('DBPOProfile','No');
                                }                  
                            }                   
                        }                 
                    }
                }                                     
            }  
            if(response.getStatusCode() == 404 || response.getStatusCode() == 400 || response.getStatusCode() == 500 || response.getStatusCode() == 408){
                if(response.getStatusCode() == 400){
                    NBNIntegationErrors__c NBNError = NBNIntegationErrors__c.getInstance('Services400');
                        If(NBNError.Status_Code__c == String.valueof(response.getStatusCode())){
                            servMap.put('Error',NBNError.Error_Message__c);
                        }
                }
                if(response.getStatusCode() == 404){
                    NBNIntegationErrors__c NBNError = NBNIntegationErrors__c.getInstance('Services404');
                        If(NBNError.Status_Code__c == String.valueof(response.getStatusCode())){
                            servMap.put('Error',NBNError.Error_Message__c);
                         }
                }
                if(response.getStatusCode() == 408){
                    NBNIntegationErrors__c NBNError = NBNIntegationErrors__c.getInstance('Services408');
                        If(NBNError.Status_Code__c == String.valueOf(response.getStatusCode())){
                            servMap.put('Error',NBNError.Error_Message__c);
                        }
                 }
                if(response.getStatusCode() == 500){
                    NBNIntegationErrors__c NBNError = NBNIntegationErrors__c.getInstance('Services500');
                        If(NBNError.Status_Code__c == String.valueOf(response.getStatusCode())){
                            servMap.put('Error',NBNError.Error_Message__c);
                        }
                 }
             }
             else{
                servMap.put('Error','');
             }    
        }
        catch (Exception ex){} 
        return servMap;
    }
    
    public static Map<String,String> viewLoc(HttpResponse response,String incNum,String serviceSegment){        
        try{ 
            String ASI='';     
                if(serviceSegment==null){
                    locMap.put('BusinessvsResidential',Label.No_Information);
                }
                else{
                    locMap.put('BusinessvsResidential',serviceSegment);
                }  
            if(response.getBody()!='{}'&& response.getBody()!=''){   
                RtObject = (integrationWrapper.RootObject)JSON.deserialize(response.getBody(),integrationWrapper.RootObject.Class);       
                if(RtObject!=null){
                    if(RtObject.included !=null){
                        for(integer i=0; i<=RtObject.included.size()-1; i++){
                            if(RtObject.included[i].attributes!=null){
                                if(RtObject.included[i].attributes.accessSeekerId!=null)
                                {
                                    ASI=RtObject.included[i].attributes.accessSeekerId;
                                    String AccessSeekerName='';
                                    AccessSeekerName= getAccName(ASI);                        
                                    if(AccessSeekerName!='')
                                        locMap.put('AccessSeekerName', AccessSeekerName); 
                                }
                            }
                        }
                    }
                    if(RtObject.data.relationships.associatedWith.data!=null){
                        locMap.put('priID',RtObject.data.relationships.associatedWith.data.id);
                        PRIid=RtObject.data.relationships.associatedWith.data.id; 
                        try{
                            ProdResp = integrationUtility.getHttpResponse(PRIid,'');
                            if(ProdResp.getBody()!='{}'&& ProdResp.getBody()!='' && ProdResp.getStatusCode() == 200){
                                RtPObject = (integrationWrapper.RootProdObject)JSON.deserialize(ProdResp.getBody(),integrationWrapper.RootProdObject.Class);
                                for(integer j=0; j<=RtPObject.data.size()-1; j++){
                                    if(RtPObject.data[j].attributes.orderCompletionDate==null){
                                        locMap.put('orderCompletionDate',Label.No_Information);
                                    }
                                    else{
                                        locMap.put('orderCompletionDate',RtPObject.data[j].attributes.orderCompletionDate);
                                    }
                                }
                                for(integer k=0; k<=RtPObject.included.size()-1; k++){
                                    if(RtPObject.included[k].attributes.address==null){
                                        locMap.put('PhysicalAddressState',Label.No_Information);
                                    }
                                    else{
                                        locMap.put('PhysicalAddressState',RtPObject.included[k].attributes.address.unstructured);
                                    }
                                    if(RtPObject.included[k].attributes.dwellingType==null){
                                           locMap.put('Typeofpremise',Label.No_Information);
                                    }else{
                                         String intKey = 'Typeofpremise';
                                         String strValuetoDisplay = '';
                                         strValuetoDisplay = CustomSettingsUtility.getDisplayVal(strValuetoDisplay,String.valueOf(RtPObject.included[k].attributes.dwellingType),intKey);
                                         if(strValuetoDisplay != null){
                                            locMap.put('Typeofpremise',strValuetoDisplay);
                                         }
                                    }
                                   if(RtPObject.included[k].attributes.region==null){
                                       locMap.put('Region',Label.No_Information);
                                   }
                                   else{
                                       locMap.put('Region',RtPObject.included[k].attributes.region);
                                   }
                                   if(RtPObject.included[k].id==null){
                                       locMap.put('locid',Label.No_Information);
                                   }
                                   else{
                                       locMap.put('locid',RtPObject.included[k].id);
                                   }
                                   LocID=RtPObject.included[k].id;
                                }
                            }
                           if(ProdResp.getStatusCode() == 404 || ProdResp.getStatusCode() == 400 || ProdResp.getStatusCode() == 409 || ProdResp.getStatusCode() == 429 || ProdResp.getStatusCode() == 500){
                               if(ProdResp.getStatusCode() == 400){
                                    NBNIntegationErrors__c NBNError = NBNIntegationErrors__c.getInstance('Products400');
                                    If(NBNError.Status_Code__c == String.valueof(ProdResp.getStatusCode())){
                                        locMap.put('Error',NBNError.Error_Message__c);
                                    }
                               }
                               if(ProdResp.getStatusCode() == 404){
                                    NBNIntegationErrors__c NBNError = NBNIntegationErrors__c.getInstance('Products404');
                                    If(NBNError.Status_Code__c == String.valueof(ProdResp.getStatusCode())){
                                        locMap.put('Error',NBNError.Error_Message__c);
                                    }
                               }
                               if(ProdResp.getStatusCode() == 408){
                                    NBNIntegationErrors__c NBNError = NBNIntegationErrors__c.getInstance('Products408');
                                    If(NBNError.Status_Code__c == String.valueof(ProdResp.getStatusCode())){
                                        locMap.put('Error',NBNError.Error_Message__c);
                                    }
                               }
                               if(ProdResp.getStatusCode() == 429){
                                    NBNIntegationErrors__c NBNError = NBNIntegationErrors__c.getInstance('Products429');
                                    If(NBNError.Status_Code__c == String.valueOf(ProdResp.getStatusCode())){
                                        locMap.put('Error',NBNError.Error_Message__c);
                                    }
                               }
                               if(ProdResp.getStatusCode() == 500){
                                    NBNIntegationErrors__c NBNError = NBNIntegationErrors__c.getInstance('Products500');
                                    If(NBNError.Status_Code__c == String.valueOf(ProdResp.getStatusCode())){
                                        locMap.put('Error',NBNError.Error_Message__c);
                                    }
                               }
                           }
                           else{
                               locMap.put('Error','');
                           }
                         }
                        catch (Exception ex){} 
                    }
                }
            }      
        }
        catch (Exception ex){} 
        return locMap;
    }
    
    // method to get the account name      
    public static String getAccName(String id){
        String Accname='';
        List<Account>  AccnameList=[select name from account where Access_Seeker_ID__c =:id ];
        if(AccnameList.size()>0){
            Accname=AccnameList[0].name;  
            
            return Accname;
        }
        else return ''; 
    }
    
    //method to deserialize JSON
    
    public Static Map<String,Object> deserializeJSON(Object jsonObj,String param1){      
        String jsonStr=String.valueOf(jsonObj);
        Map <String, Object> root = (Map <String, Object>) JSON.deserializeUntyped(jsonStr);         
        Map<String,Object>  subvalue= new Map<String,Object>() ;
        for(String i:root.keyset()){
            if(i==param1){
                subValue.put(i,root.get(i));
            }      
        }return subvalue;   
    }
    
    
    //method to invoke services API
    @auraEnabled 
    public static List<string> getServ(String sLightningComponentName, String incNum){ 
        
        List<String> servList=new List<String>();
        Set<String> fromCustSettingSet= new set<String>();
        string avcId;
        Map<String,String> fromResp=new Map<String,String>();
        HttpResponse response = new HttpResponse();
        if(!Test.isRunningTest()){
               if(incNum!=null){              
                List<Incident_Management__c> lstIncMan = new List<Incident_Management__c>();
               lstIncMan =[SELECT AVC_Id__c from Incident_Management__c where Id =: incNum];
               for(Incident_Management__c incM:lstIncMan ){
                   if(incM.Id == incNum){
                       avcId = incM.AVC_Id__c;
                   }
               }
            }
            if(avcId!=null && !String.isblank(avcId))
            {
                response = integrationUtility.getServiceInfo(avcId);  
            }          
        } 
        if(response.getStatusCode() == 400 || response.getStatusCode() == 404 || response.getStatusCode() == 408 || response.getStatusCode() == 429 || response.getStatusCode() == 500){ 
            GlobalUtility.logIntMessage(GlobalConstants.ERROR_RECTYPE_NAME,'viewServiceLocation_LC','viewSer','','','',string.valueOf(response.getBody()),0);          
        }
        if(response.getStatusCode() == 200){  
            fromResp=viewSer(response,incNum);          
        }  
        
        lstLightningComponentConfig = CustomSettingsUtility.getLightningComponentConfiguration(sLightningComponentName);
        lstLightningComponentConfigbyUser = CustomSettingsUtility.getObjectConfig(lstLightningComponentConfig,GlobalUtility.getProfileName(),GlobalUtility.getRoleName());
        
        Map<string,string> fromCustSettingmap = new Map<string,string>();
        fromCustSettingmap = GlobalUtility.processCustomSetting(lstLightningComponentConfigbyUser);       

        servList=getUserVal(fromCustSettingmap,fromResp);        
        return servList; 
    } 
    
        @auraEnabled 
    public static List<string> getLoc(String sLightningComponentName, String incNum){ 
        
        List<String> locList=new List<String>();
        Set<String> fromCustSettingSet= new set<String>();
        string avcId;
        string serviceSegment;
        Map<String,String> fromResp=new Map<String,String>();
        HttpResponse response = new HttpResponse();
        if(!Test.isRunningTest()){
            if(incNum!=null){              
                List<Incident_Management__c> lstIncMan = new List<Incident_Management__c>();
                lstIncMan =[SELECT AVC_Id__c,Segment__c from Incident_Management__c where Id =: incNum];
                for(Incident_Management__c incM:lstIncMan ){
                    if(incM.Id == incNum){
                       avcId = incM.AVC_Id__c;
                       serviceSegment = incM.Segment__c;
                    }
                }
            }            response = integrationUtility.getServiceInfo(avcId);            
        } 
        if(response.getStatusCode() == 400 || response.getStatusCode() == 404 || response.getStatusCode() == 408 || response.getStatusCode() == 500){ 
            GlobalUtility.logIntMessage(GlobalConstants.ERROR_RECTYPE_NAME,'viewServiceLocation_LC','viewLoc','','','',string.valueOf(response.getBody()),0);          
        }
        if(response.getStatusCode() == 200){  
            fromResp=viewLoc(response,incNum,serviceSegment);   
            //fromResp=viewLocTemp(response,incNum);  
        }  
        
        lstLightningComponentConfig = CustomSettingsUtility.getLightningComponentConfiguration(sLightningComponentName);
        lstLightningComponentConfigbyUser = CustomSettingsUtility.getObjectConfig(lstLightningComponentConfig,GlobalUtility.getProfileName(),GlobalUtility.getRoleName());
        
        Map<string,string> fromCustSettingmap = new Map<string,string>();
        fromCustSettingmap = GlobalUtility.processCustomSetting(lstLightningComponentConfigbyUser);       

        locList=getUserVal(fromCustSettingmap,fromResp);        
        return locList; 
    }
    
    //method to get user permitted fields 
    
    public static List<string> getUserVal(map<String,string> fromCustSettingmap,Map<String,String> fromResp){    
        List<String> attributeList=new List<String>();  
        String attri = '';
        
        if(fromResp!=null){
            for(String s:fromCustSettingmap.keyset()){    
                if(fromResp.keyset().contains(s) && fromResp.get(s)!=null){
                    if(fromCustSettingmap.get(s)==null)
                        attri=fromResp.get(s);
                    else
                        attri=fromCustSettingmap.get(s)+'  :  '+fromResp.get(s);
                    attributeList.add(attri);
                }
                if(fromResp.keyset().contains(s) && fromResp.get(s)==null){
                    attri=fromCustSettingmap.get(s)+'  :  '+'No Information';
                    attributeList.add(attri);
                }   
            }
        }
        else{
            attri='No Service or Location information found';
            attributeList.add(attri);
        }
        return attributeList;
    }   
    
    //method to update PRI and LocId in incident object 
    public static void updateIncident(String id,String PRI,String Loc ){
        
        if(id!=null){
            try{
                List<Incident_Management__c> incList=[select PRI_ID__c,locId__c from  Incident_Management__c where id=:id limit 1];
                incList[0].PRI_ID__c=PRI;
                incList[0].locId__c=Loc;    
                update incList;
                
            }
            
            catch(Exception e) {
                System.debug('An unexpected error has occurred: in updateIncident ' + e.getMessage());
            }
        }
    } 
}