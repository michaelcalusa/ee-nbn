<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>IFrameParameters.CSS</label>
    <protected>false</protected>
    <values>
        <field>Parameter_Type__c</field>
        <value xsi:type="xsd:string">String</value>
    </values>
    <values>
        <field>Parameter_Value_Long__c</field>
        <value xsi:type="xsd:string">.control-label{font-weight: normal;display: inline-block;max-width: 100%;margin-bottom: 5px;font-family: Arial,sans-serif;font-size: 16px;line-height: 1.42857;color: #585862;} .form-control{display: block;width: 100%;height: 44px;padding: 10px 12px;font-size: 16px;line-height: 1.42857;color: #555;background-color: #f5f5f5;background-image: none;border: 1px solid #ccc;border-radius: 4px;}</value>
    </values>
    <values>
        <field>Parameter_Value__c</field>
        <value xsi:type="xsd:string">TBD</value>
    </values>
</CustomMetadata>
