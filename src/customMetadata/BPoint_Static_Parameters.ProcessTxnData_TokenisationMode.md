<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ProcessTxnData.TokenisationMode</label>
    <protected>false</protected>
    <values>
        <field>Parameter_Type__c</field>
        <value xsi:type="xsd:string">Integer</value>
    </values>
    <values>
        <field>Parameter_Value_Long__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Parameter_Value__c</field>
        <value xsi:type="xsd:string">1</value>
    </values>
</CustomMetadata>
