<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>BSM EE Pre Order Plus</label>
    <values>
        <field>DF_Permissions__c</field>
        <value xsi:type="xsd:string">EE_Pre_Order_Plus_Permission</value>
    </values>
</CustomMetadata>
