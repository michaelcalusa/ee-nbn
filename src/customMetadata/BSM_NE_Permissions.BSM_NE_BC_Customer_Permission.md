<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>BSM NE BC Customer Permission</label>
    <protected>false</protected>
    <values>
        <field>NE_Permissions__c</field>
        <value xsi:type="xsd:string">NE_BC_Customer_Permission</value>
    </values>
</CustomMetadata>
