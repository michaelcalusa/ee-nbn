<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>NS</label>
    <protected>false</protected>
    <values>
        <field>AppainToSFSource__c</field>
        <value xsi:type="xsd:string">Appian</value>
    </values>
    <values>
        <field>Batch_Size__c</field>
        <value xsi:type="xsd:double">23.0</value>
    </values>
    <values>
        <field>DF_SFC_Desktop_Assessment__c</field>
        <value xsi:type="xsd:string">SF-NS-000001</value>
    </values>
    <values>
        <field>SFC_Desktop_Assessment_completed__c</field>
        <value xsi:type="xsd:string">AP-SF-01000</value>
    </values>
    <values>
        <field>SFToAppainSource__c</field>
        <value xsi:type="xsd:string">Salesforce</value>
    </values>
</CustomMetadata>
