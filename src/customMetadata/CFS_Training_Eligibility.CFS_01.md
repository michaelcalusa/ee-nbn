<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>CFS 01</label>
    <protected>false</protected>
    <values>
        <field>CommunityID__c</field>
        <value xsi:type="xsd:string">0DB0I000000MwylWAC</value>
    </values>
    <values>
        <field>Community_Name__c</field>
        <value xsi:type="xsd:string">ICT Partner Program</value>
    </values>
</CustomMetadata>
