<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Case Questions</label>
    <protected>false</protected>
    <values>
        <field>Nature_of_Call__c</field>
        <value xsi:type="xsd:string">Damage to property</value>
    </values>
    <values>
        <field>Question_Number__c</field>
        <value xsi:type="xsd:double">2.0</value>
    </values>
    <values>
        <field>Question__c</field>
        <value xsi:type="xsd:string">When did the issue with the service occur?</value>
    </values>
</CustomMetadata>
