<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>CM007</label>
    <protected>false</protected>
    <values>
        <field>Field_Set_Name__c</field>
        <value xsi:type="xsd:string">FS_CUS_ACCOUNT_TEAM</value>
    </values>
    <values>
        <field>Field_Set_Type__c</field>
        <value xsi:type="xsd:string">Customer</value>
    </values>
    <values>
        <field>Notes__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Role_Type__c</field>
        <value xsi:type="xsd:string">Official Notices Point of Contact</value>
    </values>
    <values>
        <field>Role__c</field>
        <value xsi:type="xsd:string">Account Team</value>
    </values>
</CustomMetadata>
