<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>CM097</label>
    <protected>false</protected>
    <values>
        <field>Field_Set_Name__c</field>
        <value xsi:type="xsd:string">FS_NBN_ESCALATIONS_PLANNED_OUTAGE</value>
    </values>
    <values>
        <field>Field_Set_Type__c</field>
        <value xsi:type="xsd:string">nbn</value>
    </values>
    <values>
        <field>Notes__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Role_Type__c</field>
        <value xsi:type="xsd:string">Second Contact</value>
    </values>
    <values>
        <field>Role__c</field>
        <value xsi:type="xsd:string">Escalations - Planned Outage</value>
    </values>
</CustomMetadata>
