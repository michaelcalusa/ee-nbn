<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>OVC4CoSHigh</label>
    <protected>false</protected>
    <values>
        <field>API_Name__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>DataType__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>IgnoreProcessing__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Object_Name__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Require_Next__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Required__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Sequence_Number__c</field>
        <value xsi:type="xsd:double">37.0</value>
    </values>
</CustomMetadata>
