<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>DF Order</label>
    <protected>false</protected>
    <values>
        <field>Contact_Matrix_Role__c</field>
        <value xsi:type="xsd:string">Operational - Activations</value>
    </values>
    <values>
        <field>Sender_Address__c</field>
        <value xsi:type="xsd:string">noreply@nbnco.com.au</value>
    </values>
    <values>
        <field>Turn_Off_Creator_Emails__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Turn_Off_Notification_Emails__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Turn_Off_Watch_Me_Emails__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
