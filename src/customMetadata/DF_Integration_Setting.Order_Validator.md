<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Order Validator</label>
    <protected>false</protected>
    <values>
        <field>Context_Path__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>DP_Environment_Override__c</field>
        <value xsi:type="xsd:string">SIT3</value>
    </values>
    <values>
        <field>Enable_Stubbing_PNI__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Enable_Stubbing__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Named_Credential__c</field>
        <value xsi:type="xsd:string">OrderValidator</value>
    </values>
    <values>
        <field>Prod_EndPoint__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Timeout__c</field>
        <value xsi:type="xsd:double">60000.0</value>
    </values>
</CustomMetadata>
