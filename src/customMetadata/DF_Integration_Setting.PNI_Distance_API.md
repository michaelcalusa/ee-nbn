<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>PNI Distance API</label>
    <protected>false</protected>
    <values>
        <field>Context_Path__c</field>
        <value xsi:type="xsd:string">/spatialSTORM/spatialSUITE/v8_2_1/custom/QueryService/V1</value>
    </values>
    <values>
        <field>DP_Environment_Override__c</field>
        <value xsi:type="xsd:string">STG</value>
    </values>
    <values>
        <field>Enable_Stubbing_PNI__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Enable_Stubbing__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Named_Credential__c</field>
        <value xsi:type="xsd:string">NBNAPPGATEWAY</value>
    </values>
    <values>
        <field>Prod_EndPoint__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Timeout__c</field>
        <value xsi:type="xsd:double">120000.0</value>
    </values>
</CustomMetadata>
