<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Change Service Restoration SLA</label>
    <protected>false</protected>
    <values>
        <field>Affected_Component__c</field>
        <value xsi:type="xsd:string">UNI</value>
    </values>
    <values>
        <field>After_Business_Hours__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>BTD_Type__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Cos_High__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Cos_Low__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Cos_Medium__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Interface_Bandwidth__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Interface_Type__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Mapping_Mode__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>NNI__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>OVC_Type__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Route_Type__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>SVLAN_Id__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Service_Restoration_SLA__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>TPID__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Term__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>UNI_VLAN_Id__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Zone__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Toggle_Modify_Option__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
