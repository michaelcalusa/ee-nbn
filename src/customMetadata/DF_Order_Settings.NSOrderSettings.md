<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>NSOrderSettings</label>
    <protected>false</protected>
    <values>
        <field>Location_API__c</field>
        <value xsi:type="xsd:string">CIS</value>
    </values>
    <values>
        <field>OrderAccBatchSize__c</field>
        <value xsi:type="xsd:string">50</value>
    </values>
    <values>
        <field>OrderAccEventCode__c</field>
        <value xsi:type="xsd:string">ORDACC101</value>
    </values>
    <values>
        <field>OrderAccSource__c</field>
        <value xsi:type="xsd:string">OrderNotifier</value>
    </values>
    <values>
        <field>OrderAckBatchSize__c</field>
        <value xsi:type="xsd:string">50</value>
    </values>
    <values>
        <field>OrderAckEventCode__c</field>
        <value xsi:type="xsd:string">ORDACK101</value>
    </values>
    <values>
        <field>OrderAckSource__c</field>
        <value xsi:type="xsd:string">OrderNotifier</value>
    </values>
    <values>
        <field>OrderCancelBatchSize__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>OrderCancelBusinessDays__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>OrderCancelEventCode__c</field>
         <value xsi:type="xsd:string">SF-NS-000004</value>
    </values>
    <values>
        <field>OrderCancelSource__c</field>
        <value xsi:type="xsd:string">Salesforce</value>
    </values>
    <values>
        <field>OrderCancelledEventCode__c</field>
        <value xsi:type="xsd:string">ORDCANCELLED101</value>
    </values>
    <values>
        <field>OrderCompleteBatchSize__c</field>
        <value xsi:type="xsd:string">50</value>
    </values>
    <values>
        <field>OrderCompleteEventCode__c</field>
        <value xsi:type="xsd:string">ORDCOMP101</value>
    </values>
    <values>
        <field>OrderCompleteSource__c</field>
        <value xsi:type="xsd:string">OrderNotifier</value>
    </values>
    <values>
        <field>OrderConstCompleteBatchSize__c</field>
        <value xsi:type="xsd:string">50</value>
    </values>
    <values>
        <field>OrderConstCompleteEventCode__c</field>
        <value xsi:type="xsd:string">CONCOMP101</value>
    </values>
    <values>
        <field>OrderConstCompleteSource__c</field>
        <value xsi:type="xsd:string">OrderNotifier</value>
    </values>
    <values>
        <field>OrderConstructionBatchSize__c</field>
        <value xsi:type="xsd:string">50</value>
    </values>
    <values>
        <field>OrderConstructionCode__c</field>
        <value xsi:type="xsd:string">CONSTART101</value>
    </values>
    <values>
        <field>OrderConstructionSource__c</field>
        <value xsi:type="xsd:string">OrderNotifier</value>
    </values>
    <values>
        <field>OrderCostVarianceCode__c</field>
        <value xsi:type="xsd:string">SF-NS-000003</value>
    </values>
    <values>
        <field>OrderCreateBatchSize__c</field>
        <value xsi:type="xsd:string">50</value>
    </values>
    <values>
        <field>OrderCreateCode__c</field>
        <value xsi:type="xsd:string">SF-NS-000002</value>
    </values>
    <values>
        <field>OrderCreateSource__c</field>
        <value xsi:type="xsd:string">Salesforce</value>
    </values>
    <values>
        <field>OrderInfoReqReminderBatchSize__c</field>
        <value xsi:type="xsd:string">50</value>
    </values>
    <values>
        <field>OrderInfoReqReminderEventCode__c</field>
        <value xsi:type="xsd:string">INFOREQREMINDER101</value>
    </values>
    <values>
        <field>OrderInfoReqReminderSource__c</field>
        <value xsi:type="xsd:string">OrderNotifier</value>
    </values>
    <values>
        <field>OrderNTDInstallationDateUpdateEventCode__c</field>
        <value xsi:type="xsd:string">APPTSCHED101</value>
    </values>
    <values>
        <field>OrderOnSchedBatchSize__c</field>
        <value xsi:type="xsd:string">50</value>
    </values>
    <values>
        <field>OrderOnSchedEventCode__c</field>
        <value xsi:type="xsd:string">ORDONSCHED101</value>
    </values>
    <values>
        <field>OrderReschedBatchSize__c</field>
        <value xsi:type="xsd:string">50</value>
    </values>
    <values>
        <field>OrderReschedEventCode__c</field>
        <value xsi:type="xsd:string">ORDRESCHED101</value>
    </values>
    <values>
        <field>OrderRspActionCompBatchSize__c</field>
        <value xsi:type="xsd:string">50</value>
    </values>
    <values>
        <field>OrderRspActionCompEventCode__c</field>
        <value xsi:type="xsd:string">RSPACTIONCOMP101</value>
    </values>
    <values>
        <field>OrderRspActionCompSource__c</field>
        <value xsi:type="xsd:string">OrderNotifier</value>
    </values>
    <values>
        <field>OrderRspActionReqdEventCode__c</field>
        <value xsi:type="xsd:string">RSPACTREQRD101</value>
    </values>
    <values>
        <field>OrderSVCBatchSize__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>OrderSVCCOMPCode__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>OrderSVCCOMPSource__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>OrderSchBatchSize__c</field>
        <value xsi:type="xsd:string">50</value>
    </values>
    <values>
        <field>OrderSchEventCode__c</field>
        <value xsi:type="xsd:string">ORDSCHED101</value>
    </values>
    <values>
        <field>OrderSchSource__c</field>
        <value xsi:type="xsd:string">OrderNotifier</value>
    </values>
</CustomMetadata>
