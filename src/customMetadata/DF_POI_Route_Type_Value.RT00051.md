<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>State 2</label>
    <protected>false</protected>
    <values>
        <field>CSA__c</field>
        <value xsi:type="xsd:string">CSA300000000339</value>
    </values>
    <values>
        <field>POI__c</field>
        <value xsi:type="xsd:string">3MEB - PORT MELBOURNE</value>
    </values>
    <values>
        <field>Route_Type_Zone__c</field>
        <value xsi:type="xsd:string">2</value>
    </values>
    <values>
        <field>Route_Type__c</field>
        <value xsi:type="xsd:string">State</value>
    </values>
</CustomMetadata>
