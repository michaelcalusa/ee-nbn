<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>State POI Medium</label>
    <protected>false</protected>
    <values>
        <field>Class_of_Service__c</field>
        <value xsi:type="xsd:string">Medium</value>
    </values>
    <values>
        <field>Frame_Delay_ms__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Frame_Loss_Ratio__c</field>
        <value xsi:type="xsd:string">0.01</value>
    </values>
    <values>
        <field>Inter_Frame_Delay_Variation_ms__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Route_Type__c</field>
        <value xsi:type="xsd:string">State</value>
    </values>
</CustomMetadata>
