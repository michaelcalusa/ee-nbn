<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>OVC 0-99</label>
    <protected>false</protected>
    <values>
        <field>Bandwidth_Bucket__c</field>
        <value xsi:type="xsd:string">0-99</value>
    </values>
    <values>
        <field>Higher_bandwidth_range__c</field>
        <value xsi:type="xsd:double">99.0</value>
    </values>
    <values>
        <field>Lower_Bandwidth_Range__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Product_Name__c</field>
        <value xsi:type="xsd:string">OVC</value>
    </values>
</CustomMetadata>
