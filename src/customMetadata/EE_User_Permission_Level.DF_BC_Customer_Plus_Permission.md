<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>DF BC Customer Plus Permission</label>
    <values>
        <field>Level_of_Permission__c</field>
        <value xsi:type="xsd:double">4.0</value>
    </values>
    <values>
        <field>Permissions__c</field>
        <value xsi:type="xsd:string">Ability to place Order</value>
    </values>
</CustomMetadata>
