<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ETM042</label>
    <protected>false</protected>
    <values>
        <field>Entity_Code__c</field>
        <value xsi:type="xsd:string">STD</value>
    </values>
    <values>
        <field>Entity_Type__c</field>
        <value xsi:type="xsd:string">State Government Discretionary Services Management Trust</value>
    </values>
    <values>
        <field>Entity__c</field>
        <value xsi:type="xsd:string">Trust</value>
    </values>
</CustomMetadata>
