<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ETM066</label>
    <protected>false</protected>
    <values>
        <field>Entity_Code__c</field>
        <value xsi:type="xsd:string">CSS</value>
    </values>
    <values>
        <field>Entity_Type__c</field>
        <value xsi:type="xsd:string">Commonwealth Government Non-Regulated Super Fund</value>
    </values>
    <values>
        <field>Entity__c</field>
        <value xsi:type="xsd:string">Superannuation</value>
    </values>
</CustomMetadata>
