<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Accredited Training Marketing</label>
    <protected>false</protected>
    <values>
        <field>Public_Groups__c</field>
        <value xsi:type="xsd:string">ICT Stay Updated,ICT Training Material,ICT Technical &amp; Support,ICT Marketing &amp; Brand,ICT Pre-sales Support</value>
    </values>
</CustomMetadata>
