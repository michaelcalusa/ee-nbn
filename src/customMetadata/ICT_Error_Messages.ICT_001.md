<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ICT - 001</label>
    <protected>false</protected>
    <values>
        <field>Component__c</field>
        <value xsi:type="xsd:string">Login</value>
    </values>
    <values>
        <field>Error_Code__c</field>
        <value xsi:type="xsd:string">001</value>
    </values>
    <values>
        <field>Error_Message__c</field>
        <value xsi:type="xsd:string">Your access to the ICT Channel Portal has been suspended. Please contact &lt;a href=&quot;mailto:ICTChannel@nbnco.com.au&quot;&gt;ICTChannel@nbnco.com.au&lt;/a&gt; for more information.</value>
    </values>
    <values>
        <field>Error__c</field>
        <value xsi:type="xsd:string">Inactive User</value>
    </values>
</CustomMetadata>
