<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>TestDiagnostics</label>
    <protected>false</protected>
    <values>
        <field>Jigsaw_Integration_Type__c</field>
        <value xsi:type="xsd:string">Test and Diagnostics</value>
    </values>
    <values>
        <field>Jigsaw_Number_Of_Days__c</field>
        <value xsi:type="xsd:double">7.0</value>
    </values>
</CustomMetadata>
