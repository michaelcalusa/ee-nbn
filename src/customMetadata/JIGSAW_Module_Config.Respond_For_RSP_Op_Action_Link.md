<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Respond For RSP Op Action Link</label>
    <protected>false</protected>
    <values>
        <field>Component_Name__c</field>
        <value xsi:type="xsd:string">JIGSAW_Respond_For_RSP</value>
    </values>
    <values>
        <field>CurrentUserAsAssigneeRequired__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Enabled__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IgnoreLoggedinUserAssigneeCheck__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Incident_Current_Status__c</field>
        <value xsi:type="xsd:string">Pending</value>
    </values>
    <values>
        <field>Technology_Type__c</field>
        <value xsi:type="xsd:string">FTTN,FTTB,NHAS,NHUR</value>
    </values>
    <values>
        <field>User_Message__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Visibility_Method__c</field>
        <value xsi:type="xsd:string">displayRespondForRSP</value>
    </values>
</CustomMetadata>
