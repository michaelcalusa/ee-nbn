<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Test Manager Link Module</label>
    <protected>false</protected>
    <values>
        <field>Component_Name__c</field>
        <value xsi:type="xsd:string">testManagerLink</value>
    </values>
    <values>
        <field>CurrentUserAsAssigneeRequired__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Enabled__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IgnoreLoggedinUserAssigneeCheck__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Incident_Current_Status__c</field>
        <value xsi:type="xsd:string">New incident,Closed, Resolution Rejected, In Progress, Incident Accepted, Awaiting Technician, Tech Onsite, Tech Offsite , RSP Responded, Resolved, Resolution Rejected, Pending, On Hold, Cancelled, Rejected</value>
    </values>
    <values>
        <field>Technology_Type__c</field>
        <value xsi:type="xsd:string">FTTN, FTTB</value>
    </values>
    <values>
        <field>User_Message__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Visibility_Method__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
