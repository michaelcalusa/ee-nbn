<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>OPCAT-042</label>
    <protected>false</protected>
    <values>
        <field>Technology__c</field>
        <value xsi:type="xsd:string">LTSS</value>
    </values>
    <values>
        <field>Tier1__c</field>
        <value xsi:type="xsd:string">Customer Incident</value>
    </values>
    <values>
        <field>Tier2__c</field>
        <value xsi:type="xsd:string">SNTD</value>
    </values>
    <values>
        <field>Tier3__c</field>
        <value xsi:type="xsd:string">NTD offline</value>
    </values>
</CustomMetadata>
