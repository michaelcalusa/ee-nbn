<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>OPCAT-066</label>
    <protected>false</protected>
    <values>
        <field>Technology__c</field>
        <value xsi:type="xsd:string">NHAS</value>
    </values>
    <values>
        <field>Tier1__c</field>
        <value xsi:type="xsd:string">Customer Incident - Priority Assist</value>
    </values>
    <values>
        <field>Tier2__c</field>
        <value xsi:type="xsd:string">HFC NTD</value>
    </values>
    <values>
        <field>Tier3__c</field>
        <value xsi:type="xsd:string">Service Degradation - Slow Speed</value>
    </values>
</CustomMetadata>
