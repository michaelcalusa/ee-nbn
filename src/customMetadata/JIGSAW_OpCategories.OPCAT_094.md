<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>OPCAT-094</label>
    <protected>false</protected>
    <values>
        <field>Technology__c</field>
        <value xsi:type="xsd:string">FTTN/B</value>
    </values>
    <values>
        <field>Tier1__c</field>
        <value xsi:type="xsd:string">Customer Incident - Priority Assist</value>
    </values>
    <values>
        <field>Tier2__c</field>
        <value xsi:type="xsd:string">Dropout</value>
    </values>
    <values>
        <field>Tier3__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
