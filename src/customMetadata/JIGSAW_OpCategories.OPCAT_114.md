<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>OPCAT-114</label>
    <protected>false</protected>
    <values>
        <field>Technology__c</field>
        <value xsi:type="xsd:string">FTTP</value>
    </values>
    <values>
        <field>Tier1__c</field>
        <value xsi:type="xsd:string">Customer Incident</value>
    </values>
    <values>
        <field>Tier2__c</field>
        <value xsi:type="xsd:string">NTD Patch lead</value>
    </values>
    <values>
        <field>Tier3__c</field>
        <value xsi:type="xsd:string">High attenuation</value>
    </values>
</CustomMetadata>
