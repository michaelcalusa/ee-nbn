<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>DP1019</label>
    <protected>false</protected>
    <values>
        <field>Field_Name__c</field>
        <value xsi:type="xsd:string">SDP</value>
    </values>
    <values>
        <field>Jigsaw_Display_Field__c</field>
        <value xsi:type="xsd:string">Telstra</value>
    </values>
    <values>
        <field>Maximo_DP_Code__c</field>
        <value xsi:type="xsd:string">DP1019</value>
    </values>
</CustomMetadata>
