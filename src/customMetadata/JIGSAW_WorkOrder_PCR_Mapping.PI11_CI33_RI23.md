<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>PI11 / CI33 / RI23</label>
    <protected>false</protected>
    <values>
        <field>Cause_Code__c</field>
        <value xsi:type="xsd:string">CI33</value>
    </values>
    <values>
        <field>Cause_Description__c</field>
        <value xsi:type="xsd:string">Material Shortage</value>
    </values>
    <values>
        <field>Problem_Code__c</field>
        <value xsi:type="xsd:string">PI11</value>
    </values>
    <values>
        <field>Problem_Description__c</field>
        <value xsi:type="xsd:string">WORKFORCE DELAY</value>
    </values>
    <values>
        <field>Remedy_Code__c</field>
        <value xsi:type="xsd:string">RI23</value>
    </values>
    <values>
        <field>Remedy_Description__c</field>
        <value xsi:type="xsd:string">Procurement</value>
    </values>
</CustomMetadata>
