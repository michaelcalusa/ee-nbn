<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>PI13 / CI31 / RI19</label>
    <protected>false</protected>
    <values>
        <field>Cause_Code__c</field>
        <value xsi:type="xsd:string">CI31</value>
    </values>
    <values>
        <field>Cause_Description__c</field>
        <value xsi:type="xsd:string">Long NBN Call Queues</value>
    </values>
    <values>
        <field>Problem_Code__c</field>
        <value xsi:type="xsd:string">PI13</value>
    </values>
    <values>
        <field>Problem_Description__c</field>
        <value xsi:type="xsd:string">NBN Delay</value>
    </values>
    <values>
        <field>Remedy_Code__c</field>
        <value xsi:type="xsd:string">RI19</value>
    </values>
    <values>
        <field>Remedy_Description__c</field>
        <value xsi:type="xsd:string">NBN Issue</value>
    </values>
</CustomMetadata>
