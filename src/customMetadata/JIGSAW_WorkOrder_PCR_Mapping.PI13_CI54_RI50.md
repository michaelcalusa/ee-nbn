<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>PI13 / CI54 / RI50</label>
    <protected>false</protected>
    <values>
        <field>Cause_Code__c</field>
        <value xsi:type="xsd:string">CI54</value>
    </values>
    <values>
        <field>Cause_Description__c</field>
        <value xsi:type="xsd:string">Lead Time</value>
    </values>
    <values>
        <field>Problem_Code__c</field>
        <value xsi:type="xsd:string">PI13</value>
    </values>
    <values>
        <field>Problem_Description__c</field>
        <value xsi:type="xsd:string">NBN Delay</value>
    </values>
    <values>
        <field>Remedy_Code__c</field>
        <value xsi:type="xsd:string">RI50</value>
    </values>
    <values>
        <field>Remedy_Description__c</field>
        <value xsi:type="xsd:string">Insufficient Lead Time</value>
    </values>
</CustomMetadata>
