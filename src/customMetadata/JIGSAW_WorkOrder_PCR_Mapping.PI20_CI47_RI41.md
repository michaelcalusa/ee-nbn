<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>PI20 / CI47 / RI41</label>
    <protected>false</protected>
    <values>
        <field>Cause_Code__c</field>
        <value xsi:type="xsd:string">CI47</value>
    </values>
    <values>
        <field>Cause_Description__c</field>
        <value xsi:type="xsd:string">Restricted workspace access required</value>
    </values>
    <values>
        <field>Problem_Code__c</field>
        <value xsi:type="xsd:string">PI20</value>
    </values>
    <values>
        <field>Problem_Description__c</field>
        <value xsi:type="xsd:string">CUSTOMER DELAY</value>
    </values>
    <values>
        <field>Remedy_Code__c</field>
        <value xsi:type="xsd:string">RI41</value>
    </values>
    <values>
        <field>Remedy_Description__c</field>
        <value xsi:type="xsd:string">Permission Required</value>
    </values>
</CustomMetadata>
