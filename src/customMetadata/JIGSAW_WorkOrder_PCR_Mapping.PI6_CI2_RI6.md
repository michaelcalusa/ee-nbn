<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>PI6 / CI2 / RI6</label>
    <protected>false</protected>
    <values>
        <field>Cause_Code__c</field>
        <value xsi:type="xsd:string">CI2</value>
    </values>
    <values>
        <field>Cause_Description__c</field>
        <value xsi:type="xsd:string">Attenuation or impedance incorrect</value>
    </values>
    <values>
        <field>Problem_Code__c</field>
        <value xsi:type="xsd:string">PI6</value>
    </values>
    <values>
        <field>Problem_Description__c</field>
        <value xsi:type="xsd:string">Loading Coil</value>
    </values>
    <values>
        <field>Remedy_Code__c</field>
        <value xsi:type="xsd:string">RI6</value>
    </values>
    <values>
        <field>Remedy_Description__c</field>
        <value xsi:type="xsd:string">Removed</value>
    </values>
</CustomMetadata>
