<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>JM002</label>
    <protected>false</protected>
    <values>
        <field>Communiity__c</field>
        <value xsi:type="xsd:string">customer</value>
    </values>
    <values>
        <field>EUAP_Role__c</field>
        <value xsi:type="xsd:string">Customer Onboarding</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Profile_Precedence__c</field>
        <value xsi:type="xsd:double">2.0</value>
    </values>
    <values>
        <field>Salesforce_Permission_Set__c</field>
        <value xsi:type="xsd:string">nbn_Customer_Centre_App_User</value>
    </values>
    <values>
        <field>Salesforce_Profile__c</field>
        <value xsi:type="xsd:string">RSP Customer Community Plus Profile</value>
    </values>
    <values>
        <field>User_Name_Postfix__c</field>
        <value xsi:type="xsd:string">nbnRSP</value>
    </values>
</CustomMetadata>
