<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>JM015</label>
    <protected>false</protected>
    <values>
        <field>Communiity__c</field>
        <value xsi:type="xsd:string">support</value>
    </values>
    <values>
        <field>EUAP_Role__c</field>
        <value xsi:type="xsd:string">Service Portal User</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Profile_Precedence__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>Salesforce_Permission_Set__c</field>
        <value xsi:type="xsd:string">RSP_Knowledge_and_Learning</value>
    </values>
    <values>
        <field>Salesforce_Profile__c</field>
        <value xsi:type="xsd:string">RSP Customer Community Plus Login User</value>
    </values>
    <values>
        <field>User_Name_Postfix__c</field>
        <value xsi:type="xsd:string">nbnRSP</value>
    </values>
</CustomMetadata>
