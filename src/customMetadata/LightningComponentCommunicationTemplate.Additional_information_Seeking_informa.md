<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Additional information - Seeking informa</label>
    <protected>false</protected>
    <values>
        <field>Category__c</field>
        <value xsi:type="xsd:string">Request_More_Information</value>
    </values>
    <values>
        <field>Is_Accept__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Template_Name__c</field>
        <value xsi:type="xsd:string">Additional information - Seeking information regarding exact fault</value>
    </values>
    <values>
        <field>Template_Value__c</field>
        <value xsi:type="xsd:string">Hi Team,
Test and Diagnostic results of the service within nbn&apos;s network have proven successful.
Our systems indicate service is in sync with CPE MAC (xx:xx:xx:xx:xx:xx).
Also, we see bidirectional traffic without any faults on the line and the speeds customer is receiving are also within specs.
Fault notes do not indicate the fault experienced by the customer while using the line.
Please advise what fault is so we can investigate the issue accordingly.
Please also provide any further testing / troubleshooting that have been performed to isolate the issue in nbn&apos;s network.
Thank You
nbn Service Assurance</value>
    </values>
</CustomMetadata>
