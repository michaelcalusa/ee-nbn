<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>DLM – Change Applied Overnight</label>
    <protected>false</protected>
    <values>
        <field>Category__c</field>
        <value xsi:type="xsd:string">Resolve_incident</value>
    </values>
    <values>
        <field>Is_Accept__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Template_Description__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Template_Name__c</field>
        <value xsi:type="xsd:string">DLM – Change Applied Overnight</value>
    </values>
    <values>
        <field>Template_Value__c</field>
        <value xsi:type="xsd:string">Hi Team

We have investigated this incident and our testing indicates a high likelihood that a change applied to the network configuration last night by Dynamic Line Management (DLM) will have resolved this customers issue.

Details of the DLM change can be seen on the AVC Diagnostics page in the Service Portal or via B2B. 

Reject Notifications require supporting information that applies to information post the DLM network configuration changes to assist with further investigation activities.

This Incident will be closed if above data has not been provided or a response is not received within 2 business days.

Regards,
nbn Service Assurance</value>
    </values>
</CustomMetadata>
