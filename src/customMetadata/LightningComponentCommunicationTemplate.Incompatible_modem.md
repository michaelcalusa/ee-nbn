<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Incompatible modem</label>
    <protected>false</protected>
    <values>
        <field>Category__c</field>
        <value xsi:type="xsd:string">Request_More_Information</value>
    </values>
    <values>
        <field>Is_Accept__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Template_Name__c</field>
        <value xsi:type="xsd:string">Incompatible modem</value>
    </values>
    <values>
        <field>Template_Value__c</field>
        <value xsi:type="xsd:string">Hi Team,
Test and Diagnostic results of the service within nbn&apos;s Network have proven successful.
Our investigation of this issue has detected a CPE related problem.
Our systems indicate the modem connected to the line is an ADSL2+ capable modem and not VDSL2 RTX compatible.
Kindly retest with a VDSL2 RTX compatible modem, also share the below information:
Mac address of the old CPE
MAC address of the new CPE
Model name / number of alternative CPE
Serial Number of the new CPE
We require the above information before this can be investigated further.
Please also provide any further testing / troubleshooting that have been performed to isolate the issue in nbn&apos;s network.
Thank You 
nbn Service Assurance</value>
    </values>
</CustomMetadata>
