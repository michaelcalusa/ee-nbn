<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Modem firmware is not up to date</label>
    <protected>false</protected>
    <values>
        <field>Category__c</field>
        <value xsi:type="xsd:string">Request_More_Information</value>
    </values>
    <values>
        <field>Is_Accept__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Template_Name__c</field>
        <value xsi:type="xsd:string">Modem firmware is not up to date</value>
    </values>
    <values>
        <field>Template_Value__c</field>
        <value xsi:type="xsd:string">Hi Team,
Test and Diagnostic results of the service within nbn&apos;s Network have proven successful.
Our investigation of this issue has detected a CPE related problem.
The service is not in sync and we see Modem firmware is not up to date. Please verify if CPE firmware is up to date.
In addition to that please have the end user test with an alternative CPE that is VDSL2 RTX compatible with latest firmware.
Also, if you believe the modem is VDSL2 RTX compatible please share the below information:
Mac address of the old CPE
Serial number of the old modem:
Mac address of the New CPE
Serial Number of the new CPE
We require the above information before this can be investigated further.
Please also provide any further testing / troubleshooting that have been performed to isolate the issue in nbn&apos;s network.
Thank You 
nbn Service Assurance</value>
    </values>
</CustomMetadata>
