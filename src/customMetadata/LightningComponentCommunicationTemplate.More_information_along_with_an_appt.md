<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>More information along with an appt</label>
    <protected>false</protected>
    <values>
        <field>Category__c</field>
        <value xsi:type="xsd:string">Request_More_Information</value>
    </values>
    <values>
        <field>Is_Accept__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Template_Name__c</field>
        <value xsi:type="xsd:string">More information along with an appointment</value>
    </values>
    <values>
        <field>Template_Value__c</field>
        <value xsi:type="xsd:string">Hi Team,
Our testing has Indicated CPE related issue. Please ensure that all cables are connected securely to the modem.
Also perform an Isolation test , unplug all equipment connected to a phone socket and connect modem directly to the main socket.
If the issue still persist, we would request you to please reserve an appointment.
☆Mac address
☆Serial Number of the CPE
☆Model name / Mac address of alternative CPE(if tested)
In order for us to go ahead with the Appointment, it is necessary for you to provide us with the above requested details and perform the isolation test or else it will cause delay in Resolution.
Thank You 
nbn Service Assurance</value>
    </values>
</CustomMetadata>
