<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>NEEA9012</label>
    <protected>false</protected>
    <values>
        <field>Category__c</field>
        <value xsi:type="xsd:string">Place_Incident_On_Hold</value>
    </values>
    <values>
        <field>Is_Accept__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Template_Name__c</field>
        <value xsi:type="xsd:string">An External Event declared in a Area (NEEA9012)</value>
    </values>
    <values>
        <field>Template_Description__c</field>
        <value xsi:type="xsd:string">NEEA9012 - An External Event declared in a Area</value>
    </values>
</CustomMetadata>
