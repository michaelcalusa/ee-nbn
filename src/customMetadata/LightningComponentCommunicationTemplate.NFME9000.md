<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>NFME9000</label>
    <protected>false</protected>
    <values>
        <field>Category__c</field>
        <value xsi:type="xsd:string">Place_Incident_On_Hold</value>
    </values>
    <values>
        <field>Is_Accept__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Template_Description__c</field>
        <value xsi:type="xsd:string">NFME9000 - An Force Majeure event has been declared in the Area</value>
    </values>
    <values>
        <field>Template_Name__c</field>
        <value xsi:type="xsd:string">An Force Majeure event has been declared in the Area (NFME9000)</value>
    </values>
    <values>
        <field>Template_Value__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
