<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>NKNF9100</label>
    <protected>false</protected>
    <values>
        <field>Category__c</field>
        <value xsi:type="xsd:string">Place_Incident_On_Hold</value>
    </values>
    <values>
        <field>Is_Accept__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Template_Description__c</field>
        <value xsi:type="xsd:string">NKNF9100-There is a known NBN network fault</value>
    </values>
    <values>
        <field>Template_Name__c</field>
        <value xsi:type="xsd:string">There is a known NBN network fault (NKNF9100)</value>
    </values>
    <values>
        <field>Template_Value__c</field>
        <value xsi:type="xsd:string">Hi Team,

Thank you for contacting nbn Service Assurance.

This service has been affected by a Network Outage/Infrastructure Restoration in the area.

Please follow {0} which has been created for the network fault under the service which is listed as part of the outage.

Outage details will be updated through this ticket.

This Incident has now been placed On Hold and future updates will be provided through {0}.

 __ 

Regards,
nbn Service Assurance</value>
    </values>
</CustomMetadata>
