<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>NKSF9100</label>
    <protected>false</protected>
    <values>
        <field>Category__c</field>
        <value xsi:type="xsd:string">Place_Incident_On_Hold</value>
    </values>
    <values>
        <field>Is_Accept__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Template_Description__c</field>
        <value xsi:type="xsd:string">NKSF9100-There is a known NBN service fault</value> 
    </values>
    <values>
        <field>Template_Name__c</field>
        <value xsi:type="xsd:string">There is a known NBN service fault (NKSF9100)</value>
    </values>
    <values>
        <field>Template_Value__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
