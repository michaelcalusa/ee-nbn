<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>NNIT9700</label>
    <protected>false</protected>
    <values>
        <field>Category__c</field>
        <value xsi:type="xsd:string">Place_Incident_On_Hold</value>
    </values>
    <values>
        <field>Is_Accept__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Template_Description__c</field>
        <value xsi:type="xsd:string">NNIT9700 - NBN IT Issue</value>
    </values>
    <values>
        <field>Template_Name__c</field>
        <value xsi:type="xsd:string">NBN IT Issue There is an NBN Co IT issue which is affecting the processing of this incident. We will provide an update as soon as possible (NNIT9700)</value>
    </values>
    <values>
        <field>Template_Value__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
