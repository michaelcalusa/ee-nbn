<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Verify the MAC Address with End User</label>
    <protected>false</protected>
    <values>
        <field>Category__c</field>
        <value xsi:type="xsd:string">Request_More_Information</value>
    </values>
    <values>
        <field>Is_Accept__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Template_Name__c</field>
        <value xsi:type="xsd:string">Verify the MAC Address with End User</value>
    </values>
    <values>
        <field>Template_Value__c</field>
        <value xsi:type="xsd:string">Hi Team,
We can see a MAC address being learnt on the port.
Request you to check with the End User and provide the below information:
MAC address of the CPE
Serial Number of the CPE
Model name of the CPE
We require the above information before this can be investigated further.
Please also provide any further testing / troubleshooting that have been performed to isolate the issue in nbn&apos;s network.
Thank You
nbn Service Assurance</value>
    </values>
</CustomMetadata>
