<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>LCE-0001</label>
    <protected>false</protected>
    <values>
        <field>ErrorCode__c</field>
        <value xsi:type="xsd:string">0001</value>
    </values>
    <values>
        <field>ErrorMessage__c</field>
        <value xsi:type="xsd:string">We can&apos;t find this address in our records. If the premises is new, you might need to register the address &lt;a href=&quot;https://www1.nbnco.com.au/mdu/terms_and_conditions&quot; target=&quot;_blank&quot;&gt;here&lt;/a&gt;</value>
    </values>
    <values>
        <field>LightningComponent__c</field>
        <value xsi:type="xsd:string">LocationSearch</value>
    </values>
</CustomMetadata>
