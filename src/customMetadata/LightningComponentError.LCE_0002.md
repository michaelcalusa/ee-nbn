<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>LCE-0002</label>
    <protected>false</protected>
    <values>
        <field>ErrorCode__c</field>
        <value xsi:type="xsd:string">0002</value>
    </values>
    <values>
        <field>ErrorMessage__c</field>
        <value xsi:type="xsd:string">We are currently experiencing technical issues. Please try again later.</value>
    </values>
    <values>
        <field>LightningComponent__c</field>
        <value xsi:type="xsd:string">LocationSearch</value>
    </values>
</CustomMetadata>
