<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Mar Auto Qualify Emails</label>
    <protected>false</protected>
    <values>
        <field>Value__c</field>
        <value xsi:type="xsd:string">shuoli@nbnco.com.au,arpitnarain@nbnco.com.au,CSGoldCoastMigrationTeam@nbnco.com.au</value>
    </values>
</CustomMetadata>
