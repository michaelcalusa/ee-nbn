<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>BEM060</label>
    <protected>false</protected>
    <values>
        <field>CRMOD_Entity_Type__c</field>
        <value xsi:type="xsd:string">Trust</value>
    </values>
    <values>
        <field>Class__c</field>
        <value xsi:type="xsd:string">Class1/2</value>
    </values>
    <values>
        <field>Entity_Code__c</field>
        <value xsi:type="xsd:string">PUT</value>
    </values>
    <values>
        <field>Entity__c</field>
        <value xsi:type="xsd:string">Listed Public Unit Trust</value>
    </values>
</CustomMetadata>
