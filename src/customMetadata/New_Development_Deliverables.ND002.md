<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ND002</label>
    <protected>false</protected>
    <values>
        <field>Build_Type__c</field>
        <value xsi:type="xsd:string">Lead-In Conduit</value>
    </values>
    <values>
        <field>Class__c</field>
        <value xsi:type="xsd:string">Class1/2</value>
    </values>
    <values>
        <field>Deliverables__c</field>
        <value xsi:type="xsd:string">Service Plan</value>
    </values>
    <values>
        <field>Explanation__c</field>
        <value xsi:type="xsd:string">&lt;p&gt;
&lt;b&gt;What is it?&lt;/b&gt;&lt;br/&gt;
The communications plan, or services plan, is an illustration of the utilities (eg. communications lead-in) to be installed in the services trench within the development. 
&lt;br/&gt;&lt;br/&gt;
Please upload a plan that clearly shows property boundary and where the lead-in is intended to be built.  
&lt;br/&gt;&lt;br/&gt;
&lt;b&gt;What’s next?&lt;/b&gt;&lt;br/&gt;
&lt;b&gt;nbn&lt;/b&gt; will review your documents and application details to determine if the proposed service trench is suitable to connect your development to the &lt;b&gt;nbn&lt;/b&gt;™ broadband access network. 
&lt;br/&gt;&lt;br/&gt;
An assessment outcome will be emailed to you shortly. 
&lt;/p&gt;</value>
    </values>
    <values>
        <field>Is_Pit_and_Pipe_Private__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Service_Delivery__c</field>
        <value xsi:type="xsd:string">SD-2</value>
    </values>
    <values>
        <field>Subject__c</field>
        <value xsi:type="xsd:string">1. Service Plan</value>
    </values>
    <values>
        <field>Technical_Assessment_Required__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
