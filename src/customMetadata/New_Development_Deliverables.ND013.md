<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ND013</label>
    <protected>false</protected>
    <values>
        <field>Build_Type__c</field>
        <value xsi:type="xsd:string">Pit and Pipe</value>
    </values>
    <values>
        <field>Class__c</field>
        <value xsi:type="xsd:string">Class3/4</value>
    </values>
    <values>
        <field>Deliverables__c</field>
        <value xsi:type="xsd:string">Pit &amp; Pipe Design</value>
    </values>
    <values>
        <field>Explanation__c</field>
        <value xsi:type="xsd:string">&lt;p&gt;
&lt;b&gt;What is it?&lt;/b&gt;&lt;br/&gt;
The pre-construct design should be completed before construction has started on the site. 
&lt;br/&gt;&lt;br/&gt;
Please upload the designs that meet the &lt;a href=&quot;https://www.nbnco.com.au/develop-or-plan-with-the-nbn/new-developments/design-build-install/pit-and-pipe-build-process/hdmu-pit-and-pipe-guidelines.html&quot; target=&quot;_blank&quot;&gt;HMDU pit and pipe guidelines&lt;/a&gt;. &lt;b&gt;nbn&lt;/b&gt; will review these designs and once approved, you can commence the build of the pit and pipes works. 
&lt;br/&gt;&lt;br/&gt;
&lt;b&gt;What’s next?&lt;/b&gt;&lt;br/&gt;
&lt;b&gt;nbn&lt;/b&gt; will review your documents to ensure that your construction plans will meet &lt;b&gt;nbn&lt;/b&gt;’s standards to help you avoid delays and remediation. &lt;a href=&quot;https://www.nbnco.com.au/develop-or-plan-with-the-nbn/new-developments/design-build-install/design-support-tools.html&quot; target=&quot;_blank&quot;&gt;Design support tools&lt;/a&gt; are available to assist you with creating the correct design files.
&lt;br/&gt;&lt;br/&gt;
An assessment will be emailed to you within 20 business days. 
&lt;/p&gt;</value>
    </values>
    <values>
        <field>Is_Pit_and_Pipe_Private__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Service_Delivery__c</field>
        <value xsi:type="xsd:string">SD-2</value>
    </values>
    <values>
        <field>Subject__c</field>
        <value xsi:type="xsd:string">2. Pre-construct designs</value>
    </values>
    <values>
        <field>Technical_Assessment_Required__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
