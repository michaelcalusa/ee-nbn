<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ND014</label>
    <protected>false</protected>
    <values>
        <field>Build_Type__c</field>
        <value xsi:type="xsd:string">Pit and Pipe</value>
    </values>
    <values>
        <field>Class__c</field>
        <value xsi:type="xsd:string">Class3/4</value>
    </values>
    <values>
        <field>Deliverables__c</field>
        <value xsi:type="xsd:string">Insurance</value>
    </values>
    <values>
        <field>Explanation__c</field>
        <value xsi:type="xsd:string">&lt;p&gt;
&lt;b&gt;What is it?&lt;/b&gt;&lt;br/&gt;
The public liability insurance must be provided before the pit and pipe works is constructed. 
&lt;br/&gt;&lt;br/&gt;
It must:
&lt;br/&gt;
&lt;ul style=&quot;list-style: unset;&quot;&gt;
&lt;li&gt;Show a limit of indemnity of $10 million for any one occurrence&lt;/li&gt;
&lt;li&gt;Cover legal liability for personal injury and property damage in connection with the developer’s activities&lt;/li&gt;
&lt;li&gt;Be in place from the date this development application was submitted (the date of commencement of the Online Terms) until the expiry of the Defects liability period. For the dates, please refer to the Developer Application Online Terms.&lt;/li&gt;
&lt;/ul&gt;&lt;/br&gt;
&lt;b&gt;What’s next?&lt;/b&gt;&lt;br/&gt;
&lt;b&gt;nbn&lt;/b&gt; will add this document to your account. 
&lt;/p&gt;</value>
    </values>
    <values>
        <field>Is_Pit_and_Pipe_Private__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Service_Delivery__c</field>
        <value xsi:type="xsd:string">SD-2</value>
    </values>
    <values>
        <field>Subject__c</field>
        <value xsi:type="xsd:string">3. Insurance</value>
    </values>
    <values>
        <field>Technical_Assessment_Required__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
