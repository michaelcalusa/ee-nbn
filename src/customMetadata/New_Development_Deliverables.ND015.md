<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ND015</label>
    <protected>false</protected>
    <values>
        <field>Build_Type__c</field>
        <value xsi:type="xsd:string">Pit and Pipe</value>
    </values>
    <values>
        <field>Class__c</field>
        <value xsi:type="xsd:string">Class3/4</value>
    </values>
    <values>
        <field>Deliverables__c</field>
        <value xsi:type="xsd:string">As-built design</value>
    </values>
    <values>
        <field>Explanation__c</field>
        <value xsi:type="xsd:string">&lt;p&gt;
&lt;b&gt;What is it?&lt;/b&gt;&lt;br/&gt;
The as-built designs should be completed before the Notice of Practical Completion (PCN) is submitted for this stage. 
&lt;br/&gt;&lt;br/&gt;
Please upload the designs that meet the &lt;a href=&quot;https://www.nbnco.com.au/develop-or-plan-with-the-nbn/new-developments/design-build-install/upload-designs/as-built-handover-checklist.html&quot; target=&quot;_blank&quot;&gt;as-built handover checklist&lt;/a&gt; to avoid delays and remediation
&lt;br/&gt;&lt;br/&gt;
&lt;b&gt;What’s next?&lt;/b&gt;&lt;br/&gt;
&lt;b&gt;nbn&lt;/b&gt; will review your documents to ensure that the pit and pipe works has been constructed to &lt;b&gt;nbn&lt;/b&gt;’s standards.
&lt;br/&gt;&lt;br/&gt;
An assessment will be emailed to you within 20 business days.  
&lt;br/&gt;&lt;br/&gt;
If the designs are rejected, &lt;b&gt;nbn&lt;/b&gt; will email you the corrections required and the updated design must be re-submitted within 10 business days. 
&lt;/p&gt;</value>
    </values>
    <values>
        <field>Is_Pit_and_Pipe_Private__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Service_Delivery__c</field>
        <value xsi:type="xsd:string">SD-2</value>
    </values>
    <values>
        <field>Subject__c</field>
        <value xsi:type="xsd:string">4. As Built</value>
    </values>
    <values>
        <field>Technical_Assessment_Required__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
