<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ND016</label>
    <protected>false</protected>
    <values>
        <field>Build_Type__c</field>
        <value xsi:type="xsd:string">Pit and Pipe</value>
    </values>
    <values>
        <field>Class__c</field>
        <value xsi:type="xsd:string">Class3/4</value>
    </values>
    <values>
        <field>Deliverables__c</field>
        <value xsi:type="xsd:string">PCN</value>
    </values>
    <values>
        <field>Explanation__c</field>
        <value xsi:type="xsd:string">&lt;p&gt;
&lt;b&gt;What is it?&lt;/b&gt;&lt;br/&gt;
A Notice of Practical Completion (PCN) is a notification to &lt;b&gt;nbn&lt;/b&gt; that you are ready for a site inspection as you consider Practical Completion has been achieved and to transfer the ownership of the pit and pipe works to &lt;b&gt;nbn&lt;/b&gt;. 
&lt;br/&gt;&lt;br/&gt;
Check the PCN checklist to ensure that &lt;a href=&quot;https://www.nbnco.com.au/develop-or-plan-with-the-nbn/new-developments/design-build-install/upload-designs/hmdu-and-hybrid-mdu-pcn-checklist.html&quot; target=&quot;_blank&quot;&gt;&lt;b&gt;nbn&lt;/b&gt;’s standards&lt;/a&gt; have been met before submitting your PCN.
&lt;br/&gt;&lt;br/&gt;
&lt;b&gt;What’s next?&lt;/b&gt;&lt;br/&gt;
&lt;b&gt;nbn&lt;/b&gt; will contact you to organise a site inspection to take place within 20 business days. 
&lt;br/&gt;&lt;br/&gt;
If &lt;b&gt;nbn&lt;/b&gt; determines that the Practical Completion has been achieved, a Certificate of Practical Completion (PCC) will be issued. &lt;b&gt;nbn&lt;/b&gt; will organise the handover of the ownership of the pit and pipe works.
&lt;br/&gt;&lt;br/&gt;
If &lt;b&gt;nbn&lt;/b&gt; determines that the Practical Completion has not been achieved, you may be charged reasonable costs for a secondary inspection and the handover will be delayed.
&lt;/p&gt;</value>
    </values>
    <values>
        <field>Is_Pit_and_Pipe_Private__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Service_Delivery__c</field>
        <value xsi:type="xsd:string">SD-2</value>
    </values>
    <values>
        <field>Subject__c</field>
        <value xsi:type="xsd:string">5. Notice of Practical Completion (PCN)</value>
    </values>
    <values>
        <field>Technical_Assessment_Required__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
