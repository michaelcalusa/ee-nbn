<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ND044</label>
    <protected>false</protected>
    <values>
        <field>Build_Type__c</field>
        <value xsi:type="xsd:string">Pit and Pipe</value>
    </values>
    <values>
        <field>Class__c</field>
        <value xsi:type="xsd:string">Class3/4</value>
    </values>
    <values>
        <field>Deliverables__c</field>
        <value xsi:type="xsd:string">Master Plan</value>
    </values>
    <values>
        <field>Explanation__c</field>
        <value xsi:type="xsd:string">&lt;p&gt; 
&lt;b&gt;What is it?&lt;/b&gt;&lt;br/&gt; 
The master development plan is an illustration showing the area being developed in relation to this application. You may upload the latest version of the plan available that shows an indication of what will be built. 
&lt;br/&gt;&lt;br/&gt; 
&lt;b&gt;What’s next?&lt;/b&gt;&lt;br/&gt; 
&lt;b&gt;nbn&lt;/b&gt; will review your documents and your application details to determine the location and boundary of the development, the number of stages and where &lt;b&gt;nbn&lt;/b&gt; will enter and connect your development into the &lt;b&gt;nbn&lt;/b&gt;™ broadband access network. 
&lt;br/&gt;&lt;br/&gt; 
An assessment outcome will be emailed to you within 20 business days. 
&lt;/p&gt;</value>
    </values>
    <values>
        <field>Is_Pit_and_Pipe_Private__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Service_Delivery__c</field>
        <value xsi:type="xsd:string">SD-1</value>
    </values>
    <values>
        <field>Subject__c</field>
        <value xsi:type="xsd:string">1. Master Plan</value>
    </values>
    <values>
        <field>Technical_Assessment_Required__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
