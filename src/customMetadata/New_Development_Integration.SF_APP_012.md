<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>SF-APP-012</label>
    <protected>false</protected>
    <values>
        <field>Field_API_Name__c</field>
        <value xsi:type="xsd:string">mailingcity</value>
    </values>
    <values>
        <field>Integrated_System__c</field>
        <value xsi:type="xsd:string">Appian</value>
    </values>
    <values>
        <field>Integration_Mode__c</field>
        <value xsi:type="xsd:string">Outbound</value>
    </values>
    <values>
        <field>Object_API_Name__c</field>
        <value xsi:type="xsd:string">Contact</value>
    </values>
    <values>
        <field>Triggered_By__c</field>
        <value xsi:type="xsd:string">Update</value>
    </values>
    <values>
        <field>Valid__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
