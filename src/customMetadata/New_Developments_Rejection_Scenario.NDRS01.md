<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>NDRS01</label>
    <protected>false</protected>
    <values>
        <field>FLF_Fixed_Line_Footprint__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>MTM_TechType__c</field>
        <value xsi:type="xsd:string">Any</value>
    </values>
    <values>
        <field>Max_premises__c</field>
        <value xsi:type="xsd:double">9999999.0</value>
    </values>
    <values>
        <field>Min_premises__c</field>
        <value xsi:type="xsd:double">9.0</value>
    </values>
    <values>
        <field>Service_Status__c</field>
        <value xsi:type="xsd:string">Any</value>
    </values>
</CustomMetadata>
