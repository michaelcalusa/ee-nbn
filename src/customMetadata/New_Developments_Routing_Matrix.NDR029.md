<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>NDR029</label>
    <protected>false</protected>
    <values>
        <field>Building_Type__c</field>
        <value xsi:type="xsd:string">Pit and Pipe</value>
    </values>
    <values>
        <field>Dwelling_Type__c</field>
        <value xsi:type="xsd:string">Multi Dwelling Unit (MDU)</value>
    </values>
    <values>
        <field>Essential_Service__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Is_Pit_Pipe_Private__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>MTM__c</field>
        <value xsi:type="xsd:string">HFC</value>
    </values>
    <values>
        <field>Max__c</field>
        <value xsi:type="xsd:double">8.0</value>
    </values>
    <values>
        <field>Min__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>Premises_Max_Count__c</field>
        <value xsi:type="xsd:string">8</value>
    </values>
    <values>
        <field>Premises_Min_Count__c</field>
        <value xsi:type="xsd:string">1</value>
    </values>
    <values>
        <field>Service_Delivery_Type__c</field>
        <value xsi:type="xsd:string">Technical Assessment</value>
    </values>
</CustomMetadata>
