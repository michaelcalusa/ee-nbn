<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>NDR038</label>
    <protected>false</protected>
    <values>
        <field>Building_Type__c</field>
        <value xsi:type="xsd:string">Pit and Pipe</value>
    </values>
    <values>
        <field>Dwelling_Type__c</field>
        <value xsi:type="xsd:string">Single Dwelling Unit (SDU)</value>
    </values>
    <values>
        <field>Essential_Service__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Is_Pit_Pipe_Private__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>MTM__c</field>
        <value xsi:type="xsd:string">HFC</value>
    </values>
    <values>
        <field>Max__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Min__c</field>
        <value xsi:type="xsd:double">3.0</value>
    </values>
    <values>
        <field>Premises_Max_Count__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Premises_Min_Count__c</field>
        <value xsi:type="xsd:string">3</value>
    </values>
    <values>
        <field>Service_Delivery_Type__c</field>
        <value xsi:type="xsd:string">SD-1</value>
    </values>
</CustomMetadata>
