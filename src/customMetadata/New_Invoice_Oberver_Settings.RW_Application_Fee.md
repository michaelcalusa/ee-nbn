<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>RW Application Fee</label>
    <protected>false</protected>
    <values>
        <field>Generate_Invoice_for_Opportunity_Rtype__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Opportunity_Record_Type__c</field>
        <value xsi:type="xsd:string">RW Application Fee</value>
    </values>
</CustomMetadata>
