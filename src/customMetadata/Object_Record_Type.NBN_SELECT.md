<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>NBN SELECT</label>
    <protected>false</protected>
    <values>
        <field>DF_Quote_Record_Type_Name__c</field>
        <value xsi:type="xsd:string">NBN_Select</value>
    </values>
    <values>
        <field>Opportunity_Bundle_Record_Type_Name__c</field>
        <value xsi:type="xsd:string">NBN_Select</value>
    </values>
    <values>
        <field>Opportunity_Record_Type_Name__c</field>
        <value xsi:type="xsd:string">NBN_Select</value>
    </values>
</CustomMetadata>
