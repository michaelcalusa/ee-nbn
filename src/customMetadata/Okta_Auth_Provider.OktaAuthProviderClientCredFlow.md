<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>OktaAuthProviderClientCredFlow</label>
    <protected>false</protected>
    <values>
        <field>Access_Token_URL__c</field>
        <value xsi:type="xsd:string">https://sso-ee.nbnco.com.au/oauth2/ausfu8d2z90MM1Gnx0h7/v1/token</value>
    </values>
    <values>
        <field>Auth_Provider_Name__c</field>
        <value xsi:type="xsd:string">OktaAuthProviderClientCredFlow</value>
    </values>
    <values>
        <field>Callback_URL__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Client_Id__c</field>
        <value xsi:type="xsd:string">0oaj2jhuvgiLOgob00h7</value>
    </values>
    <values>
        <field>Client_Secret__c</field>
        <value xsi:type="xsd:string">qjLpdIGTVvRfV8eXQ9kLaKZQ6RfgcojLnY0UjtWv</value>
    </values>
    <values>
        <field>Scope__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Use_JSON_Encoding__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
