<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ACRJ1403</label>
    <protected>false</protected>
    <values>
        <field>Display_Sort__c</field>
        <value xsi:type="xsd:string">3</value>
    </values>
    <values>
        <field>Reason_Description__c</field>
        <value xsi:type="xsd:string">More information required on Restoration</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Resolution Rejection Reason</value>
    </values>
</CustomMetadata>
