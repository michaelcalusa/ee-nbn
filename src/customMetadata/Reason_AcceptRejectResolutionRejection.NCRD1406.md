<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>NCRD1406</label>
    <protected>false</protected>
    <values>
        <field>Display_Sort__c</field>
        <value xsi:type="xsd:double">5000.0</value>
    </values>
    <values>
        <field>Reason_Description__c</field>
        <value xsi:type="xsd:string">Operating within Specification</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Resolution Rejection Decline Reason</value>
    </values>
</CustomMetadata>
