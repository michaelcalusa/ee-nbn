<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>AADT9706</label>
    <protected>false</protected>
    <values>
        <field>AS_Request_Description__c</field>
        <value xsi:type="xsd:string">New Appointment Required - There is a mismatch between the Demand Type specified when reserving the Appointment and the Trouble Ticket to which the Appointment is associated</value>
    </values>
    <values>
        <field>Group_Name__c</field>
        <value xsi:type="xsd:string">Appointment Required</value>
    </values>
    <values>
        <field>Reason_Code__c</field>
        <value xsi:type="xsd:string">AADT9706</value>
    </values>
    <values>
        <field>Request_Description__c</field>
        <value xsi:type="xsd:string">AADT9706 - Demand Type does not match</value>
    </values>
    <values>
        <field>Substatus__c</field>
        <value xsi:type="xsd:string">Pending</value>
    </values>
</CustomMetadata>
