<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>EHAZ9011</label>
    <protected>false</protected>
    <values>
        <field>AS_Request_Description__c</field>
        <value xsi:type="xsd:string">Reschedule Required - An Internal Premises Occupational Health or Safety issue has delayed the completion of work</value>
    </values>
    <values>
        <field>Group_Name__c</field>
        <value xsi:type="xsd:string">Appointment Reschedule Required</value>
    </values>
    <values>
        <field>Reason_Code__c</field>
        <value xsi:type="xsd:string">EHAZ9011</value>
    </values>
    <values>
        <field>Request_Description__c</field>
        <value xsi:type="xsd:string">EHAZ9011 - In premise Safety Hazard</value>
    </values>
    <values>
        <field>Substatus__c</field>
        <value xsi:type="xsd:string">Pending</value>
    </values>
</CustomMetadata>
