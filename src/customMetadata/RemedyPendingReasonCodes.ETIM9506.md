<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ETIM9506</label>
    <protected>false</protected>
    <values>
        <field>AS_Request_Description__c</field>
        <value xsi:type="xsd:string">Reschedule Required - The NBN Co technician has arrived on site to investigate the fault however has been requested to leave due to a time constraint by the Designated End User</value>
    </values>
    <values>
        <field>Group_Name__c</field>
        <value xsi:type="xsd:string">Appointment Reschedule Required</value>
    </values>
    <values>
        <field>Reason_Code__c</field>
        <value xsi:type="xsd:string">ETIM9506</value>
    </values>
    <values>
        <field>Request_Description__c</field>
        <value xsi:type="xsd:string">ETIM9506 - Incomplete Time Constraint (Customer)</value>
    </values>
    <values>
        <field>Substatus__c</field>
        <value xsi:type="xsd:string">Pending</value>
    </values>
</CustomMetadata>
