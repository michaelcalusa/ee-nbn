<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>NFME9000</label>
    <protected>false</protected>
    <values>
        <field>AS_Request_Description__c</field>
        <value xsi:type="xsd:string">An Force Majeure event has been declared for the area and the Trouble Ticket appointment cannot proceed until the external event has cleared.</value>
    </values>
    <values>
        <field>Group_Name__c</field>
        <value xsi:type="xsd:string">NBN Action Required</value>
    </values>
    <values>
        <field>Reason_Code__c</field>
        <value xsi:type="xsd:string">NFME9000</value>
    </values>
    <values>
        <field>Request_Description__c</field>
        <value xsi:type="xsd:string">NFME9000 - An Force Majeure event has been declared in the Area</value>
    </values>
    <values>
        <field>Substatus__c</field>
        <value xsi:type="xsd:string">Held</value>
    </values>
</CustomMetadata>
