<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>NKSF9100</label>
    <protected>false</protected>
    <values>
        <field>AS_Request_Description__c</field>
        <value xsi:type="xsd:string">There is a known NBN service fault which is related to this trouble ticket.  We will provide an update as soon as available.</value>
    </values>
    <values>
        <field>Group_Name__c</field>
        <value xsi:type="xsd:string">NBN Action Required</value>
    </values>
    <values>
        <field>Reason_Code__c</field>
        <value xsi:type="xsd:string">NKSF9100</value>
    </values>
    <values>
        <field>Request_Description__c</field>
        <value xsi:type="xsd:string">NKSF9100-There is a known NBN service fault</value>
    </values>
    <values>
        <field>Substatus__c</field>
        <value xsi:type="xsd:string">Held</value>
    </values>
</CustomMetadata>
