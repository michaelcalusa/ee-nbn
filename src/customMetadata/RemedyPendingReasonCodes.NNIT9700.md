<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>NNIT9700</label>
    <protected>false</protected>
    <values>
        <field>AS_Request_Description__c</field>
        <value xsi:type="xsd:string">There is an NBN Co IT issue which is affecting the processing of this incident.  We will provide an update as soon as possible.</value>
    </values>
    <values>
        <field>Group_Name__c</field>
        <value xsi:type="xsd:string">NBN Action Required</value>
    </values>
    <values>
        <field>Reason_Code__c</field>
        <value xsi:type="xsd:string">NNIT9700</value>
    </values>
    <values>
        <field>Request_Description__c</field>
        <value xsi:type="xsd:string">NNIT9700 - NBN IT Issue</value>
    </values>
    <values>
        <field>Substatus__c</field>
        <value xsi:type="xsd:string">Held</value>
    </values>
</CustomMetadata>
