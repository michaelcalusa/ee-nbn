<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>NNSF9300</label>
    <protected>false</protected>
    <values>
        <field>AS_Request_Description__c</field>
        <value xsi:type="xsd:string">There is an Network shortfall affecting the this service.  A Network Augmentation has been requested</value>
    </values>
    <values>
        <field>Group_Name__c</field>
        <value xsi:type="xsd:string">NBN Action Required</value>
    </values>
    <values>
        <field>Reason_Code__c</field>
        <value xsi:type="xsd:string">NNSF9300</value>
    </values>
    <values>
        <field>Request_Description__c</field>
        <value xsi:type="xsd:string">NNSF9300 - Network Shortfall or Augmentation Required</value>
    </values>
    <values>
        <field>Substatus__c</field>
        <value xsi:type="xsd:string">Held</value>
    </values>
</CustomMetadata>
