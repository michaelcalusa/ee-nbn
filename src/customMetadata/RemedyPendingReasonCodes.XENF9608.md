<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>XENF9608</label>
    <protected>false</protected>
    <values>
        <field>AS_Request_Description__c</field>
        <value xsi:type="xsd:string">Reschedule Required – End user and NBN failed to agree on a suitable rescheduled appointment slot</value>
    </values>
    <values>
        <field>Group_Name__c</field>
        <value xsi:type="xsd:string">Appointment Reschedule Required</value>
    </values>
    <values>
        <field>Reason_Code__c</field>
        <value xsi:type="xsd:string">XENF9608</value>
    </values>
    <values>
        <field>Request_Description__c</field>
        <value xsi:type="xsd:string">XENF9608 - End user and NBN failed to agree on a suitable rescheduled appointment slot</value>
    </values>
    <values>
        <field>Substatus__c</field>
        <value xsi:type="xsd:string">Pending</value>
    </values>
</CustomMetadata>
