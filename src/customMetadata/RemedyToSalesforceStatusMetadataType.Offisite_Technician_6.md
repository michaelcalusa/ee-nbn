<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Offisite Technician 6</label>
    <protected>false</protected>
    <values>
        <field>After_Engagement_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>After_Incident_Status__c</field>
        <value xsi:type="xsd:string">Pending</value>
    </values>
    <values>
        <field>After_Industry_Status__c</field>
        <value xsi:type="xsd:string">InProgress</value>
    </values>
    <values>
        <field>After_Industry_Sub_Status__c</field>
        <value xsi:type="xsd:string">Pending</value>
    </values>
    <values>
        <field>After_Status_Reason__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Before_Incident_Status__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Before_Industry_Status__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Before_Industry_Sub_Status__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Current_Work_Order_Status__c</field>
        <value xsi:type="xsd:string">COMPLETED</value>
    </values>
    <values>
        <field>Incident_Field_API_Name__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Incident_Field_API_Value__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Request_Subject__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>User_Action__c</field>
        <value xsi:type="xsd:string">offsiteTechnician</value>
    </values>
</CustomMetadata>
