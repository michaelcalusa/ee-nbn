<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Resolution Category Codes 203</label>
    <protected>false</protected>
    <values>
        <field>Industry_Code__c</field>
        <value xsi:type="xsd:string">ANFF4200</value>
    </values>
    <values>
        <field>Industry_Description__c</field>
        <value xsi:type="xsd:string">No Fault Found – Internal cabling issue</value>
    </values>
    <values>
        <field>Operation_Category_Tier1_Code__c</field>
        <value xsi:type="xsd:string">Customer Incident</value>
    </values>
    <values>
        <field>Product_Category_Tier1_Code__c</field>
        <value xsi:type="xsd:string">NCAS-FTTN</value>
    </values>
    <values>
        <field>Resolution_Category_Tier_1_Code__c</field>
        <value xsi:type="xsd:string">Premises</value>
    </values>
    <values>
        <field>Resolution_Category_Tier_2_Code__c</field>
        <value xsi:type="xsd:string">Cabling - Faulty /Configuration /Incompatible</value>
    </values>
    <values>
        <field>Resolution_Category_Tier_3_Code__c</field>
        <value xsi:type="xsd:string">No Fault Found - Truck Roll Required</value>
    </values>
</CustomMetadata>
