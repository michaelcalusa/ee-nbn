<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Resolution Category Codes 319</label>
    <protected>false</protected>
    <values>
        <field>Industry_Code__c</field>
        <value xsi:type="xsd:string">NTRR4203</value>
    </values>
    <values>
        <field>Industry_Description__c</field>
        <value xsi:type="xsd:string">nbn reconfigured network to restore service.</value>
    </values>
    <values>
        <field>Operation_Category_Tier1_Code__c</field>
        <value xsi:type="xsd:string">Customer Incident</value>
    </values>
    <values>
        <field>Product_Category_Tier1_Code__c</field>
        <value xsi:type="xsd:string">NCAS-FTTN</value>
    </values>
    <values>
        <field>Resolution_Category_Tier_1_Code__c</field>
        <value xsi:type="xsd:string">DLM</value>
    </values>
    <values>
        <field>Resolution_Category_Tier_2_Code__c</field>
        <value xsi:type="xsd:string">Manually Applied Change</value>
    </values>
    <values>
        <field>Resolution_Category_Tier_3_Code__c</field>
        <value xsi:type="xsd:string">NBNCo - Remote Reconfigure</value>
    </values>
    <values>
        <field>Template_Name__c</field>
        <value xsi:type="xsd:string">DLM_Manually_Applied_Change</value>
    </values>
</CustomMetadata>
