<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Resolution Category Codes 332</label>
    <protected>false</protected>
    <values>
        <field>Industry_Code__c</field>
        <value xsi:type="xsd:string">NNFF5205</value>
    </values>
    <values>
        <field>Industry_Description__c</field>
        <value xsi:type="xsd:string">Service speed impacted, however threshold is not yet breached to allow Trouble Ticket placement</value>
    </values>
    <values>
        <field>Operation_Category_Tier1_Code__c</field>
        <value xsi:type="xsd:string">Customer Incident - Priority Assist</value>
    </values>
    <values>
        <field>Product_Category_Tier1_Code__c</field>
        <value xsi:type="xsd:string">NCAS-FTTN</value>
    </values>
    <values>
        <field>Resolution_Category_Tier_1_Code__c</field>
        <value xsi:type="xsd:string">Incident Response</value>
    </values>
    <values>
        <field>Resolution_Category_Tier_2_Code__c</field>
        <value xsi:type="xsd:string">Access Seeker</value>
    </values>
    <values>
        <field>Resolution_Category_Tier_3_Code__c</field>
        <value xsi:type="xsd:string">Service working to specification</value>
    </values>
    <values>
        <field>Template_Name__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
