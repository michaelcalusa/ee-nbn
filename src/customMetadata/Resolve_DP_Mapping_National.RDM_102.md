<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>RDM_102</label>
    <protected>false</protected>
    <values>
        <field>Build_DP__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>P6_DP_Name__c</field>
        <value xsi:type="xsd:string">SAPN</value>
    </values>
    <values>
        <field>Salesforce_DP_Name__c</field>
        <value xsi:type="xsd:string">SA Power Networks</value>
    </values>
    <values>
        <field>State_wise_Assignment__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Technology_Type__c</field>
        <value xsi:type="xsd:string">HFC</value>
    </values>
</CustomMetadata>
