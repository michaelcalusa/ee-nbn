<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>RDSM-028</label>
    <protected>false</protected>
    <values>
        <field>Parent_DP_Name__c</field>
        <value xsi:type="xsd:string">Downer</value>
    </values>
    <values>
        <field>Salesforce_DP_Name__c</field>
        <value xsi:type="xsd:string">Downer - NSW/ACT/QLD</value>
    </values>
    <values>
        <field>State__c</field>
        <value xsi:type="xsd:string">ACT</value>
    </values>
    <values>
        <field>Technology_Type__c</field>
        <value xsi:type="xsd:string">FTTX</value>
    </values>
</CustomMetadata>
