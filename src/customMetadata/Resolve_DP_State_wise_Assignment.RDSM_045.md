<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>RDSM-045</label>
    <protected>false</protected>
    <values>
        <field>Parent_DP_Name__c</field>
        <value xsi:type="xsd:string">Visionstream</value>
    </values>
    <values>
        <field>Salesforce_DP_Name__c</field>
        <value xsi:type="xsd:string">Visionstream - TAS</value>
    </values>
    <values>
        <field>State__c</field>
        <value xsi:type="xsd:string">TAS</value>
    </values>
    <values>
        <field>Technology_Type__c</field>
        <value xsi:type="xsd:string">FTTX</value>
    </values>
</CustomMetadata>
