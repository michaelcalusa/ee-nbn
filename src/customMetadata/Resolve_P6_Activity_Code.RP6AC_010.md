<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>RP6AC_010</label>
    <protected>false</protected>
    <values>
        <field>Build_Start__c</field>
        <value xsi:type="xsd:string">CVLS (Start Date)</value>
    </values>
    <values>
        <field>Build_WR__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Design_Approved__c</field>
        <value xsi:type="xsd:string">PDD-IAS</value>
    </values>
    <values>
        <field>Design_Start__c</field>
        <value xsi:type="xsd:string">WALK</value>
    </values>
    <values>
        <field>PC_Met__c</field>
        <value xsi:type="xsd:string">PC-MET</value>
    </values>
    <values>
        <field>Program_Delivery__c</field>
        <value xsi:type="xsd:string">MDU</value>
    </values>
    <values>
        <field>Ready_for_service__c</field>
        <value xsi:type="xsd:string">FSMDL-02</value>
    </values>
</CustomMetadata>
