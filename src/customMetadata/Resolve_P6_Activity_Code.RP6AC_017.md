<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>RP6AC_017</label>
    <protected>false</protected>
    <values>
        <field>Build_Start__c</field>
        <value xsi:type="xsd:string">CON-START</value>
    </values>
    <values>
        <field>Build_WR__c</field>
        <value xsi:type="xsd:string">ADM-ICI</value>
    </values>
    <values>
        <field>Design_Approved__c</field>
        <value xsi:type="xsd:string">DDD-FDA</value>
    </values>
    <values>
        <field>Design_Start__c</field>
        <value xsi:type="xsd:string">NDD-1030</value>
    </values>
    <values>
        <field>PC_Met__c</field>
        <value xsi:type="xsd:string">PCDL1000</value>
    </values>
    <values>
        <field>Program_Delivery__c</field>
        <value xsi:type="xsd:string">BUSINESS SEGMENT</value>
    </values>
    <values>
        <field>Ready_for_service__c</field>
        <value xsi:type="xsd:string">RFS</value>
    </values>
</CustomMetadata>
