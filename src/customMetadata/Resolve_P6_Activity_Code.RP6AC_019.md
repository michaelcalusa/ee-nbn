<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>RP6AC_019</label>
    <protected>false</protected>
    <values>
        <field>Build_Start__c</field>
        <value xsi:type="xsd:string">4.Const-Fiber.ISP (Start date)</value>
    </values>
    <values>
        <field>Build_WR__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Design_Approved__c</field>
        <value xsi:type="xsd:string">3.PNI-Create Design (End date)</value>
    </values>
    <values>
        <field>Design_Start__c</field>
        <value xsi:type="xsd:string">2.NodeSplit-Design (start date)</value>
    </values>
    <values>
        <field>PC_Met__c</field>
        <value xsi:type="xsd:string">7.Acceptance (end date)</value>
    </values>
    <values>
        <field>Program_Delivery__c</field>
        <value xsi:type="xsd:string">HFC Node Split</value>
    </values>
    <values>
        <field>Ready_for_service__c</field>
        <value xsi:type="xsd:string">7.Acceptance (end date)</value>
    </values>
</CustomMetadata>
