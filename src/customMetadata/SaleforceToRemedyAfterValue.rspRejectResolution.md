<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>rspRejectResolution</label>
    <protected>false</protected>
    <values>
        <field>End_User_Engagement_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Incident_Status__c</field>
        <value xsi:type="xsd:string">Assigned</value>
    </values>
    <values>
        <field>Industry_Status__c</field>
        <value xsi:type="xsd:string">Resolved</value>
    </values>
    <values>
        <field>Industry_Sub_Status__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Pending_Reason_Code__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Request_Subject__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Status_Reason__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>User_Action__c</field>
        <value xsi:type="xsd:string">rspRejectResolution</value>
    </values>
</CustomMetadata>
