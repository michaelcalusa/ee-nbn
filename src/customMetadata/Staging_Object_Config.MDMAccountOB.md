<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>MDMAccountOB</label>
    <protected>false</protected>
    <values>
        <field>Config_For__c</field>
        <value xsi:type="xsd:string">MDM</value>
    </values>
    <values>
        <field>Delete_Fields_Map__c</field>
        <value xsi:type="xsd:string">{&quot;OwnerId&quot;:&quot;OwnerId&quot;,&quot;CreatedDate&quot;:&quot;CreatedDate&quot;,&quot;CreatedById&quot;:&quot;CreatedById&quot;, &quot;LastModifiedDate&quot;:&quot;LastModifiedDate&quot;, &quot;LastModifiedById&quot;:&quot;LastModifiedById&quot;}</value>
    </values>
    <values>
        <field>Exclude_Sync_Event_Fields__c</field>
        <value xsi:type="xsd:string">LastModifiedDate,LastModifiedById</value>
    </values>
    <values>
        <field>External_Id_Field__c</field>
        <value xsi:type="xsd:string">MDMID__c</value>
    </values>
    <values>
        <field>Fields_Map__c</field>
        <value xsi:type="xsd:string">{
&quot;ABN__c&quot;:&quot;ABN&quot;,
&quot;ACN__c&quot;:&quot;ACN&quot;,
&quot;DUNS_ID__c&quot;:&quot;DUNS&quot;,
&quot;No_of_Employees__c&quot;:&quot;employeeNum&quot;,
&quot;Status__c&quot;:&quot;partystatus&quot;,
&quot;Trading_Name__c&quot;:&quot;sitetradingname&quot;,
&quot;Website&quot;:&quot;Webaddress&quot;,
&quot;Phone&quot;:&quot;partyPhone&quot;,
&quot;Primary_ANZSIC__c&quot;:&quot;anzsicClass&quot;,
&quot;Primary_ANZSIC_Description__c&quot;:&quot;anzsicClassDescription&quot;,
&quot;Primary_ANZSIC_Division__c&quot;:&quot;anzsicDivision&quot;,
&quot;Primary_ANZSIC_Division_Description__c&quot;:&quot;anzsicDivisionDescription&quot;,
&quot;Tertiary_ANZSIC__c&quot;:&quot;anzsicGroup&quot;,
&quot;Tertiary_ANZSIC_Description__c&quot;:&quot;anzsicGroupDescription&quot;,
&quot;Secondary_ANZSIC__c&quot;:&quot;anzsicSubDivision&quot;,
&quot;Secondary_ANZSIC_Description__c&quot; : &quot;anzsicSubDivisionDescription&quot;,
&quot;Domestic_Parent_Name__c&quot;:&quot;DomesticParentName&quot;,
&quot;Global_Ultimate_Parent_County__c&quot;:&quot;GlobalUltimateParentCountry&quot;,
&quot;Global_Ultimate_Parent_Name__c&quot;:&quot;GlobalUltimateParentName&quot;,
&quot;Immediate_Parent_Name__c&quot;:&quot;ImmediateParentName&quot;,
&quot;ABN_Entity_Type__c&quot;:&quot;ABNEntityType&quot;,
&quot;Business_Flag__c&quot;:&quot;Business&quot;,
&quot;Employee_Band__c&quot;:&quot;EmployeeBand&quot;,
&quot;Employee_Indicator__c&quot;:&quot;EmployeeIndicator&quot;,
&quot;Revenue__c&quot;:&quot;Revenue&quot;,
&quot;Revenue_Band__c&quot;:&quot;RevenueBand&quot;,
&quot;Revenue_Indicator__c&quot;:&quot;RevenueIndicator&quot;,
&quot;Update_Recency_Indicator__c&quot;:&quot;UpdateRecencyInd&quot;,
&quot;Entity_Name__c&quot;:&quot;EntityName&quot;,
&quot;Purchasing_Entity__c&quot;:&quot;Purchasingentityflag&quot;,
&quot;OwnerId&quot;:&quot;OwnerId&quot;,
&quot;CreatedDate&quot;:&quot;CreatedDate&quot;,
&quot;CreatedById&quot;:&quot;CreatedById&quot;,
&quot;LastModifiedDate&quot;:&quot;LastModifiedDate&quot;, 
&quot;LastModifiedById&quot;:&quot;LastModifiedById&quot;
}</value>
    </values>
    <values>
        <field>MappingType__c</field>
        <value xsi:type="xsd:string">OutBound</value>
    </values>
    <values>
        <field>Object__c</field>
        <value xsi:type="xsd:string">Account</value>
    </values>
    <values>
        <field>RecordType_Names__c</field>
        <value xsi:type="xsd:string">Business End Customer</value>
    </values>
    <values>
        <field>is_Creation_Allowed__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>is_Deletion_Allowed__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>is_Modification_Allowed__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
