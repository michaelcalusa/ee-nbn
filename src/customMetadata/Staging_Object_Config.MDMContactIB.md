<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>MDMContactIB</label>
    <protected>false</protected>
    <values>
        <field>Config_For__c</field>
        <value xsi:type="xsd:string">MDM</value>
    </values>
    <values>
        <field>Delete_Fields_Map__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Exclude_Sync_Event_Fields__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>External_Id_Field__c</field>
        <value xsi:type="xsd:string">MDMID__c</value>
    </values>
    <values>
        <field>Fields_Map__c</field>
        <value xsi:type="xsd:string">{
&quot;address1&quot;:&quot;MailingStreet&quot;,
&quot;firstName&quot;:&quot;FirstName&quot;,
&quot;position&quot;:&quot;Job_Title__c&quot;,
&quot;postcode&quot;:&quot;MailingPostalCode&quot;,
&quot;salutation&quot;:&quot;Salutation&quot;,
&quot;suburb&quot;:&quot;MailingCity&quot;,
&quot;surname&quot;:&quot;LastName&quot;,
&quot;emailAddress&quot;:&quot;Email&quot;,
&quot;phoneNumber&quot;:&quot;Phone&quot;,
&quot;state&quot;:&quot;MailingState&quot;,
&quot;partyId&quot;:&quot;MDMID__c&quot;
}</value>
    </values>
    <values>
        <field>MappingType__c</field>
        <value xsi:type="xsd:string">InBound</value>
    </values>
    <values>
        <field>Object__c</field>
        <value xsi:type="xsd:string">Contact</value>
    </values>
    <values>
        <field>RecordType_Names__c</field>
        <value xsi:type="xsd:string">Business_End_Customer</value>
    </values>
    <values>
        <field>is_Creation_Allowed__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>is_Deletion_Allowed__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>is_Modification_Allowed__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
