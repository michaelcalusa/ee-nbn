<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>MDMContactOB</label>
    <protected>false</protected>
    <values>
        <field>Config_For__c</field>
        <value xsi:type="xsd:string">MDM</value>
    </values>
    <values>
        <field>Delete_Fields_Map__c</field>
        <value xsi:type="xsd:string">{&quot;OwnerId&quot;:&quot;OwnerId&quot;,&quot;CreatedDate&quot;:&quot;CreatedDate&quot;,&quot;CreatedById&quot;:&quot;CreatedById&quot;, &quot;LastModifiedDate&quot;:&quot;LastModifiedDate&quot;, &quot;LastModifiedById&quot;:&quot;LastModifiedById&quot;}</value>
    </values>
    <values>
        <field>Exclude_Sync_Event_Fields__c</field>
        <value xsi:type="xsd:string">LastModifiedDate,LastModifiedById,IndividualId</value>
    </values>
    <values>
        <field>External_Id_Field__c</field>
        <value xsi:type="xsd:string">MDMID__c</value>
    </values>
    <values>
        <field>Fields_Map__c</field>
        <value xsi:type="xsd:string">{
&quot;MailingStreet&quot;:&quot;address1&quot;,
&quot;FirstName&quot;:&quot;firstName&quot;,
&quot;Job_Title__c&quot;:&quot;position&quot;,
&quot;MailingPostalCode&quot;:&quot;postcode&quot;,
&quot;Salutation&quot;:&quot;salutation&quot;,
&quot;MailingCity&quot;:&quot;suburb&quot;,
&quot;LastName&quot;:&quot;surname&quot;,
&quot;Email&quot;:&quot;emailAddress&quot;,
&quot;Phone&quot;:&quot;phoneNumber&quot;,
&quot;MailingState&quot;:&quot;state&quot;,
&quot;OwnerId&quot;:&quot;OwnerId&quot;,
&quot;CreatedDate&quot;:&quot;CreatedDate&quot;,
&quot;CreatedById&quot;:&quot;CreatedById&quot;,
 &quot;LastModifiedDate&quot;:&quot;LastModifiedDate&quot;,
 &quot;LastModifiedById&quot;:&quot;LastModifiedById&quot;
}</value>
    </values>
    <values>
        <field>MappingType__c</field>
        <value xsi:type="xsd:string">OutBound</value>
    </values>
    <values>
        <field>Object__c</field>
        <value xsi:type="xsd:string">Contact</value>
    </values>
    <values>
        <field>RecordType_Names__c</field>
        <value xsi:type="xsd:string">Business End Customer</value>
    </values>
    <values>
        <field>is_Creation_Allowed__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>is_Deletion_Allowed__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>is_Modification_Allowed__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
