<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ST006</label>
    <protected>false</protected>
    <values>
        <field>Assigned_To__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Days_to_complete__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Milestone__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Related_Object_API_Name__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Sequence_Number__c</field>
        <value xsi:type="xsd:double">6.0</value>
    </values>
    <values>
        <field>Sub_Task_Name__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Task_Name__c</field>
        <value xsi:type="xsd:string">Task 6 CDMs advise EGM who will be attending the Customer Engagement session.</value>
    </values>
    <values>
        <field>Task_Record_Type__c</field>
        <value xsi:type="xsd:string">Senior_Operational_Engagement</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Task</value>
    </values>
</CustomMetadata>
