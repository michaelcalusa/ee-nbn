<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ST021</label>
    <protected>false</protected>
    <values>
        <field>Assigned_To__c</field>
        <value xsi:type="xsd:string">Bid_Transition_Engagement_Team</value>
    </values>
    <values>
        <field>Days_to_complete__c</field>
        <value xsi:type="xsd:double">14.0</value>
    </values>
    <values>
        <field>Milestone__c</field>
        <value xsi:type="xsd:string">Milestone 11</value>
    </values>
    <values>
        <field>Related_Object_API_Name__c</field>
        <value xsi:type="xsd:string">Transition_Engagement__c</value>
    </values>
    <values>
        <field>Sequence_Number__c</field>
        <value xsi:type="xsd:double">11.0</value>
    </values>
    <values>
        <field>Sub_Task_Name__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Task_Name__c</field>
        <value xsi:type="xsd:string">11. Submit Letter of Offer or Deal Module</value>
    </values>
    <values>
        <field>Task_Record_Type__c</field>
        <value xsi:type="xsd:string">Bid_Transition_Engagement</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Milestone</value>
    </values>
</CustomMetadata>
