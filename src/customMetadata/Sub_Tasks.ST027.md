<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ST027</label>
    <protected>false</protected>
    <values>
        <field>Assigned_To__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Days_to_complete__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Milestone__c</field>
        <value xsi:type="xsd:string">Milestone 8</value>
    </values>
    <values>
        <field>Related_Object_API_Name__c</field>
        <value xsi:type="xsd:string">Transition_Engagement_Milestone__c</value>
    </values>
    <values>
        <field>Sequence_Number__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>Sub_Task_Name__c</field>
        <value xsi:type="xsd:string">Present Solution and Transition Plan to EU and/or RSP</value>
    </values>
    <values>
        <field>Task_Name__c</field>
        <value xsi:type="xsd:string">Acceptance from End User and or RSP</value>
    </values>
    <values>
        <field>Task_Record_Type__c</field>
        <value xsi:type="xsd:string">Business Segment E&amp;G</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Milestone Sub Task</value>
    </values>
</CustomMetadata>
