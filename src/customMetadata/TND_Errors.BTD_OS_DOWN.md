<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>BTD_OS_DOWN</label>
    <protected>false</protected>
    <values>
        <field>Error_Code__c</field>
        <value xsi:type="xsd:string">BTD_OS_DOWN</value>
    </values>
    <values>
        <field>External_Error_Message__c</field>
        <value xsi:type="xsd:string">Test results indicate the BTD is unreachable. Perform further troubleshooting and retry the test. 
OR 
BTD Status Failed due to BTD offline</value>
    </values>
    <values>
        <field>Internal_Error_Message__c</field>
        <value xsi:type="xsd:string">BTD Operational State is DOWN. Please investigate service in SAM</value>
    </values>
</CustomMetadata>
