<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ERR_GET_SITE_ID</label>
    <protected>false</protected>
    <values>
        <field>Error_Code__c</field>
        <value xsi:type="xsd:string">ERR_GET_SITE_ID</value>
    </values>
    <values>
        <field>External_Error_Message__c</field>
        <value xsi:type="xsd:string">Unable to determine active E-NNI/I-NNI. Please Contact NBN Co for Assistance</value>
    </values>
    <values>
        <field>Internal_Error_Message__c</field>
        <value xsi:type="xsd:string">Unable to determine active E-NNI/INNI. Please check the operational state</value>
    </values>
</CustomMetadata>
