<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ERR_INITIATE_LOOPBACK</label>
    <protected>false</protected>
    <values>
        <field>Error_Code__c</field>
        <value xsi:type="xsd:string">ERR_INITIATE_LOOPBACK</value>
    </values>
    <values>
        <field>External_Error_Message__c</field>
        <value xsi:type="xsd:string">Loopback Test has failed. Please refer to the Test and Diagnostics Checklist prior to raising a Service Restoration Trouble Ticket</value>
    </values>
    <values>
        <field>Internal_Error_Message__c</field>
        <value xsi:type="xsd:string">SAM Error while initiate LoopBack Test</value>
    </values>
</CustomMetadata>
