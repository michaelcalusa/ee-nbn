<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ERR_INNI_SITE_FAILURE</label>
    <protected>false</protected>
    <values>
        <field>Error_Code__c</field>
        <value xsi:type="xsd:string">ERR_INNI_SITE_FAILURE</value>
    </values>
    <values>
        <field>External_Error_Message__c</field>
        <value xsi:type="xsd:string">An unexpected problem has occurred when running the Loopback Test. Please retry again later before raising a Service Incident Trouble Ticket</value>
    </values>
    <values>
        <field>Internal_Error_Message__c</field>
        <value xsi:type="xsd:string">Unable to determine active I-NNI. Please check the operational state</value>
    </values>
</CustomMetadata>
