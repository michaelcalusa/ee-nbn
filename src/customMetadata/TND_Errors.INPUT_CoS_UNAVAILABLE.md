<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>INPUT_CoS_UNAVAILABLE</label>
    <protected>false</protected>
    <values>
        <field>Error_Code__c</field>
        <value xsi:type="xsd:string">INPUT_CoS_UNAVAILABLE</value>
    </values>
    <values>
        <field>External_Error_Message__c</field>
        <value xsi:type="xsd:string">Network Performance Test is not supported for Class of Service - Low</value>
    </values>
    <values>
        <field>Internal_Error_Message__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
