<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>LINKTRACE FAILED</label>
    <protected>false</protected>
    <values>
        <field>Error_Code__c</field>
        <value xsi:type="xsd:string">LINKTRACE_FAILED</value>
    </values>
    <values>
        <field>External_Error_Message__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Internal_Error_Message__c</field>
        <value xsi:type="xsd:string">Linktrace test has failed. Please perform LinkTrace test for further information</value>
    </values>
</CustomMetadata>
