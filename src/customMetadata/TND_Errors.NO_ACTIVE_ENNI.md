<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>NO_ACTIVE_ENNI</label>
    <protected>false</protected>
    <values>
        <field>Error_Code__c</field>
        <value xsi:type="xsd:string">NO_ACTIVE_ENNI</value>
    </values>
    <values>
        <field>External_Error_Message__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Internal_Error_Message__c</field>
        <value xsi:type="xsd:string">No active E-NNI site found. Please check the aggregation OVC configuration.</value>
    </values>
</CustomMetadata>
