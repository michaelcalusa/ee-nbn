<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>UNI_E_PORT_AS_DOWN</label>
    <protected>false</protected>
    <values>
        <field>Error_Code__c</field>
        <value xsi:type="xsd:string">UNI-E_PORT_AS_DOWN</value>
    </values>
    <values>
        <field>External_Error_Message__c</field>
        <value xsi:type="xsd:string">An internal UNI test parameter is outside specification. Please raise a Service Restoration Trouble Ticket</value>
    </values>
    <values>
        <field>Internal_Error_Message__c</field>
        <value xsi:type="xsd:string">UNI-E Port Administrative state is DOWN. Please investigate service in SAM</value>
    </values>
</CustomMetadata>
