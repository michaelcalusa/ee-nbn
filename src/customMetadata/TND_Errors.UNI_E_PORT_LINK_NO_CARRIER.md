<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>UNI_E_PORT_LINK_NO_CARRIER</label>
    <protected>false</protected>
    <values>
        <field>Error_Code__c</field>
        <value xsi:type="xsd:string">UNI-E_PORT_LINK_NO_CARRIER</value>
    </values>
    <values>
        <field>External_Error_Message__c</field>
        <value xsi:type="xsd:string">No Ethernet carrier detected. Please check CPE connectivity to the NTD</value>
    </values>
    <values>
        <field>Internal_Error_Message__c</field>
        <value xsi:type="xsd:string">Please check with Retail Service Provider whether a Residential Gateway or PC is connected to the UNI-D port</value>
    </values>
</CustomMetadata>
