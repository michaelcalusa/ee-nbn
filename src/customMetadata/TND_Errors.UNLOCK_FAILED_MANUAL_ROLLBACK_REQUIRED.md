<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>UNLOCK_FAILED_MANUAL_ROLLBACK_REQUIRED</label>
    <protected>false</protected>
    <values>
        <field>Error_Code__c</field>
        <value xsi:type="xsd:string">UNLOCK_FAILED_MANUAL_ROLLBACK_REQUIRED</value>
    </values>
    <values>
        <field>External_Error_Message__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Internal_Error_Message__c</field>
        <value xsi:type="xsd:string">Port Unlock is failed, Manual Rollback for Port Lock is required.</value>
    </values>
</CustomMetadata>
