<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Small</label>
    <protected>false</protected>
    <values>
        <field>Account_Address_Record_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Credit_Memo_Transaction_Type__c</field>
        <value xsi:type="xsd:string">CM-Recoverable Works</value>
    </values>
    <values>
        <field>Invoice_Transaction_Type__c</field>
        <value xsi:type="xsd:string">Recoverable Works</value>
    </values>
    <values>
        <field>Opportunity_Record_Type__c</field>
        <value xsi:type="xsd:string">Recoverable Works Small</value>
    </values>
</CustomMetadata>
