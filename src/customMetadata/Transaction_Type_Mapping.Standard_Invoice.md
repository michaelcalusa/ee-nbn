<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Standard Invoice</label>
    <protected>false</protected>
    <values>
        <field>Account_Address_Record_Type__c</field>
        <value xsi:type="xsd:string">Dunning</value>
    </values>
    <values>
        <field>Credit_Memo_Transaction_Type__c</field>
        <value xsi:type="xsd:string">CM-Standard Invoice</value>
    </values>
    <values>
        <field>Invoice_Transaction_Type__c</field>
        <value xsi:type="xsd:string">Standard Invoice</value>
    </values>
    <values>
        <field>Opportunity_Record_Type__c</field>
        <value xsi:type="xsd:string">Standard</value>
    </values>
</CustomMetadata>
