<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Object to store migrations &amp; disconnection FAL contact info</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fieldSets>
        <fullName>FALPhoneNumber</fullName>
        <description>This field set is used in FALPhoneNumber Visual Force Page and Extension to display FAL Phone Number records in a table.</description>
        <displayedFields>
            <field>Name</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>First_Name__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Last_Name__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Site__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Site_Name__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Contact_Number_1__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Contact_Number_2__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Email_Address__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Status__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <label>FALPhoneNumber</label>
    </fieldSets>
    <fields>
        <fullName>Contact_Number_1__c</fullName>
        <description>First Contact Number</description>
        <externalId>false</externalId>
        <label>Contact Number 1</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Phone</type>
    </fields>
    <fields>
        <fullName>Contact_Number_2__c</fullName>
        <description>Secondary Contact Number if applicable</description>
        <externalId>false</externalId>
        <label>Contact Number 2</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Phone</type>
    </fields>
    <fields>
        <fullName>Email_Address__c</fullName>
        <description>Email Address of Contact</description>
        <externalId>false</externalId>
        <label>Email Address</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Email</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Fire_Lift_Site_Service__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Fire &amp; Lift Site Service</label>
        <referenceTo>FAL_Site_Service__c</referenceTo>
        <relationshipLabel>Fire &amp; Lift Unverified Contact Details</relationshipLabel>
        <relationshipName>Fire_Lift_Unverified_Contact_Details</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>First_Name__c</fullName>
        <description>First Name of Contact Number</description>
        <externalId>false</externalId>
        <label>First Name</label>
        <length>15</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Invalid_Reason__c</fullName>
        <description>The reason for the number being invalid</description>
        <externalId>false</externalId>
        <label>Invalid Reason</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Last_Name__c</fullName>
        <description>Last Name of Contact Number</description>
        <externalId>false</externalId>
        <label>Last Name</label>
        <length>45</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>MDDB_Id__c</fullName>
        <description>Contact identifier from MDDB.</description>
        <externalId>true</externalId>
        <label>MDDB Id</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Name__c</fullName>
        <description>Holds the full name of the contact</description>
        <externalId>false</externalId>
        <label>Name</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Role__c</fullName>
        <description>The role of the person who has the phone number</description>
        <externalId>false</externalId>
        <label>Role</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Site_Name__c</fullName>
        <description>This formula field is used in FALPhoneNumber field set to point to Site Name</description>
        <externalId>false</externalId>
        <formula>Site__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Site Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Site__c</fullName>
        <description>Related site to migrations and disconnections phone numbers</description>
        <externalId>false</externalId>
        <label>Site</label>
        <referenceTo>Site__c</referenceTo>
        <relationshipName>Phone_Numbers</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Unknown</fullName>
                    <default>true</default>
                    <label>Unknown</label>
                </value>
                <value>
                    <fullName>Invalid</fullName>
                    <default>false</default>
                    <label>Invalid</label>
                </value>
                <value>
                    <fullName>Valid</fullName>
                    <default>false</default>
                    <label>Valid</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Type__c</fullName>
        <description>M&amp;D team&apos;s version of contact phone number type</description>
        <externalId>false</externalId>
        <label>Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Primary</fullName>
                    <default>false</default>
                    <label>Primary</label>
                </value>
                <value>
                    <fullName>Secondary</fullName>
                    <default>false</default>
                    <label>Secondary</label>
                </value>
                <value>
                    <fullName>Tertiary</fullName>
                    <default>false</default>
                    <label>Tertiary</label>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                    <label>Other</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Fire &amp; Lift Unverified Contact Detail</label>
    <listViews>
        <fullName>All</fullName>
        <columns>First_Name__c</columns>
        <columns>Last_Name__c</columns>
        <columns>Contact_Number_1__c</columns>
        <columns>Contact_Number_2__c</columns>
        <columns>Email_Address__c</columns>
        <columns>MDDB_Id__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>PNBR-{0000000}</displayFormat>
        <label>Phone Number Ref</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Fire &amp; Lift Unverified Contact Details</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>First_Name__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Last_Name__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Contact_Number_1__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Contact_Number_2__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Email_Address__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Status__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Contact_Number_1__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Contact_Number_2__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Email_Address__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>First_Name__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Last_Name__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Status__c</lookupDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Contact_Number_1__c</searchFilterFields>
        <searchFilterFields>Contact_Number_2__c</searchFilterFields>
        <searchFilterFields>Email_Address__c</searchFilterFields>
        <searchFilterFields>First_Name__c</searchFilterFields>
        <searchFilterFields>Last_Name__c</searchFilterFields>
        <searchFilterFields>Status__c</searchFilterFields>
        <searchResultsAdditionalFields>First_Name__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Last_Name__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Site__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Email_Address__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Contact_Number_1__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Contact_Number_2__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>Invalid_Reason_Required_Status</fullName>
        <active>true</active>
        <description>Invalid reason is required if status is &apos;Invalid&apos;</description>
        <errorConditionFormula>IF ( TEXT(Status__c) = &apos;Invalid&apos; &amp;&amp; ISBLANK(Invalid_Reason__c), true, false)</errorConditionFormula>
        <errorDisplayField>Invalid_Reason__c</errorDisplayField>
        <errorMessage>You must enter a reason if status is &apos;Invalid&apos;</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Phone_and_Email_cannot_all_be_blank</fullName>
        <active>false</active>
        <description>Prevents a record from being created that has no actual contact information. One of the two Number fields OR the email field must contain a value</description>
        <errorConditionFormula>AND(
ISBLANK(Contact_Number_1__c),
ISBLANK(Contact_Number_2__c),
ISBLANK(Email_Address__c)
)</errorConditionFormula>
        <errorMessage>At least one piece of contact information must be provided. 
Please provide a Contact Number and/or Email Address before saving.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>Convert_to_Contact</fullName>
        <availability>online</availability>
        <description>Converts  phone number to a Contact</description>
        <displayType>button</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Convert to Contact</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <url>{!REQUIRESCRIPT(&apos;/soap/ajax/22.0/connection.js&apos;)} 

var result = sforce.connection.describeSObject(&apos;Contact&apos;); 

var NBNphNum1 = &apos;{!URLENCODE(FAL_Phone_Number__c.Contact_Number_1__c)}&apos;; 
var NBNphNum2 = &apos;{!URLENCODE(FAL_Phone_Number__c.Contact_Number_2__c)}&apos;; 
var NBNemail = &apos;{!URLENCODE(FAL_Phone_Number__c.Email_Address__c)}&apos;; 
var fName = &apos;{!URLENCODE(FAL_Phone_Number__c.First_Name__c)}&apos;; 
var lName = &apos;{!URLENCODE(FAL_Phone_Number__c.Last_Name__c)}&apos;; 
var recId = &apos;{!URLENCODE(FAL_Phone_Number__c.Id)}&apos;; 

var rt = &apos;&apos;; 

for (var i=0; i &lt; result.recordTypeInfos.length; i++) { 
   if ( result.recordTypeInfos[i].name == &apos;External Contact&apos;){ 
      rt = result.recordTypeInfos[i]; 
   } 
} 

if (typeof(srcUp) == &apos;function&apos;) 
{ 
srcUp(&apos;/{!$ObjectType.Contact}/e?RecordType=&apos;+rt.recordTypeId+&apos;&amp;00N2800000HSOSB=&apos;+NBNphNum1+&apos;&amp;00N2800000HSORy=&apos;+NBNphNum2+&apos;&amp;con15=&apos;+NBNemail+&apos;&amp;name_firstcon2=&apos;+fName+&apos;&amp;name_lastcon2=&apos;+lName+&apos;&amp;retURL=%2F&apos;+recId+&apos;&amp;isdtp=vw&apos;); 

}
else {
top.location.replace(&apos;/{!$ObjectType.Contact}/e?RecordType=&apos;+rt.recordTypeId+&apos;&amp;00N2800000HSOSB=&apos;+NBNphNum1+&apos;&amp;00N2800000HSORy=&apos;+NBNphNum2+&apos;&amp;con15=&apos;+NBNemail+&apos;&amp;name_firstcon2=&apos;+fName+&apos;&amp;name_lastcon2=&apos;+lName+&apos;&amp;retURL=%2F&apos;+recId); 
}</url>
    </webLinks>
</CustomObject>
