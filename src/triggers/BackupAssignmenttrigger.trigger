trigger BackupAssignmenttrigger on Backup_Assignment__c (Before Insert, Before Update) {

If(Trigger.IsBefore){
    
        If(Trigger.Isinsert){
         
         BackupAssignmenttriggerHandler.CheckforSameState(Trigger.new,null);
        // BackupAssignmenttriggerHandler.CheckforOrder(Trigger.new);
        
        }
        IF(Trigger.Isupdate){
        
        BackupAssignmenttriggerHandler.CheckforSameState(Trigger.new,trigger.oldmap);
        // BackupAssignmenttriggerHandler.CheckforOrder(Trigger.new);
         
        }
        
    }
    
    }