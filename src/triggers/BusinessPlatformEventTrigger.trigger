trigger BusinessPlatformEventTrigger on BusinessEvent__e (after insert ) {
    
   if (Trigger.isAfter) {
        BusinessPlatformEventTriggerHandler.eventTypesIdentification(Trigger.new);
        
    }

}