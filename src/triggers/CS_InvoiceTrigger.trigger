trigger CS_InvoiceTrigger on Invoice__c (after update) {
    if (trigger.isAfter) {
        if (trigger.isUpdate) {
            CS_ChangeInvoiceUtils.createOLI(trigger.newMap, trigger.oldMap);
        }
    }
}