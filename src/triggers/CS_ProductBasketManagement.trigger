trigger CS_ProductBasketManagement on cscfga__Product_Basket__c (before update) 
{
    new CS_ProductBasketTriggerHandler().run();

}