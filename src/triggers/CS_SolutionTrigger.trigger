/*************************************************
 Developed by: Li Tan
 Description: 
 Version History:
 - v1.0, 2017-03-02, LT: Created
*/

trigger CS_SolutionTrigger on csord__Solution__c (before Update, after Update) {
	
	ObjectCommonSettings__c solutionStatuses = ObjectCommonSettings__c.getValues('Solution Statuses');
	
    if (trigger.isBefore) {
        if (trigger.isUpdate) {

            // if cancellation request is rejected, update the status to previous status.
            map<Id, csord__Solution__c> rejectedMap = new map<Id, csord__Solution__c>();
            for (csord__Solution__c sol: trigger.newMap.values()) {
                if (sol.Status__c == solutionStatuses.Rejected__c/*'Rejected'*/ && trigger.oldMap.get(sol.Id).Status__c == solutionStatuses.Cancellation_Pending_Approval__c/*'Cancellation Pending Approval'*/) {
                    rejectedMap.put(sol.Id, sol);
                }
            }
            if (!rejectedMap.isEmpty()) {
                CS_ChangeInvoiceUtils.setOrderStatus(rejectedMap, trigger.oldMap);    
            }

            // validate delivery status.
            map<Id, csord__Solution__c> deliveryMap = new map<Id, csord__Solution__c>();
            for (csord__Solution__c sol: trigger.newMap.values()) {
                if (sol.Status__c == solutionStatuses.Delivery__c/*'Delivery'*/ || sol.Status__c == solutionStatuses.Complete__c/*'Complete'*/) {
                    deliveryMap.put(sol.Id, sol);
                }
            }
            if (!deliveryMap.isEmpty()) {
                CS_ChangeInvoiceUtils.validatePayments(deliveryMap);
            }

            
        }
    }

    if (trigger.isAfter) {
        if (trigger.isUpdate) {
            map<Id, csord__Solution__c> solMap = new map<Id, csord__Solution__c>();
            for (csord__Solution__c sol: trigger.new) {
                if (sol.Status__c == solutionStatuses.Cancelled__c/*'Cancelled'*/ && trigger.oldMap.get(sol.Id).Status__c == solutionStatuses.Cancellation_Pending_Approval__c/*'Cancellation Pending Approval'*/) {
                    solMap.put(sol.Id, sol);
                }
            }
            if (!solMap.isEmpty()) {
                CS_ChangeInvoiceUtils.canccelInvoices(solMap);    
            }
        }
    }
}