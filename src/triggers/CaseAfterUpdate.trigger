/*

<Date> <Authors Name>       <Brief Description of Change>
15-11-2017    Dolly Yadav         Merged  CaseAfterUpdate and CaseBeforeInsertUpdate in CaseManagement

*/


trigger CaseAfterUpdate on Case(after update) {  
    
    if (!CaseTeamReportUtil.firstRun) return;
    
    Map<String,Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Case.getRecordTypeInfosByName();
    Set<Id> recordTypeIds = new Set<Id>{
        rtMap.get('Complex Query').getRecordTypeId(),
        rtMap.get('Complex Complaint').getRecordTypeId(),
        rtMap.get('Urgent Complaint').getRecordTypeId(),
        rtMap.get('Formal Complaint').getRecordTypeId(),
        //Changed the Record Type Name to Commercial Billing - Dilip MSEU-891
        //Changed the Record Type Name to Credit & Collections - Dilip MSEU-4711
        rtMap.get('Credit & Collections').getRecordTypeId()
    };    

    List<Id> caseIds = new List<Id>();
    for (Id caseId : Trigger.newMap.keySet()){ 
        Case newCase = (Case)Trigger.newMap.get(caseId);
        Case oldCase = (Case)Trigger.oldMap.get(caseId);
        
        if (recordTypeIds.contains(newCase.recordTypeId) && oldCase.Status != 'Closed' && newCase.Status == 'Closed'){
            caseIds.add(newCase.Id);
            CaseTeamReportUtil.firstRun = false;
        }
    }
    
    if (caseIds.size()>0)
        CaseTeamReportUtil.createReports(caseIds);
}