/*

<Date> <Authors Name>       <Brief Description of Change>
15-11-2017    Dolly Yadav         Merged  CaseAfterUpdate and CaseBeforeInsertUpdate in CaseManagement

*/
trigger CaseBeforeInsertUpdate on Case(before insert, before update) {  
    if (Trigger.isInsert){
        CaseUtil.beforeInsertRules();
    }
    
    if (Trigger.isUpdate){
        CaseUtil.beforeUpdateRules();
    }   
}