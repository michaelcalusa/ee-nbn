trigger CaseContactManagement on Case_Contact__c ( before insert, before update, before delete, after insert, after update) { 
  /* if (Trigger.isInsert){
        CaseContactTriggerHandler.beforeInsertHandler();
    }
    
    if (Trigger.isUpdate){
        CaseContactTriggerHandler.beforeUpdateHandler();
    }
    
    if (Trigger.isDelete){
        CaseContactTriggerHandler.beforeDeleteHandler();
    } */  
    
     
   
    if(Trigger.isBefore){
        if (Trigger.isInsert){
            CaseContactTriggerHandler.beforeInsertHandler();
        }    
        if (Trigger.isUpdate){
           CaseContactTriggerHandler.beforeUpdateHandler();
        }    
        if (Trigger.isDelete){
           CaseContactTriggerHandler.beforeDeleteHandler();
        }
    }
    else {
            if(Trigger.isInsert){
              CaseContactTriggerHandler.afterInsertHandler();
            }
            if(Trigger.isUpdate){
              CaseContactTriggerHandler.afterUpdateHandler();
            }
    } 
}