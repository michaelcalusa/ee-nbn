trigger CaseManagement on Case (before insert, before update, before delete,after insert, after update, after delete, after undelete) {

/*
Author:       SantoshRaoBompally
Company:      NbnCo
Description:  Trigger on Case Object to meet following requirements 
              Case 1 : Notify account manager and Lead owner when a case is closed
Inputs:        
Test Class:   CaseTrigger_Test
History
<Date> <Authors Name>       <Brief Description of Change>
15-11-2017    Dolly Yadav         Merged  CaseAfterUpdate and CaseBeforeInsertUpdate in CaseManagement

*/
/***Code merge of CaseAfterUpdate added by Dolly***/
    if(trigger.isAfter && trigger.isUpdate){
    	//Added by Shubham for Story ACT-16605
    	CaseUtil.afterUpdate(trigger.oldMap, trigger.newMap);
	//END by Shubham for Story ACT-16605
        if (!CaseTeamReportUtil.firstRun) return;
        
        Map<String,Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Case.getRecordTypeInfosByName();
        Set<Id> recordTypeIds = new Set<Id>{
            rtMap.get('Complex Query').getRecordTypeId(),
            rtMap.get('Complex Complaint').getRecordTypeId(),
            rtMap.get('Urgent Complaint').getRecordTypeId(),
            rtMap.get('Formal Complaint').getRecordTypeId(),
            rtMap.get('Credit & Collections').getRecordTypeId()
        };    

        List<Id> caseIds = new List<Id>();
        for (Id caseId : Trigger.newMap.keySet()){ 
            Case newCase = (Case)Trigger.newMap.get(caseId);
            Case oldCase = (Case)Trigger.oldMap.get(caseId);
            
            if (recordTypeIds.contains(newCase.recordTypeId) && oldCase.Status != 'Closed' && newCase.Status == 'Closed'){
                caseIds.add(newCase.Id);
                CaseTeamReportUtil.firstRun = false;
            }
        }
        
        if (caseIds.size()>0)
            CaseTeamReportUtil.createReports(caseIds);
    }
    /*** Code merge of CaseAfterUpdate added by Dolly***/
    
    /***Code merge of CaseBeforeInsertUpdate added by DOlly***/
    if (trigger.isBefore){
        if (trigger.isInsert){
            CaseUtil.beforeInsertRules();
        }
        if (trigger.isUpdate){
	    CaseUtil.isbeforeUpdate(trigger.new,trigger.oldMap);//added by justin
            CaseUtil.beforeUpdateRules();
        }
    }
    /***Code merge of CaseBeforeInsertUpdate added by Dolly***/
    
    //Customer Connections Release 3
    if(trigger.isAfter && trigger.isInsert){
        //Added by Shubham for ACT-15535
        CaseUtil.afterInsert(trigger.new);
        Id agedOrderRecTypID = Schema.SObjectType.case.getRecordTypeInfosByName().get('Cust Conn CM').getRecordTypeId();

        List<Id> AgedCaseIdList = new List<Id>();
        for(Case cas:trigger.new){
            if(cas.recordTypeID == agedOrderRecTypID && cas.cc_team__c  != Constants.DOMTEAM){
            //Added DOMTeam by Ramya as part of ACT-17170
                AgedCaseIdList.add(cas.Id);              
            }
        }
        
        //Added by Wayne for the Aged case auto assign
        if(AgedCaseIdList.size() > 0 && UserInfo.getName() != 'Customer Connections Integration User'){
            Database.executeBatch(new AgedCaseAssignBatch(AgedCaseIdList),Integer.valueOf(Label.AgedCaseBatchSize));  
        }
    }    
    
    new CaseTriggerHandler().run();
}