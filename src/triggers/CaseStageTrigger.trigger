trigger CaseStageTrigger on Case_Stage__c (after insert,after update) 
{
    CaseStageTriggerHandler triggerHandler = new CaseStageTriggerHandler(Trigger.old, Trigger.new, Trigger.oldmap, Trigger.newmap);

    if(Trigger.isInsert)
        triggerHandler.OnAfterInsert();
    else if(Trigger.isUpdate)
        triggerHandler.OnAfterUpdate();
}