trigger CommunicationTrigger on Communication__c (before insert, before update) {
    new CommunicationTriggerHandler();
}