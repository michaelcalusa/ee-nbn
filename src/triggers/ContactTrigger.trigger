trigger ContactTrigger on Contact (before insert, before update, before delete, after insert,  after update,  after delete, after undelete){
    NewContactTriggerHandler triggerHandler = new NewContactTriggerHandler(Trigger.isExecuting, Trigger.size, Trigger.old, Trigger.new, Trigger.oldmap, Trigger.newmap);
    if(Trigger.isBefore){
        if(Trigger.isInsert)
            triggerHandler.OnBeforeInsert();
        else if(Trigger.isUpdate)
            triggerHandler.OnBeforeUpdate();
        else if(Trigger.isDelete)
            triggerHandler.OnBeforeDelete();
    }else{
        if(Trigger.isInsert)
            triggerHandler.OnAfterInsert();
        else if(Trigger.isUpdate)
            triggerHandler.OnAfterUpdate();
        else if(Trigger.isDelete)
            triggerHandler.OnAfterDelete();
        else
            triggerHandler.OnUndelete();
    }
}