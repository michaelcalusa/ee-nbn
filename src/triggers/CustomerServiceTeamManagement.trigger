trigger CustomerServiceTeamManagement on Customer_Service_Team__c (after insert, after update, after delete, after undelete) {  
    if (Trigger.isInsert){
        CustomerServiceTeamTriggerHandler.afterInsertRules();
    }
    
    if (Trigger.isUpdate){
        CustomerServiceTeamTriggerHandler.afterUpdateRules();    
    }   
    
    if (Trigger.isDelete){
        CustomerServiceTeamTriggerHandler.afterDeleteRules();    
    }       
}