/*================================================
    * @Trigger Name : DF_CommercialDealCriteria
    * @author : GAgarwal
    * @Purpose: This is a trigger used in Commercial Deal Criteria object
    * @created date:October 09 2018
    * @Last modified date:
    * @Last modified by : Accenture
================================================*/

trigger DF_CommercialDealCriteria on Commercial_Deal_Criteria__c (before insert,before update) {
    if(trigger.isBefore && (trigger.isInsert || trigger.isUpdate)){
        DF_CommercialDealCriterialTrigger_Util.commercialDealCriteria(trigger.new, Null, False);
    }
}