/*================================================
    * @Trigger Name : DF_CommercialDealTrigger
    * @author : Suman Gunaganti
    * @Purpose: This is a trigger used in Commercial Deal object
    * @created date:October 22 2018
    * @Last modified date:
    * @Last modified by : 
================================================*/
trigger DF_CommercialDealTrigger on Commercial_Deal__c (before update) {
    Map<Id, Commercial_Deal__c> commDealIdMap = new Map<Id, Commercial_Deal__c>();
        if(trigger.isBefore && trigger.isUpdate){
            for(Commercial_Deal__c cd: Trigger.newMap.values()){
                if(cd.Active__c == True && cd.Account__c <> null && cd.Start_Date__c <> Null && cd.End_Date__c <> Null)
                    commDealIdMap.put(cd.Id, cd);
            }
        List<Commercial_Deal_Criteria__c> dealCriteriaRecords = [Select id,Name,Commercial_Deal__c,Criteria__c, Criteria_Value__c,Start_Date__c,End_Date__c
                                                          From Commercial_Deal_Criteria__c
                                                          Where Commercial_Deal__c IN:commDealIdMap.keySet() And
                                                          Start_Date__c <> null AND
                                                          End_Date__c <> null];
        DF_CommercialDealCriterialTrigger_Util.commercialDealCriteria(dealCriteriaRecords,commDealIdMap,True);
    }

}