trigger DF_OrderEventTrigger on DF_Order_Event__e (after insert) {
    
    if (Trigger.isAfter) {
        DF_OrderEventTriggerHandler.eventTypesIdentification(Trigger.new);        
    }
}