trigger DF_OrderTrigger on DF_Order__c (before insert, before update, before delete, after insert,  after update, after delete, after undelete) {

	system.debug('DF_OrderTrigger - start');

	new DF_OrderTriggerHandler().run();

	system.debug('DF_OrderTrigger - end');
}