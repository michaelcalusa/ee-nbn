trigger EE_AS_IncidentNoteAccessSeekerTrigger on EE_AS_IncidentNoteAccessSeeker__c (before insert, before update, before delete, after insert,  after update, after delete, after undelete) {
    if (Trigger.isAfter && Trigger.isInsert) {
        EE_AS_IncidentNoteAccessSeekerTgrHandler.callIncidentNoteEmailToContactMatrix(Trigger.new);
    }    
}