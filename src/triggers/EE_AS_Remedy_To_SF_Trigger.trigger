trigger EE_AS_Remedy_To_SF_Trigger on EE_AS_Remedy_To_SF__e (after insert) {
	
	if (Trigger.isAfter && Trigger.isInsert) {
        EE_AS_Remedy_To_SF_TriggerHandler.processInboundEvents(Trigger.new);      
    }
    
}