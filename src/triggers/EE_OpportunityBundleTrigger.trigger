/*************************************************
- Developed by: Ashwani Kaushik
- Date Created: 27/08/2018 (dd/MM/yyyy)
- Description: Trigger for Opportunity Bundle object, and invokes the EE_OpportunityBundleTriggerHandler.
- Version History:
- v1.0 - 27/08/2018, RS: Created
*/

trigger EE_OpportunityBundleTrigger on DF_Opportunity_Bundle__c (before insert, before update, before delete, after insert,  after update,  after delete, after undelete) {
  EE_OpportunityBundleTriggerHandler handler = new EE_OpportunityBundleTriggerHandler();
  if(handler.IsDisabled())
     return;
      
     handler.run();
   
}