trigger GlobalFormStagingTrigger on Global_Form_Staging__c (after insert, after update) {    
    GlobalFormStagingTriggerHandler triggerHandler = new GlobalFormStagingTriggerHandler(Trigger.old, Trigger.new, Trigger.oldmap, Trigger.newmap);
    if(Trigger.isInsert)
        triggerHandler.OnAfterInsert();

}