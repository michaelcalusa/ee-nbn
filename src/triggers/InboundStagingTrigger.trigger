trigger InboundStagingTrigger on Inbound_Staging__c (after insert)
{
    InboundStagingTriggerHandler triggerHandler = new InboundStagingTriggerHandler(Trigger.isExecuting, Trigger.size, Trigger.old, Trigger.new, Trigger.oldmap, Trigger.newmap);
    if(Trigger.isBefore){
        
    }else{
        if(Trigger.isInsert)
            triggerHandler.OnAfterInsert();        
    }
}