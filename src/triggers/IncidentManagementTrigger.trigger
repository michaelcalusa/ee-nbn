/*------------------------------------------------------------
Author:    	    Kashyap Murthy
Company:        NbnCo
Description:    Trigger on Incident Management object to meet following requirements
                Case1: Assign ownerId to groupId based on group name.
                Case2: Assign Account id In Access_Seeker__c field based on Access_Seeker__c value obtained
                       in inbound JSON.
                Case3: To only allow "Automated Process" to update the OwnerId field of Incident Management record.

Inputs:
Test Class:
History
<Date>          <Authors Name>     <Brief Description of Change>
15/1/2018      Kashyap Murthy      Created trigger.
03/03/2018     Rohit Mathur        Added comments to the class.
------------------------------------------------------------*/

trigger IncidentManagementTrigger on Incident_Management__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
	TriggerDispatcher.Run(new IncidentManagementTriggerHandler());
}