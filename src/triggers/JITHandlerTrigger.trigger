trigger JITHandlerTrigger on JIT_Handler_Events__e (after insert) {
for(JIT_Handler_Events__e e:Trigger.new)
{
    if(e.UserId__c != null)
        {
            EE_JITHandlerUtility.disableUser(e.UserId__c);
        }
}
}