/*------------------------------------------------------------------------
<Date>        <Authors Name>        <Brief Description of Change> 
02-08-2018    Shubham Jaiswal        Created.
--------------------------------------------------------------------------*/
trigger LeadTrigger on Lead (before update,after update, before insert) {
  new LeadTriggerHandler().run();
}