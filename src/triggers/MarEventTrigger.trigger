/***************************************************************************************************
    Class Name          : MarEventTrigger
    Version             : 1.0
    Created Date        : 02-Aug-2018
    Author              : Arpit Narain
    Description         : Class for handling Mar Events
    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------

****************************************************************************************************/

trigger MarEventTrigger on MAR_Event__e (after insert) {

    if (Trigger.isAfter){
         MarEventTriggerHandler MarEventTriggerHandler = new MarEventTriggerHandler();
         MarEventTriggerHandler.processEvent(Trigger.new);
    }
}