trigger MarFormTrigger on MAR_Form_Staging__c (after insert, after update) {    
    MARFormTriggerHandler triggerHandler = new MARFormTriggerHandler(Trigger.old, Trigger.new, Trigger.oldmap, Trigger.newmap);
    if(Trigger.isInsert)
        triggerHandler.OnAfterInsert();
    else if(Trigger.isUpdate)
        triggerHandler.OnAfterUpdate();
}