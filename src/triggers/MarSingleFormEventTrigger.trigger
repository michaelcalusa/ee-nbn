trigger MarSingleFormEventTrigger on MAR_Single_Form_Event__e (after insert) {
    MarSingleFormEventTriggerHandler triggerHandler = new MarSingleFormEventTriggerHandler( Trigger.new);
    if(Trigger.isInsert) triggerHandler.OnAfterInsert();    
}