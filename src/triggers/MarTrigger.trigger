trigger MarTrigger on MAR__c (after insert, after update) {
    MarTriggerHandler triggerHandler = new MarTriggerHandler(Trigger.old, Trigger.new, Trigger.oldmap, Trigger.newmap);
    if(Trigger.isInsert)
        triggerHandler.OnAfterInsert();
    else if(Trigger.isUpdate)
        triggerHandler.OnAfterUpdate();
}