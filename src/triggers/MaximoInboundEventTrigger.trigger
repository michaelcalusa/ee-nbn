trigger MaximoInboundEventTrigger on MaximoInboundIntegration__e (after insert) {
    System.debug('Maximo Inbound Event Log');
    for (MaximoInboundIntegration__e event : Trigger.New) {
       System.debug('@MaximoEvent: ' + event);        
    }
    
    if(Trigger.isAfter){
        MaximoInboundEventTriggerHandler.processMaximoEvents(Trigger.New);
    }

}