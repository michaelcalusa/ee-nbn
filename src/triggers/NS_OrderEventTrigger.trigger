trigger NS_OrderEventTrigger on NS_Order_Event__e (after insert) {
    NS_OrderEventTriggerHandler.eventTypesIdentification(Trigger.new);
}