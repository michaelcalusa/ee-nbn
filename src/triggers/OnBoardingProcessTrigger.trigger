/*------------------------------------------------------------------------
Author:        Syed Moosa Nazir TN
Company:       Wipro Technologies
Description:   Apex Trigger to handle the business logic. Logic is written in "OnBoardingProcessTriggerHandler" Apex Class
               Responsible for:
               1 - The Onboarding step “Start Date” cannot be prior to the Onboarding Process “Start Date” (BAU-33)
History
<Date>            <Authors Name>        <Brief Description of Change> 
29-06-2016      Syed Moosa Nazir TN     Created.
--------------------------------------------------------------------------*/
trigger OnBoardingProcessTrigger on Onboarding_Process__c (before insert, before update, before delete,
after insert, after update, after delete, after undelete ) {
    new OnBoardingProcessTriggerHandler().run();
}