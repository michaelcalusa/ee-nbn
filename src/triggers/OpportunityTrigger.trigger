/*************************************************
- Developed by: Ravindran Shanmugam
- Date Created: 04/10/2017 (dd/MM/yyyy)
- Description: Trigger for opportunity object, and invokes the OpportunityTriggerHandler.
- Version History:
- v1.0 - 04/10/2017, RS: Created
*/

trigger OpportunityTrigger on Opportunity (before insert, before update, before delete, after insert,  after update,  after delete, after undelete) {
	OpportunityTriggerHandler triggerHandler = new OpportunityTriggerHandler();
    triggerHandler.run();
}