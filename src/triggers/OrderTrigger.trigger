/*Created By : Vipul Jain
Purpose: A Trigger on Order as a pert of ACT-16605*/
//Updated By Vipul  Jain for ACT-17408
trigger OrderTrigger on Customer_Connections_Order__c (after insert, after update, before insert, before update) {
    
    if(trigger.isUpdate && trigger.isAfter){
    	OrderTriggerHandler.onAfterUpdate(trigger.newMap, trigger.oldMap); 
    }
}