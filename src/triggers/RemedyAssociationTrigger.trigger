trigger RemedyAssociationTrigger on Remedy_Association__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
	TriggerDispatcher.Run(new RemedyAssociationHandler());
}