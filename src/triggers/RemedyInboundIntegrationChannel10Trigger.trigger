/***************************************************************************************************
Trigger Name:       RemedyInboundIntegrationChannel10Trigger
Version:            1.0 
Created Date:       30 May 2018
Function:           Trigger to process MicroService events.
Below are the Platform events channels for MicroService Notification.
RemedyInboundIntegration
RemedyInboundIntegration2
RemedyInboundIntegration3
RemedyInboundIntegration4
RemedyInboundIntegration5
RemedyInboundIntegration6
RemedyInboundIntegration7
RemedyInboundIntegration8
RemedyInboundIntegration9
RemedyInboundIntegration10
***************************************************************************************************
Modification Log:
* Developer                 Date             Description
* --------------------------------------------------------------------------------------------------                 
* Syed Moosa Nazir TN      30/May/2018       Created
****************************************************************************************************/ 
trigger RemedyInboundIntegrationChannel10Trigger on RemedyInboundIntegrationChannel10__e (After INSERT) {    
    ProcessRemedyToSalesforceIntergration.processInboundJsons(Trigger.new,'RemedyToSalesforce','RemedyInboundIntegrationChannel10__e');
}