/*------------------------------------------------------------
Author:    	    Rohit Mathur
Company:        NbnCo
Description:    Trigger on Remedy Work Log object to meet following requirements
                Case1: Stop creation of worklog if the body of worklog matches with any of the feed items on 
                the related incident management record
                
Inputs:         
Test Class:     
History
<Date>          <Authors Name>     <Brief Description of Change>
23/03/2018      Rohit Mathur        Created the trigger
------------------------------------------------------------*/

trigger RemedyWorkLogTrigger on Remedy_Work_Log__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
	TriggerDispatcher.Run(new RemedyWorkLogTriggerHandler());
}