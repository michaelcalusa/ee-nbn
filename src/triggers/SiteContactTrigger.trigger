/*
Author:       Naga
Company:      NbnCo
Description:  Trigger on Site Contact Object to meet following requirements 
              Case 1 : To chage the residential address of contact.
Inputs:        
Test Class:   SiteContactTrigger_Test
History
<Date>          <Authors Name>       <Brief Description of Change>
11/01/2017      Naga                    https://jira-cc.slb.nbndc.local/browse/BAU-488
                                        Change the residential Address for an external contact in console               
*/
trigger SiteContactTrigger on Site_Contacts__c (before insert, before update, before delete,after insert,after update,after delete,after undelete) {
    
    if(Trigger.isAfter && Trigger.isUpdate){
        List<Id> contactIds = new List<Id>();
        set<Id> updatedSiteContactSet = new set<ID>();
        for(Site_Contacts__c  st : Trigger.new){
            if(st.Is_Residential_Address__c){
                contactIds.add(st.Related_Contact__c);
                updatedSiteContactSet.add(st.id);
            }
            
        }
        
		if(contactIds != null && SiteContactTriggerHandler.isExecuting) {
		
			List<Site_Contacts__c> newSiteContactList = [SELECT Id, Role__c, Related_Contact__c, Related_Site__c, Is_Residential_Address__c FROM Site_Contacts__c where Related_Contact__c in : contactIds];
			
			List<Site_Contacts__c> updateNewSiteContactList = new List<Site_Contacts__c>();
			for(Site_Contacts__c st : newSiteContactList ){
				if(!updatedSiteContactSet.contains(st.id)){
					st.Is_Residential_Address__c  = false;
					updateNewSiteContactList.add(st);
				}
			} 
			
			if(updateNewSiteContactList.size() > 0){   
				SiteContactTriggerHandler.isExecuting = false;		
				update updateNewSiteContactList;
			}
        
		}
	}
}