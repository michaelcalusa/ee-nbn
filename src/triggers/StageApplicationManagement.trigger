trigger StageApplicationManagement on Stage_Application__c (before update, before insert, after insert, after Update) {    
	new StageApplicationTriggerHandler().run();
}