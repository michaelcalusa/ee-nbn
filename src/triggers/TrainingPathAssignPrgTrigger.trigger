trigger TrainingPathAssignPrgTrigger on lmscons__Training_Path_Assignment_Progress__c (before insert, after insert, before update,after Update) {    
    TrainingPathAssignPrgTriggerHandler triggerHandler = new TrainingPathAssignPrgTriggerHandler(Trigger.old, Trigger.new, Trigger.oldmap, Trigger.newmap);
    if(Trigger.isBefore && Trigger.isInsert)
        triggerHandler.OnBeforeInsert();
    else if(Trigger.isBefore && Trigger.isUpdate)
        triggerHandler.OnBeforeUpdate();
    // Added After Insert event by Rupendra Kumar Vats on 13/02/2018
    else if(Trigger.isAfter && Trigger.isInsert)
        triggerHandler.OnAfterInsert();
    //After Update event and related logic added by Nancy Gupta on 23/11/2017
    else if(Trigger.isAfter && Trigger.isUpdate)
        triggerHandler.OnAfterUpdate();
}