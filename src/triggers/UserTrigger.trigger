////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*  Name    : UserTrigger
*  Purpose : This is a trigger to enroll User as CFS user
*  Author  : Nancy Gupta
*  Date    : 2017-11-08
*  Version : 1.0
*/
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
trigger UserTrigger on User (after insert, before update) {
    map<string, id> prfleNameIDMap = new map<string, id>();
    for(profile prfl: [select id,Name from profile where Name IN ('ICT Customer Community Plus User', 'ICT Partner Community User')]){
        prfleNameIDMap.put(prfl.Name, prfl.id);
    }
    if (Trigger.isInsert) {
        Set<Id> userIdset = new Set<Id>();
        Set<Id> ICTPrtnrCntctIdset = new Set<Id>();
        for (User u : Trigger.new) {
            if(prfleNameIDMap.values().contains(u.Profileid)){
                if(u.ContactID != null){
                    system.debug('UserTrigger==>'+u.ContactID);
                    ICTPrtnrCntctIdset.add(u.ContactID);
                }
                if(prfleNameIDMap.get('ICT Customer Community Plus User') == u.Profileid){
                    System.debug('inside for loop' +u.Profile.Id+'::'+Trigger.newMap.keySet());    
                    userIdset.add(u.Id);
                    System.debug('Working');
                }
            }
        }
        if(userIdset.size() > 0){
            UserTriggerHandler.enrollCFSUser(userIdset);
        }
        if(Trigger.isAfter){
            
            if(!ICTPrtnrCntctIdset.isEmpty() && ICTPrtnrCntctIdset.size() > 0){
                system.debug('inside isAfter and ICTPrtnrCntctIdset check==>'+ICTPrtnrCntctIdset);
                map<id,contact> cntctIDCntctMap = new map<id,contact>([select id,recordtypeid, ICT_Community_User__c, ICT_Contact_Role__c from contact where ID IN: ICTPrtnrCntctIdset]);
                NewContactTriggerHandler contactTriggerHandler = new NewContactTriggerHandler(true, 5, null, null, null, null);
                if(!cntctIDCntctMap.keySet().isEmpty() && cntctIDCntctMap.keySet().size() > 0){
                    system.debug('before calling ICTcontactMapping method cntctIDCntctMap==>'+cntctIDCntctMap);
                    contactTriggerHandler.ICTcontactMapping(true,null, cntctIDCntctMap);
                }
            }
        }
    }
    if(Trigger.isUpdate){

        for (User u : Trigger.new) { 
            System.debug('D :: '+ U.profileid +' >> '+ Trigger.oldMap.get(u.Id).Profileid +' >> '+ prfleNameIDMap.get('ICT Customer Community Plus User'));
            if(u.profileid== prfleNameIDMap.get('ICT Customer Community Plus User') && u.Profileid != Trigger.oldMap.get(u.Id).Profileid){ 
                System.debug('inside for loop' +u.Profile.Id);    
                UserTriggerHandler.enrollCFSUser(Trigger.newMap.keySet());
                System.debug('Working');
            }
        }
    }  
}