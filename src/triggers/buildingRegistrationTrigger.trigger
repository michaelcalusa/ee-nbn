trigger buildingRegistrationTrigger on Building_Registration__c (before insert, before update,after update,after insert) {

    new BuildingRegistrationTriggerHandler().run();
}