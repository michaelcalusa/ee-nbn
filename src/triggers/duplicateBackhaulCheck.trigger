trigger duplicateBackhaulCheck on Backhaul_List__c (before insert, before update) {
    List<Id> CSAId = new List<Id>();
    List<Id> RSPID = new List<Id>();
    Map<string, List<Backhaul_List__c>> CSARSPCombinedMap = new Map<string, List<Backhaul_List__c>>();
    
    for(Backhaul_List__c bl :Trigger.New){
        CSAId.add(bl.Customer_Service_Area__c);
        RSPId.add(bl.Retail_Service_Provider__c);
    }
    for(Backhaul_List__c bl :[SELECT Id, Customer_Service_Area__c, Retail_Service_Provider__c, Name FROM Backhaul_List__c WHERE Customer_Service_Area__c IN :CSAId AND 
             Retail_Service_Provider__c IN :RSPId]){
        List<Backhaul_List__c> blList = new List<Backhaul_List__c>();
        if(CSARSPCombinedMap.containsKey(bl.Customer_Service_Area__c+''+bl.Retail_Service_Provider__c)){
            blList = CSARSPCombinedMap.get(bl.Customer_Service_Area__c+''+bl.Retail_Service_Provider__c);
        }
        blList.add(bl);
        CSARSPCombinedMap.put(bl.Customer_Service_Area__c+''+bl.Retail_Service_Provider__c, blList);
    }
    for(Backhaul_List__c bl :Trigger.New){
        if(CSARSPCombinedMap.containsKey(bl.Customer_Service_Area__c+''+bl.Retail_Service_Provider__c)){
            List<Backhaul_List__c> blList = CSARSPCombinedMap.get(bl.Customer_Service_Area__c+''+bl.Retail_Service_Provider__c);
            for(Backhaul_List__c blTemp :blList){
                if(bl.Id == blTemp.Id){
                    continue;
                }
                bl.addError('There is already a backhaul <a href=\'/'+blTemp.Id+'\'>'+blTemp.Name+'</a> with the same combination of CSA and RSP. Please try saving with different values.', false);
            }
        }
    }
}