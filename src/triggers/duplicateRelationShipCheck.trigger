trigger duplicateRelationShipCheck on Customer_Downstream_RSP_Junction__c (before insert, before update) {
    system.debug('insert '+Trigger.isinsert+' update '+trigger.isupdate);
    List<id> masterIdList = new List<id>();
    List<id> RSPIdList = new List<id>();
    Map<string,List<Customer_Downstream_RSP_Junction__c>> junMap = new Map<string,List<Customer_Downstream_RSP_Junction__c>>();
    
    for(Customer_Downstream_RSP_Junction__c jun :Trigger.New){
        masterIdList.add(jun.Master__c);
        RSPIdList.add(jun.Retail_Service_Provider__c);
        
    }
    
    for(Customer_Downstream_RSP_Junction__c jun :[SELECT Id, Name, End_User_Services_Offered__c, Master__c,
        Only_List_in_these_Locations__c, Retail_Service_Provider__c, RSP_Status__c, Service_available_for_Business__c, 
        Service_available_for_Residential__c, Service_Provider_Technology_Type__c, Type__c FROM Customer_Downstream_RSP_Junction__c
        WHERE Retail_Service_Provider__c IN :RSPIdList AND Master__c IN :masterIdList]){
        
        List<Customer_Downstream_RSP_Junction__c> junList = new List<Customer_Downstream_RSP_Junction__c>();
        if(junMap.ContainsKey(jun.Master__c+''+jun.Retail_Service_Provider__c)){
            junList = junMap.get(jun.Master__c+''+jun.Retail_Service_Provider__c);
        }
        junList.add(jun);
        junMap.put(jun.Master__c+''+jun.Retail_Service_Provider__c,junList);
    }
    
    for(Customer_Downstream_RSP_Junction__c jun :Trigger.New){
        if(junMap.containsKey(jun.Master__c+''+jun.Retail_Service_Provider__c)){
            List<Customer_Downstream_RSP_Junction__c> junTempList = junMap.get(jun.Master__c+''+jun.Retail_Service_Provider__c);
            for(Customer_Downstream_RSP_Junction__c junTemp :junTempList){
                if(Trigger.isUpdate && jun.Id == junTemp.Id){
                    continue;
                }
                if(jun.End_User_Services_Offered__c == junTemp.End_User_Services_Offered__c && jun.Only_List_in_these_Locations__c== junTemp.Only_List_in_these_Locations__c && 
                    jun.RSP_Status__c== junTemp.RSP_Status__c && jun.Service_available_for_Business__c == junTemp.Service_available_for_Business__c && 
                    jun.Service_available_for_Residential__c == junTemp.Service_available_for_Residential__c && jun.Service_Provider_Technology_Type__c == junTemp.Service_Provider_Technology_Type__c){
                     jun.addError('There is already a relationship <a href=\'/'+junTemp.Id+'\'>'+junTemp.Name+'</a> with the same combination of CSA and RSP. Please try saving with different values.', false);
                }
            }
            
        }
    }
}