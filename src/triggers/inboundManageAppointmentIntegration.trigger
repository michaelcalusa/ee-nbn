/*------------------------------------------------------------
    Author:        Murali Krishna
    Company:       NBN CO
    Description:   Trigger is fired on After Insert.
                   1. A List of String to add InboundJSONMessage__c Field is added in order to Bulkify.
    Returns:       The record id for the account
    History
    <Date>      <Authors Name>     <Brief Description of Change> 
------------------------------------------------------------*/
trigger inboundManageAppointmentIntegration on ManageAppointment__e (after insert) 
{    
       
    List<String> inboundJSONMessageList = new List<String>();
    for (ManageAppointment__e event: Trigger.New) 
    {
          if(event.InboundJSONMessage__c != null){
              inboundJSONMessageList.add(event.InboundJSONMessage__c);
          }
    }
    if(inboundJSONMessageList.size() > 0)
        ProcessManageAppointmentIntergration.ProcessManageAppointmentIntergration(inboundJSONMessageList, 'EngagementInboundIntegration');
}