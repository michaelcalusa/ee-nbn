/*------------------------------------------------------------
    Author:        Kashyap Murthy
    Company:       Contractor
    Description:   Trigger is fired on After Insert.
                   1. A List of String to add InboundJSONMessage__c Field is added in order to Bulkify.
    Returns:       The record id for the account
    History
    <Date>      <Authors Name>     <Brief Description of Change> 
------------------------------------------------------------*/
trigger inboundRemedyIntegration on RemedyInboundIntegration__e (after insert) {    
    ProcessRemedyToSalesforceIntergration.processInboundJsons(Trigger.new,'RemedyToSalesforce','RemedyInboundIntegration__e');
}