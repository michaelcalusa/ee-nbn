/*------------------------------------------------------------
    Author:        Beau Anderson
    Company:       Contractor
    Description:   Trigger is fired on After Insert to handle a post back from NBNCo microservices which relays on premise ticket of work(Maximo) information
    Returns:       
    History
    <Date>      <Authors Name>     <Brief Description of Change>
    21/03/218    Beau Anderson    Created 
------------------------------------------------------------*/
trigger inboundTicketOfWork on ticketOfWork__e (after insert) {    
    // Iterate through each notification.
    List<String> inboundJSONMessageList = new List<String>();
    List<String> WOActionJSONMessageList = new List<String>();
    for (ticketOfWork__e event: Trigger.New) {
        //Exception ex;
        //Below commented to slow down app logs in SIT org. IMPORTANT! The below line should be used for operational reporting so may need to be un commented
        //GlobalUtility.logMessage(GlobalConstants.ERROR_RECTYPE_NAME,'ticketOfWork__e','eventfiredinSF','','event',event.InboundJSONMessage__c,'',ex,0);
        system.debug('>>>'+event.InboundJSONMessage__c);
        if(event.InboundJSONMessage__c.contains('transactionStatus'))
            WOActionJSONMessageList.add(event.InboundJSONMessage__c);
        else
            inboundJSONMessageList.add(event.InboundJSONMessage__c);
        
        //inboundJSONMessageList.add(event.InboundJSONMessage__c);
        //WorkOrderHandler.AfterInsertHandlePlatformEvent(inboundJSONMessageList);  
    }
    //Deserialize The Json
   // WorkOrderHandler.AfterInsertHandlePlatformEvent(inboundJSONMessageList);  
    if(!inboundJSONMessageList.isEmpty() && inboundJSONMessageList.size() >0)
        WorkOrderHandler.AfterInsertHandlePlatformEvent(inboundJSONMessageList); 
    if(!WOActionJSONMessageList.isEmpty() && WOActionJSONMessageList.size() >0)
        WorkOrderOperatorActionhandler.updateWorkOrderThroughOperatorAction(WOActionJSONMessageList);
}