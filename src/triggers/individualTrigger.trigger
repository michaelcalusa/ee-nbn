trigger individualTrigger on Individual (after update, before update, before insert) {
     IndividualTriggerHandler triggerHandler = new IndividualTriggerHandler(Trigger.isExecuting, Trigger.size, Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
    if(Trigger.isBefore){
        if(Trigger.isUpdate)
            triggerHandler.OnBeforeUpdate();
        else if(Trigger.isInsert)
            triggerHandler.OnBeforeInsert();
    }else{
         if(Trigger.isUpdate)
            triggerHandler.OnAfterUpdate();        
    }

}