/* Purpose: Utility class to handle the negative responses from RPA 
 *          Refer to Jira: https://jira.nbnco.net.au/browse/CUSTSA-14476 for elaborate information
 * CreatedBy: SV
 * Change log:
 * DeveloperName    ChangeMade    ModifiedTimeStamp
 *     SV            Created          19/04/2018    
 */
trigger outboundRemedyIntegration on RemedyOutboundIntegration__e (after insert) {
    
    List<String> inboundJSONList = new List<String>();
    //capture the list of incoming JSONs from RPA
    for(RemedyOutboundIntegration__e e : Trigger.New) {
        if(String.isNotBlank(e.InboundJSONMessage__c)){
            inboundJSONList.add(e.InboundJSONMessage__c);
        }
    }
    //invoke the JSON processor class
    if(inboundJSONList.size() > 0)
        remedyRPAErrorHandlingUtil.deserializeRPAErrorJSON(inboundJSONList, 'inbound');
    
}