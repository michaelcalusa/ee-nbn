<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Account_Id_populate_into_text_field</fullName>
        <description>This is used to populate account ID on text filed on ASP maturity model object.</description>
        <field>Maturity_Model_Unique__c</field>
        <formula>Account__r.Id</formula>
        <name>Account Id populate into text field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ASP Maturity Model Unique per Account</fullName>
        <actions>
            <name>Account_Id_populate_into_text_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>ASP_Maturity_Model__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This rule is used to populate account ID information into a text field(Unique) on ASP maturity model object to restrict to one model per account</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
