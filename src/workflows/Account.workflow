<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>FU_to_setAccountAsPartnerOfPrtnrRcrdtyp</fullName>
        <description>US#17068: This field update will set Account of Partner recordtype as Partner.</description>
        <field>IsPartner</field>
        <literalValue>1</literalValue>
        <name>FU to setAccountAsPartnerOfPrtnrRcrdtyp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Account_Resend_Flag</fullName>
        <field>Resend_OBMsg__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck_Account_Resend_Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Account_Subtype</fullName>
        <field>Account_SubType__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Account Subtype</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Subtype</fullName>
        <field>Account_SubType__c</field>
        <literalValue>1</literalValue>
        <name>Update Account Subtype</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>DCB_Account_Updates</fullName>
        <actions>
            <name>Uncheck_Account_Resend_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PA_SF_UpdateAccount_v1_0</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Workflow to send an outbound notification to EBS to synchronise data.</description>
        <formula>OR(AND(OR(IF (ISBLANK( AR_Billing_Account_Number__c ), false, true ),IF (ISBLANK( AR_Party_Number__c ), false, true ) ), OR( ISCHANGED( Name ) , ISCHANGED( ParentId ) , ISCHANGED( ABN__c ) , ISCHANGED( ACN__c ) , ISCHANGED( TFN__c ) , ISCHANGED( Credit_Hold__c ) , ISCHANGED( Status__c ) ) ), Resend_OBMsg__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Rule to set Account as Partner of Partner Account recordtype</fullName>
        <actions>
            <name>FU_to_setAccountAsPartnerOfPrtnrRcrdtyp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Account</value>
        </criteriaItems>
        <description>US#17068: This workflow rule will trigger the action to set Account of Partner recordtype as Partner.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Uncheck Account Subtype</fullName>
        <actions>
            <name>Uncheck_Account_Subtype</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Type__c</field>
            <operation>excludes</operation>
            <value>Customer Subsidiary</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Subsidiary Account Field</fullName>
        <actions>
            <name>Update_Account_Subtype</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Type__c</field>
            <operation>includes</operation>
            <value>Customer Subsidiary</value>
        </criteriaItems>
        <description>To be updated when the Account Type is of Customer Subsidiary</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
