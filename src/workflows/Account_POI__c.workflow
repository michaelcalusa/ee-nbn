<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Duplicate_POI_AccountId_POIId</fullName>
        <description>Update the Duplicate field with a unique value</description>
        <field>Duplicate_POI__c</field>
        <formula>Account__c + POI__c</formula>
        <name>Set Duplicate POI - AccountId + POIId</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Account POI Created or Updated</fullName>
        <actions>
            <name>Set_Duplicate_POI_AccountId_POIId</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Executes process changes for creates and updates</description>
        <formula>ISNEW() || ISCHANGED ( Account__c ) || ISCHANGED (  POI__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
