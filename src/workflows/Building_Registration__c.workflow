<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>AckEmailForSingleBuildingRego</fullName>
        <description>AckEmailForSingleBuildingRego</description>
        <protected>false</protected>
        <recipients>
            <field>Registrant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Single_Building_Registration/SingleBuildingRego</template>
    </alerts>
</Workflow>
