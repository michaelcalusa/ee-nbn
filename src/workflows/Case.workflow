<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>AcknowledgementCaseAlert</fullName>
        <description>AcknowledgementCaseAlert</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Templates/AcknowledgementCaseTemplate</template>
    </alerts>
    <alerts>
        <fullName>Alert_the_case_management_team_when_the_Urgent_Complaint_case_is_created</fullName>
        <ccEmails>Lead_Case_Managers@nbnco.com.au</ccEmails>
        <description>Alert the case management team when the Urgent Complaint case is created</description>
        <protected>false</protected>
        <recipients>
            <recipient>icrmprocessautomationuser@nbnco.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Templates/Urgent_Complaint_Case_Alert</template>
    </alerts>
    <alerts>
        <fullName>Assure_Resolver_Group_L1_Alert</fullName>
        <ccEmails>escalations@nbnco.com.au</ccEmails>
        <description>Assure Resolver Group L1 Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>Resolver Group T0</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Resolver Group T1</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>escalations@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/L1EscalationTemplate</template>
    </alerts>
    <alerts>
        <fullName>Assure_Resolver_Group_L2_Alert</fullName>
        <ccEmails>escalations@nbnco.com.au</ccEmails>
        <description>Assure Resolver Group L2 Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>Resolver Group T0</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Resolver Group T1</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Resolver Group T2</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>escalations@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/L2EscalationTemplate</template>
    </alerts>
    <alerts>
        <fullName>Assure_Resolver_Group_L3_Alert</fullName>
        <ccEmails>escalations@nbnco.com.au</ccEmails>
        <description>Assure Resolver Group L3 Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>Resolver Group T0</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Resolver Group T1</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Resolver Group T2</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Resolver Group T3</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>escalations@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/L3EscalationTemplate</template>
    </alerts>
    <alerts>
        <fullName>Athena_Team_incoming_email</fullName>
        <description>Athena Team incoming email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>industrycapability_noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Athena/EU_email_received</template>
    </alerts>
    <alerts>
        <fullName>Case_SLA_expires</fullName>
        <description>Case SLA expires</description>
        <protected>false</protected>
        <recipients>
            <recipient>Manager_of_SME</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <field>SME_Case_Owner_User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Customer_Billing_Team/Case_SLA_notification_for_SME_User</template>
    </alerts>
    <alerts>
        <fullName>DP_Update_Notification</fullName>
        <description>DP Update Notification</description>
        <protected>false</protected>
        <recipients>
            <field>DP_Team_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Templates/SME_And_DP_Report_Assignment_Notification1</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_Athena_team</fullName>
        <description>Email Alert to Athena team</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>industrycapability_noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Athena/Internal_case_assigned_to_Industry_Capability_Solutions_agent</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_FAL_Review_Team</fullName>
        <ccEmails>NBNMigrationServices@nbnco.com.au</ccEmails>
        <description>Email Alert to FAL Review Team</description>
        <protected>false</protected>
        <recipients>
            <recipient>mehrnoushmasihpour@nbnco.com.au</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>violettemagyar@nbnco.com.au.icrm</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Templates/FAL_Case_Final_Review_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_MAR_M_D_Escalation_Team</fullName>
        <ccEmails>NBNMigrationServices@nbnco.com.au</ccEmails>
        <ccEmails>CSGoldCoastMigrationTeam@nbnco.com.au</ccEmails>
        <description>Email Alert to MAR M&amp;D Escalation Team</description>
        <protected>false</protected>
        <recipients>
            <recipient>mehrnoushmasihpour@nbnco.com.au</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>violettemagyar@nbnco.com.au.icrm</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Templates/MAR_Case_Escalation_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_MAR_Review_Team</fullName>
        <ccEmails>NBNMigrationServices@nbnco.com.au</ccEmails>
        <description>Email Alert to MAR Review Team</description>
        <protected>false</protected>
        <recipients>
            <recipient>mehrnoushmasihpour@nbnco.com.au</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>violettemagyar@nbnco.com.au.icrm</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Templates/MAR_Case_Final_Review_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_M_D_Escalation_Team</fullName>
        <ccEmails>NBNMigrationServices@nbnco.com.au</ccEmails>
        <description>Email Alert to M&amp;D Escalation Team</description>
        <protected>false</protected>
        <recipients>
            <recipient>mehrnoushmasihpour@nbnco.com.au</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>violettemagyar@nbnco.com.au.icrm</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Templates/FAL_Case_Escalation_Template</template>
    </alerts>
    <alerts>
        <fullName>Escalations_SME_Update_Notification</fullName>
        <description>Escalations_SME Update Notification</description>
        <protected>false</protected>
        <recipients>
            <field>SME_Team_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Templates/Escalations_SME_And_DP_Report_Assignment_Notification</template>
    </alerts>
    <alerts>
        <fullName>HSE_Impact_Minor</fullName>
        <ccEmails>CSGoldCoastSCCC@nbnco.com.au</ccEmails>
        <description>HSE Impact Minor</description>
        <protected>false</protected>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/HSE_Impact_Case_email_template</template>
    </alerts>
    <alerts>
        <fullName>HSE_Impact_marked_as_YES</fullName>
        <ccEmails>CSGoldCoastSCCC@nbnco.com.au</ccEmails>
        <ccEmails>ME_and_C_Leaders@nbnco.com.au</ccEmails>
        <description>HSE Impact marked as YES</description>
        <protected>false</protected>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/HSE_Impact_Case_email_template</template>
    </alerts>
    <alerts>
        <fullName>Jeopardy_Case_Assignment_Notification</fullName>
        <description>Jeopardy Case Assignment Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Templates/Jeopardy_Case_Assignment_Notification</template>
    </alerts>
    <alerts>
        <fullName>Jeopardy_Case_SLA_Due_date_notification</fullName>
        <description>Jeopardy Case SLA Due date notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Templates/Jeopardy_Case_SLA_due_date_email_notification</template>
    </alerts>
    <alerts>
        <fullName>LMS_Case_Queue_Email_Alert</fullName>
        <description>LMS Case Queue Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LMS_Case_Templates/LMS_New_Case_Queue_Notification</template>
    </alerts>
    <alerts>
        <fullName>LMS_Enquiries_Email_Alert</fullName>
        <description>LMS Enquiries Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Customer_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>locationmanagementandserviceability@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>LMS_Case_Templates/LMS_Enquiries_Case_Auto_Response_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>LMS_Inservice_Email_Alert</fullName>
        <description>LMS Inservice Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Customer_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>lmsinservice@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>LMS_Case_Templates/LMS_Inservice_Case_Auto_Response_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>LMS_Serviceability_Email_Alert</fullName>
        <description>LMS Serviceability Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Customer_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>serviceability@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>LMS_Case_Templates/LMS_Serviceability_Case_Auto_Response_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>NI_Integration_DWDM_Send_Auto_Response</fullName>
        <description>NI Integration DWDM Send Auto Response</description>
        <protected>false</protected>
        <recipients>
            <field>Customer_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>integrationdwdm@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Templates/Network_Integration_Autoresponse_email</template>
    </alerts>
    <alerts>
        <fullName>NI_Integration_DWDM_Send_Case_assignment_notification</fullName>
        <description>NI Integration DWDM Send Case assignment notification</description>
        <protected>false</protected>
        <recipients>
            <field>Customer_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>integrationdwdm@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Templates/Network_Integration_Case_Assigned_email</template>
    </alerts>
    <alerts>
        <fullName>NI_Integration_DWDM_Send_Case_closure_notification</fullName>
        <description>NI Integration DWDM Send Case closure notification</description>
        <protected>false</protected>
        <recipients>
            <field>Customer_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>integrationdwdm@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Templates/Network_Integration_Case_Closure_email</template>
    </alerts>
    <alerts>
        <fullName>NI_Integration_Faults_Send_Auto_Response</fullName>
        <description>NI Integration Faults Send Auto Response</description>
        <protected>false</protected>
        <recipients>
            <field>Customer_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>integrationfaults@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Templates/Network_Integration_Autoresponse_email</template>
    </alerts>
    <alerts>
        <fullName>NI_Integration_Faults_Send_Case_assignment_notification</fullName>
        <description>NI Integration Faults Send Case assignment notification</description>
        <protected>false</protected>
        <recipients>
            <field>Customer_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>integrationfaults@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Templates/Network_Integration_Case_Assigned_email</template>
    </alerts>
    <alerts>
        <fullName>NI_Integration_Faults_Send_Case_closure_notification</fullName>
        <description>NI Integration Faults Send Case closure notification</description>
        <protected>false</protected>
        <recipients>
            <field>Customer_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>integrationfaults@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Templates/Network_Integration_Case_Closure_email</template>
    </alerts>
    <alerts>
        <fullName>NI_NE_Integration_Send_Case_closure_notification</fullName>
        <description>NI NE Integration Send Case closure notification</description>
        <protected>false</protected>
        <recipients>
            <field>Customer_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>networkenablementintegration@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Templates/Network_Integration_Case_Closure_email</template>
    </alerts>
    <alerts>
        <fullName>NI_NE_Integrations_Send_Auto_Response</fullName>
        <description>NI NE Integrations Send Auto Response</description>
        <protected>false</protected>
        <recipients>
            <field>Customer_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>networkenablementintegration@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Templates/Network_Integration_Autoresponse_email</template>
    </alerts>
    <alerts>
        <fullName>NI_NE_Integrations_Send_Case_assignment_notification</fullName>
        <description>NI NE Integrations Send Case assignment notification</description>
        <protected>false</protected>
        <recipients>
            <field>Customer_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>networkenablementintegration@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Templates/Network_Integration_Case_Assigned_email</template>
    </alerts>
    <alerts>
        <fullName>NI_Network_Integrations_Send_Auto_Response</fullName>
        <description>NI Network Integrations Send Auto Response</description>
        <protected>false</protected>
        <recipients>
            <field>Customer_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>integration@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Templates/Network_Integration_Autoresponse_email</template>
    </alerts>
    <alerts>
        <fullName>NI_Network_Integrations_Send_Case_assignment_notification</fullName>
        <description>NI Network Integrations Send Case assignment notification</description>
        <protected>false</protected>
        <recipients>
            <field>Customer_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>integration@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Templates/Network_Integration_Case_Assigned_email</template>
    </alerts>
    <alerts>
        <fullName>NI_Network_Integrations_Send_Case_closure_notification</fullName>
        <description>NI Network Integrations Send Case closure notification</description>
        <protected>false</protected>
        <recipients>
            <field>Customer_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>integration@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Templates/Network_Integration_Case_Closure_email</template>
    </alerts>
    <alerts>
        <fullName>Notify_About_CAT_Overflow_Case</fullName>
        <ccEmails>Case_Advisor_Team_Leaders@nbnco.com.au</ccEmails>
        <description>Notify About CAT Overflow Case</description>
        <protected>false</protected>
        <recipients>
            <recipient>icrmprocessautomationuser@nbnco.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Templates/CM_CAT_Overflow_Alert</template>
    </alerts>
    <alerts>
        <fullName>Notify_About_CFU_Overflow_Case</fullName>
        <ccEmails>cscfuteam@nbnco.com.au</ccEmails>
        <description>Notify About CFU Overflow Case</description>
        <protected>false</protected>
        <recipients>
            <recipient>icrmprocessautomationuser@nbnco.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Templates/CM_CAT_Overflow_Alert</template>
    </alerts>
    <alerts>
        <fullName>Notify_About_CM_Overflow_Case</fullName>
        <ccEmails>Lead_Case_Managers@nbnco.com.au</ccEmails>
        <description>Notify About CM Overflow Case</description>
        <protected>false</protected>
        <recipients>
            <recipient>icrmprocessautomationuser@nbnco.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Templates/CM_CAT_Overflow_Alert</template>
    </alerts>
    <alerts>
        <fullName>Notify_Credit_and_Collections_Case_owner</fullName>
        <description>Notify Credit and Collections Case owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Templates/Notify_Case_Owner_on_Email_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notify_HSE_Operations_Team</fullName>
        <ccEmails>HSEOperations@nbnco.com.au</ccEmails>
        <description>Notify HSE Operations Team</description>
        <protected>false</protected>
        <recipients>
            <recipient>icrmprocessautomationuser@nbnco.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Templates/Notification_for_HES_Operations1</template>
    </alerts>
    <alerts>
        <fullName>Notify_NBNComplaintEscalations_Team_and_CSGoldCoastSCCC_Team</fullName>
        <ccEmails>NBNComplaintEscalations@nbnco.com.au</ccEmails>
        <ccEmails>CSGoldCoastSCCC@nbnco.com.au</ccEmails>
        <description>Notify NBNComplaintEscalations Team and CSGoldCoastSCCC Team</description>
        <protected>false</protected>
        <recipients>
            <recipient>icrmprocessautomationuser@nbnco.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Templates/TIO_Media_MP_issue_Alert1</template>
    </alerts>
    <alerts>
        <fullName>Notify_RM_Operations_Team</fullName>
        <description>Notify RM Operations Team</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Templates/Notification_for_RM_Operations1</template>
    </alerts>
    <alerts>
        <fullName>Responsible_Entity_Description_Email_Notification</fullName>
        <description>Responsible Entity Description Email Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Responsible_Entity_Description_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Templates/Notification_for_Responsible_Entity_Description</template>
    </alerts>
    <alerts>
        <fullName>Responsible_Entity_Owner_Email_Notification</fullName>
        <description>Responsible Entity Owner Email Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Responsible_Entity_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Templates/Notification_for_Responsible_Entity_Owner</template>
    </alerts>
    <alerts>
        <fullName>SME_Update_Notification</fullName>
        <description>SME Update Notification</description>
        <protected>false</protected>
        <recipients>
            <field>SME_Team_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Templates/SME_And_DP_Report_Assignment_Notification1</template>
    </alerts>
    <alerts>
        <fullName>Send_Athena_Case_Creation_Email</fullName>
        <description>Send Athena Case Creation Email</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>industrycapability@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Athena/End_User_case_creation</template>
    </alerts>
    <alerts>
        <fullName>Send_Case_Closed_Notification_to_Case_Owner</fullName>
        <description>Send Case Closed Notification to Case Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Templates/Case_Closed_Notification</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_Customer_Delivery_Suppert_Team</fullName>
        <description>Send Email to Customer Delivery Suppert Team</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Templates/RSP_Escalation_Case_Assignment_Notification_Template</template>
    </alerts>
    <alerts>
        <fullName>Send_email_reminder_to_Athena_Agents</fullName>
        <description>Send email reminder to Athena Agents</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>industrycapability_noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Athena/Internal_case_not_modified_for_5_business_days</template>
    </alerts>
    <alerts>
        <fullName>Send_email_reminder_to_Athena_Agents_and_their_manager</fullName>
        <description>Send email reminder to Athena Agents and their manager</description>
        <protected>false</protected>
        <recipients>
            <field>Athena_Owners_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>industrycapability_noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Athena/Internal_case_assigned_to_Industry_Capability_Solutions_agent</template>
    </alerts>
    <alerts>
        <fullName>Send_email_to_Jeopardy_case_owner</fullName>
        <description>Send email to Jeopardy case owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Templates/Notify_Case_owner_on_Jeopardy_Case_closure</template>
    </alerts>
    <alerts>
        <fullName>Send_email_to_case_owner</fullName>
        <description>Send email to case owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Templates/Escalation_Notify_Case_Owner_on_Case_Close_Resolved</template>
    </alerts>
    <alerts>
        <fullName>Send_email_to_case_owner_Resolved</fullName>
        <description>Send email to case owner on Resolved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Templates/Escalation_Notify_Case_Owner_on_Case_Close_Resolved</template>
    </alerts>
    <alerts>
        <fullName>Supply_chain_closed_notification</fullName>
        <description>Supply chain closed notification</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Supply_chain_Template/NBN_Orders_Supply_Case_Closed_Acknowledgement</template>
    </alerts>
    <fieldUpdates>
        <fullName>Assign_to_Athena_Team</fullName>
        <field>OwnerId</field>
        <lookupValue>Athena_Team</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Athena Team</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_Final_Review_Queue</fullName>
        <description>Assigns Case to Final Review Queue</description>
        <field>OwnerId</field>
        <lookupValue>FAL_Final_Review_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Final Review Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_Peer_Review_Queue</fullName>
        <description>Assigns to Peer Review Queue after all the field updates</description>
        <field>OwnerId</field>
        <lookupValue>FAL_Peer_Review_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Peer Review Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Capture_Created_Date</fullName>
        <description>Capture the Case created date</description>
        <field>Original_Case_Created_Date__c</field>
        <formula>CreatedDateTime__c</formula>
        <name>Capture Created Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Status_update_from_closed_to_Reopen</fullName>
        <field>Status</field>
        <literalValue>Re-Opened</literalValue>
        <name>Case Status update from closed to Reopen</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_SaveAndCloseFlag</fullName>
        <description>Set The IS Save And Close Button clicked to False</description>
        <field>Is_Save_And_Close_Button_Clicked__c</field>
        <literalValue>0</literalValue>
        <name>FU_SaveAndCloseFlag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_to_populate_Assigned_Date_field</fullName>
        <description>US#4571: This field update will populate the current date when the Aged Order case is assigned to a user from Customer Connections queue.</description>
        <field>Assigned_Date__c</field>
        <formula>NOW()</formula>
        <name>FU to populate Assigned Date field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LMS_Enquiries_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>LMS_Enquiries</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>LMS Enquiries Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LMS_High_Priority_Field_Update</fullName>
        <field>Priority</field>
        <literalValue>High</literalValue>
        <name>LMS High Priority Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LMS_Inservice_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>LMS_Inservice</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>LMS Inservice Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LMS_Serviceability_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>LMS_Serviceability</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>LMS Serviceability Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MAR_Original_Case_Handler_Name</fullName>
        <field>Original_Case_Handler_Name__c</field>
        <formula>IF(ISBLANK( Original_Case_Handler_Name__c ), Case_Owner_Name__c, Original_Case_Handler_Name__c)</formula>
        <name>MAR Original Case Handler Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MAR_Set_Submitted_For_Peer_Review_to_Yes</fullName>
        <field>Submitted_for_Peer_Review__c</field>
        <formula>&quot;Yes&quot;</formula>
        <name>MAR Set Submitted For Peer Review to Yes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MAR_Stamp_Original_Case_Handler_ID</fullName>
        <field>Original_Case_Handler_ID__c</field>
        <formula>IF(ISBLANK( Original_Case_Handler_ID__c ), Owner:User.Id , Original_Case_Handler_ID__c)</formula>
        <name>MAR Stamp Original Case Handler ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NI_Update_Config_Sent_to_Tech_Date</fullName>
        <description>Update Config Sent to Tech Date with current date time (Network Integrations)</description>
        <field>Config_Sent_to_Tech_Date__c</field>
        <formula>NOW()</formula>
        <name>NI Update Config Sent to Tech Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NI_Update_Integration_CRQ_Date</fullName>
        <description>Update Integration CRQ Date with current date time (Network Integrations)</description>
        <field>Integration_CRQ_Date__c</field>
        <formula>NOW()</formula>
        <name>NI Update Integration CRQ Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NI_Update_Scheduled_Date_Time</fullName>
        <description>Update Scheduled Date/Time with current date time (Network Integrations)</description>
        <field>Scheduled_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>NI Update Scheduled Date/Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Number_Of_Msg_Received</fullName>
        <field>Number_of_Messages_Received__c</field>
        <formula>IF(ISBLANK(Parent.Number_of_Messages_Received__c), 1, Parent.Number_of_Messages_Received__c+1)</formula>
        <name>Number Of Msg Received</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Prevent_Review_by_Handler</fullName>
        <description>If the person accepting a Peer Review Case is the same as the Original Case Handler, the Case should be assigned back to the Peer Review Queue to prevent self-reviews.</description>
        <field>OwnerId</field>
        <lookupValue>FAL_Peer_Review_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Reassign to Peer Review Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Query_Case_Status_Update_to_Close</fullName>
        <description>BAU-737</description>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Query Case Status Update to Close</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_RSP_Escalated_Flag</fullName>
        <field>RSP_Escalated__c</field>
        <literalValue>0</literalValue>
        <name>Reset RSP Escalated Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_Owner_FAL_Final_Review</fullName>
        <description>Sets case owner to final review queue</description>
        <field>OwnerId</field>
        <lookupValue>FAL_Final_Review_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Set Case Owner (FAL Final Review)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_Owner_FAL_Pending_Peer_Review</fullName>
        <description>Updates case owner to peer review queue</description>
        <field>OwnerId</field>
        <lookupValue>FAL_Peer_Review_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Set Case Owner (FAL Pending Peer Review)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_Owner_FAL_Queue</fullName>
        <description>Sets the owner of the case to the FAL - Queue</description>
        <field>OwnerId</field>
        <lookupValue>FAL_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Set Case Owner (FAL Queue)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_Owner_MAR_Final_Review</fullName>
        <description>Sets case owner to final review queue</description>
        <field>OwnerId</field>
        <lookupValue>MAR_Final_Review_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Set Case Owner (MAR Final Review)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_Owner_MAR_Pending_Peer_Review</fullName>
        <description>Updates case owner to peer review queue</description>
        <field>OwnerId</field>
        <lookupValue>MAR_Peer_Review_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Set Case Owner (MAR Pending Peer Review)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_Owner_MAR_Queue</fullName>
        <description>Sets the owner of the case to the MAR - Queue</description>
        <field>OwnerId</field>
        <lookupValue>MAR_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Set Case Owner (MAR Queue)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_Owner_M_D_Team</fullName>
        <description>Set the case owner to the escalation queue</description>
        <field>OwnerId</field>
        <lookupValue>FAL_MND_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Set Case Owner - M&amp;D Team</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_Owner_Re_opened_MAR_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>MAR_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Set Case Owner Re-opened (MAR Queue)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_Owner_Supply_chain_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Supply_Chain_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Set Case Owner (Supply chain Queue)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Commencement_Date</fullName>
        <description>Set Commencement Date to the current date and time.</description>
        <field>Commencement_Date__c</field>
        <formula>NOW()</formula>
        <name>Set Commencement Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_LiFD_Case_Cost_Estimation_Completed</fullName>
        <field>Cost_Estimation_Completed_Date__c</field>
        <formula>Now()</formula>
        <name>Set LiFD Case Cost Estimation Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_LiFD_Case_Deferral_Requested_Date</fullName>
        <field>Deferral_Requested_Date__c</field>
        <formula>Now()</formula>
        <name>Set LiFD Case Deferral Requested Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_LiFD_Case_Deferral_Reversal_Requeste</fullName>
        <field>Deferral_Reversal_Requested_Date__c</field>
        <formula>Now()</formula>
        <name>Set LiFD Case Deferral Reversal Requeste</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_LiFD_Case_Quote_Requested_Date</fullName>
        <field>Quote_Requested_Date__c</field>
        <formula>Now()</formula>
        <name>Set LiFD Case Quote Requested Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Original_Case_Handler_Name</fullName>
        <field>Original_Case_Handler_Name__c</field>
        <formula>IF(ISBLANK( Original_Case_Handler_Name__c ), Case_Owner_Name__c, Original_Case_Handler_Name__c)</formula>
        <name>Set Original Case Handler Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_SME_Resolution_Change_Date</fullName>
        <description>Sets SME Resolution Change Date to current date and time</description>
        <field>SME_Resolution_Change_Date__c</field>
        <formula>NOW()</formula>
        <name>Set SME Resolution Change Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stopped_Flag</fullName>
        <description>Set Stopped Flag on Case record to pause Entitlement clock.</description>
        <field>IsStopped</field>
        <literalValue>1</literalValue>
        <name>Set Stopped Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stopped_Flag_to_False</fullName>
        <description>Set Stopped flag to False to restart entitlement clock.</description>
        <field>IsStopped</field>
        <literalValue>0</literalValue>
        <name>Set Stopped Flag to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_isCloseFAL_to_True</fullName>
        <field>isClosedFAL__c</field>
        <literalValue>1</literalValue>
        <name>Set isCloseFAL to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_isClose_to_false</fullName>
        <field>isClosedFAL__c</field>
        <literalValue>0</literalValue>
        <name>Set isClose to false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stamp_Original_Case_Handler_ID</fullName>
        <field>Original_Case_Handler_ID__c</field>
        <formula>IF(ISBLANK( Original_Case_Handler_ID__c  ),  Owner:User.Id , Original_Case_Handler_ID__c)</formula>
        <name>Stamp Original Case Handler ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Store_Case_recordtype_name</fullName>
        <field>RecordTypeName__c</field>
        <formula>RecordType.Name</formula>
        <name>Store Case recordtype name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Submitted_Peer_Review_Yes</fullName>
        <field>Submitted_for_Peer_Review__c</field>
        <formula>&quot;Yes&quot;</formula>
        <name>Set Submitted For Peer Review to Yes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Supply_Update_Isclosed_status_to_true</fullName>
        <field>isClosedFAL__c</field>
        <literalValue>1</literalValue>
        <name>Supply Update Isclosed status to true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Supply_case_status_assigned_as_Assigned</fullName>
        <field>Status</field>
        <literalValue>Assigned</literalValue>
        <name>Supply case status assigned as Assigned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Athena_user_manager_email_in_case</fullName>
        <description>Update Athena user manager email in case object so that escalation emails can be send to case owner manager as well</description>
        <field>Athena_Owners_Manager_Email__c</field>
        <formula>IF( Owner_is_Queue__c , Owner:Queue.QueueEmail , Owner:User.Manager.Email)</formula>
        <name>Update Athena user manager email in case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Record_Type</fullName>
        <description>This would update record type name of a Case in the text field, required for use in validation rule</description>
        <field>Case_Record_Type_Name__c</field>
        <formula>RecordType.DeveloperName</formula>
        <name>Update Case Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_concat</fullName>
        <description>Update case contact field</description>
        <field>Case_Concat__c</field>
        <formula>Site__r.Location_Id__c  +  Contact.Id</formula>
        <name>Update Case concat</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_DP_Captured_Bol</fullName>
        <description>This will Update DP Captured Bol</description>
        <field>DP_Captured_Bol__c</field>
        <literalValue>1</literalValue>
        <name>Update DP Captured Bol</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_DP_Status_Changed_Date</fullName>
        <description>Update DP Resolution Status Changed Date</description>
        <field>DP_Resolution_Change_Date__c</field>
        <formula>NOW()</formula>
        <name>Update DP Status Changed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_FAL_Escalated_Date</fullName>
        <field>FAL_Escalated_Case_Date__c</field>
        <formula>LastModifiedDate</formula>
        <name>Update FAL Escalated Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_First_DP_Re_Assignment_Date</fullName>
        <description>This field will Update First DP Re-Assignment Date</description>
        <field>First_DP_Re_Assignment_Date__c</field>
        <formula>DP_Assigned_Date__c</formula>
        <name>Update First DP Re-Assignment Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_First_Resolution_Date</fullName>
        <field>First_Resolution_Date__c</field>
        <formula>ClosedDate</formula>
        <name>Update First Resolution Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_First_Resolution_Date_Bol</fullName>
        <field>First_Resolution_Date_Bol__c</field>
        <literalValue>1</literalValue>
        <name>Update First Resolution Date Bol</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_First_SME_Re_Assignement_Date</fullName>
        <description>This will Update First SME Re-Assignement Date field</description>
        <field>First_SME_Re_Assignment_Date__c</field>
        <formula>SME_Assigned_Date__c</formula>
        <name>Update First SME Re-Assignement Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_HSE_Perm_Resolution_To_None</fullName>
        <field>HSE_Permanent_Resolution__c</field>
        <name>Update HSE Perm Resolution To None</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_HSE_Perm_Resolution_To_Pending_As</fullName>
        <field>HSE_Permanent_Resolution__c</field>
        <literalValue>Pending Assessment</literalValue>
        <name>Update HSE Perm Resolution To Pending As</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Made_Safe_To_None</fullName>
        <field>Made_Safe_Status__c</field>
        <name>Update Made Safe To None</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Made_Safe_To_Pending_Assessment</fullName>
        <field>Made_Safe_Status__c</field>
        <literalValue>Pending Assessment</literalValue>
        <name>Update Made Safe To Pending Assessment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Number_of_Message_Received</fullName>
        <field>Number_of_Messages_Received__c</field>
        <formula>IF(ISBLANK(Number_of_Messages_Received__c), 1, Number_of_Messages_Received__c+1)</formula>
        <name>Update Number of Message Received</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Open_Escalated_Date</fullName>
        <field>Open_Escalated_Date__c</field>
        <formula>LastModifiedDate</formula>
        <name>Update Open- Escalated Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Original_Case_Created_Date</fullName>
        <description>Update Original Case Created Date</description>
        <field>Original_Case_Created_Date__c</field>
        <formula>Parent.CreatedDate</formula>
        <name>Update Original Case Created Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Pending_Date</fullName>
        <field>Pending_Date__c</field>
        <formula>LastModifiedDate</formula>
        <name>Update Pending Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Re_Opened_Date_Time</fullName>
        <field>Reopened_Date_Time__c</field>
        <formula>LastModifiedDate</formula>
        <name>Update Re-Opened Date/Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Request_Received_Date</fullName>
        <description>Update Request Received Date</description>
        <field>Request_Received_Date__c</field>
        <formula>CreatedDateTime__c</formula>
        <name>Update Request Received Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SME_Captured_Bol</fullName>
        <description>This will Update SME Captured Bol</description>
        <field>SME_Captured_Bol__c</field>
        <literalValue>1</literalValue>
        <name>Update SME Captured Bol</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SME_Resolution_Reason</fullName>
        <field>SME_Resolution_Reason__c</field>
        <literalValue>Resolution Met</literalValue>
        <name>Update SME Resolution Reason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Site_Address_on_case</fullName>
        <field>Site_Address__c</field>
        <formula>Site__r.Site_Address__c</formula>
        <name>Update Site Address on case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Site_Lock_Id_on_Case</fullName>
        <field>Loc_Id__c</field>
        <formula>Site__r.Location_Id__c</formula>
        <name>Update Site Lock Id on Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>srefef</fullName>
        <field>Description</field>
        <formula>OwnerId</formula>
        <name>sref</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Alert Escalation Drop Box on New Urgent Case</fullName>
        <actions>
            <name>Alert_the_case_management_team_when_the_Urgent_Complaint_case_is_created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notify Escalations team when an urgent case is created.</description>
        <formula>AND(NOT( ISPICKVAL( SME_Team__c , &apos;&apos;) ), RecordType.DeveloperName = &apos;Urgent_Complaint&apos; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Athena Case Created</fullName>
        <actions>
            <name>Email_Alert_to_Athena_team</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>When Athena case has been manually created, send email alert to Case owner</description>
        <formula>AND( NOT(Case_Owner__c = &apos;Athena Team&apos;), RecordType.DeveloperName = &apos;Athena&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Athena Case Escalated</fullName>
        <actions>
            <name>Email_Alert_to_Athena_team</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>When Athena case owner has changed, send email notification</description>
        <formula>AND( ISCHANGED( Case_Owner__c), NOT(Case_Owner__c = &apos;Athena Team&apos;), RecordType.DeveloperName = &apos;Athena&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Athena Email on Case not read</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Athena</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.New_Email__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Athena case&apos;s email has not been open the last 3 days</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Athena_Team_incoming_email</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.Last_Email_Received__c</offsetFromField>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Capture Original Case created date</fullName>
        <actions>
            <name>Capture_Created_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Complex Query</value>
        </criteriaItems>
        <description>Capture Complex Query Case created date to be use on Athena case</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Capturing FAL Escalated Date</fullName>
        <actions>
            <name>Update_FAL_Escalated_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This is used to capture the date whenever the status of FAL case changed to Escalated.</description>
        <formula>AND(ISCHANGED(Status), ISPICKVAL(Status,&apos;Escalated&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Capturing First DP Re-Assignment Date</fullName>
        <actions>
            <name>Update_DP_Captured_Bol</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_First_DP_Re_Assignment_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This will Capture First DP Re-Assignment Date</description>
        <formula>AND(  DP_Captured_Bol__c  = false,  PRIORVALUE( DP_Team__c )= &quot;None&quot; ,  NOT( ISPICKVAL(DP_Team__c , &quot;None&quot;) )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Capturing First Resolution Date</fullName>
        <actions>
            <name>Update_First_Resolution_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_First_Resolution_Date_Bol</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.First_Resolution_Date_Bol__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>This workflow is used to capture the date when the case got closed for the first time, so that it can be used for reporting purpose.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Capturing First SME Re-Assignment Date</fullName>
        <actions>
            <name>Update_First_SME_Re_Assignement_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_SME_Captured_Bol</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This field will capture Capturing First SME Re-Assignment Date.</description>
        <formula>AND( SME_Captured_Bol__c = false,   OR(  PRIORVALUE(SME_Team__c)= &quot;None&quot; , PRIORVALUE(SME_Team__c)= &quot;Default Team&quot;),    NOT( ISPICKVAL(SME_Team__c, &quot;Default Team&quot;) )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Capturing Open-Escalated Date</fullName>
        <actions>
            <name>Update_Open_Escalated_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This is used to capture the date whenever the status of case changed to Open-Escalated.</description>
        <formula>AND(ISCHANGED(Status), ISPICKVAL(Status,&apos;Open - Escalated&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Capturing Pending Date</fullName>
        <actions>
            <name>Update_Pending_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This is used to capture the date whenever the status of case changed to Pending.</description>
        <formula>AND(ISCHANGED(Status), ISPICKVAL(Status,&apos;Pending&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Capturing Re-Opened Date-Time</fullName>
        <actions>
            <name>Update_Re_Opened_Date_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This Workflow is created to Capture the latest date value when the case status will get changed from Closed to Re-opened.</description>
        <formula>AND(ISPICKVAL(PRIORVALUE(Status),&quot;Closed&quot;),ISPICKVAL(Status,&quot;Re-Opened&quot;),ISCHANGED(Status))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case Creation From External Source Workflow</fullName>
        <actions>
            <name>Number_Of_Msg_Received</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND (3 OR 4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Query</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Formal Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Web</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Twitter</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Creation Workflow</fullName>
        <actions>
            <name>Update_Number_of_Message_Received</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Query</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Formal Complaint</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Status Re-open Workflow</fullName>
        <actions>
            <name>Case_Status_update_from_closed_to_Reopen</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Query</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Formal Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Check the Athena Cases for any changes</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Athena</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>If the Athena case not modified for 5 business days, the system must send an email reminder to the assigned agent OR Athena case age is more then 15 business days, the system must send an email reminder to the assigned agent and to their Manager.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_email_reminder_to_Athena_Agents</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.Capture_custom_last_modified_date__c</offsetFromField>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Send_email_reminder_to_Athena_Agents_and_their_manager</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.Capture_custom_case_open_date__c</offsetFromField>
            <timeLength>15</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Complex Complaint or Urgent Complaint case closure Email Alert</fullName>
        <active>false</active>
        <booleanFilter>(1 OR 2) AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Complex Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Urgent Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Casus_Status_Icon__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>notEqual</operation>
            <value>{!$User.Id}</value>
        </criteriaItems>
        <description>Alert when a Complex complaint or urgent complaint is closed by a different user other than the case owner</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Credit Check Case Commenced</fullName>
        <actions>
            <name>Set_Commencement_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Stopped_Flag_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsStopped</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Not Started</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Credit Check</value>
        </criteriaItems>
        <description>When a Credit Check case Status changes from Not Completed and the Entitlement is paused, start it and set the Commencement Date.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Credit Check Case Created</fullName>
        <actions>
            <name>Set_Stopped_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Credit Check</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Not Started</value>
        </criteriaItems>
        <description>Set the Stopped flag on creation of Credit Check case if Status &quot;Not Started&quot;</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FAL Case Created</fullName>
        <actions>
            <name>Set_Case_Owner_FAL_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Fire &amp; Lift</value>
        </criteriaItems>
        <description>Assigns case owner for new Fire and Lift records.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FAL Case Peer Review Assigned</fullName>
        <actions>
            <name>Prevent_Review_by_Handler</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Updates case owner for peer review</description>
        <formula>AND( RecordType.DeveloperName == &apos;Fire_Lift&apos;  , ISPICKVAL(Status,&apos;Pending Peer Review&apos;) ,  Original_Case_Handler_ID__c ==   Current_Case_Owner_ID__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FAL Case Status Updated %28Escalated%29</fullName>
        <actions>
            <name>Email_Alert_to_M_D_Escalation_Team</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Fire &amp; Lift</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Escalated</value>
        </criteriaItems>
        <description>Updates case owner to M&amp;D team for escalations</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FAL Case Status Updated %28FInal Review%29</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Final Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Fire &amp; Lift</value>
        </criteriaItems>
        <description>Updates case owner to final review M&amp;D team</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FAL Case Status Updated %28Peer Review%29</fullName>
        <actions>
            <name>Set_Original_Case_Handler_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Stamp_Original_Case_Handler_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Submitted_Peer_Review_Yes</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates case owner for peer review</description>
        <formula>AND(RecordType.DeveloperName = &apos;Fire_Lift&apos;  , ISPICKVAL(Status,&apos;Pending Peer Review&apos;) , ISBLANK( Submitted_for_Peer_Review__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FAL Case Status Updated With Check %28Final Review%29</fullName>
        <actions>
            <name>Email_Alert_to_FAL_Review_Team</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Set_Case_Owner_FAL_Final_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates case owner to final review M&amp;D team</description>
        <formula>TEXT(Status) = &apos;Pending Final Review&apos; &amp;&amp; RecordType.DeveloperName = &apos;Fire_Lift&apos; &amp;&amp; ISPICKVAL(Peer_Review_Passed_Picklist__c , &apos;Yes&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FAL Case Status Updated With Check %28Peer Review%29</fullName>
        <actions>
            <name>Set_Case_Owner_FAL_Pending_Peer_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates case owner for peer review</description>
        <formula>AND(RecordType.DeveloperName = &apos;Fire_Lift&apos; , ISPICKVAL(Status,&apos;Pending Peer Review&apos;) , ISCHANGED(Status), NOT(ISPICKVAL(Peer_Review_Passed_Picklist__c , &apos;Yes&apos;)), NOT(ISBLANK(Original_Case_Handler_ID__c)), NOT(ISBLANK( Original_Case_Handler_Name__c )))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HSE_PermResolution _Update</fullName>
        <actions>
            <name>Update_HSE_Perm_Resolution_To_Pending_As</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update HSE Permanent Resolution to &apos;Pending Assessment&apos;</description>
        <formula>((ISPICKVAL(HSE_Impact__c, &apos;Yes&apos;)&amp;&amp; ISCHANGED(HSE_Impact__c)) || ( ISNEW() &amp;&amp; ISPICKVAL(HSE_Impact__c, &apos;Yes&apos;)))&amp;&amp; (RecordType.DeveloperName = &apos;Formal_Complaint&apos; || RecordType.DeveloperName = &apos;Complex_Complaint&apos; || RecordType.DeveloperName = &apos;Urgent_Complaint&apos;) &amp;&amp; ISBLANK(TEXT( HSE_Permanent_Resolution__c  ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Jeopardy Case SLA due date email notification</fullName>
        <active>false</active>
        <formula>IF( RecordType.Name  = &apos;Jeopardy&apos; &amp;&amp; ISPICKVAL(Status  , &apos;Open&apos;), True, False)</formula>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Jeopardy_Case_SLA_Due_date_notification</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.SLA_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>LMS Enquiries Record Type Change</fullName>
        <actions>
            <name>LMS_Enquiries_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( ISCHANGED(OwnerId), Owner:Queue.QueueName = &quot;LMS Enquiries&quot; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LMS High Priority Cases</fullName>
        <actions>
            <name>LMS_High_Priority_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>contains</operation>
            <value>urgent,tio complaint,ombudsmen</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>LMS Serviceability,LMS Enquiries,LMS Inservice</value>
        </criteriaItems>
        <description>Will change the status of LMS Cases if the subject contain the words Urgent, TIO Complaint or Ombudsmen</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LMS Inservice Record Type Change</fullName>
        <actions>
            <name>LMS_Inservice_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( ISCHANGED(OwnerId), OR( Owner:Queue.QueueName = &quot;LMS Inservice&quot;, Owner:Queue.QueueName = &quot;LMS Inservice SDU Team&quot;, Owner:Queue.QueueName = &quot;LMS Inservice MDU Team&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LMS Serviceability Record Type Change</fullName>
        <actions>
            <name>LMS_Serviceability_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( ISCHANGED(OwnerId), Owner:Queue.QueueName = &quot;LMS Serviceability&quot; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MAR Case Created</fullName>
        <actions>
            <name>Set_Case_Owner_MAR_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeName__c</field>
            <operation>equals</operation>
            <value>Medical Alarm</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>MAR Case Reopened</fullName>
        <actions>
            <name>Set_Case_Owner_Re_opened_MAR_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(RecordType.DeveloperName = &apos;MAR&apos; , ISPICKVAL(Status,&apos;Re-Opened&apos;),ISCHANGED(Status))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MAR Case Status Updated %28Escalated%29</fullName>
        <actions>
            <name>Email_Alert_to_MAR_M_D_Escalation_Team</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Updates case owner to M&amp;D team for escalations</description>
        <formula>AND(RecordType.DeveloperName = &apos;MAR&apos; , ISPICKVAL(Status,&apos;Escalated&apos;),ISCHANGED(Status))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MAR Case Status Updated %28Final Review%29</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Final Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Medical Alarm</value>
        </criteriaItems>
        <description>Updates case owner to final review M&amp;D team</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MAR Case Status Updated %28Peer Review%29</fullName>
        <actions>
            <name>MAR_Original_Case_Handler_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>MAR_Set_Submitted_For_Peer_Review_to_Yes</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>MAR_Stamp_Original_Case_Handler_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates case owner for peer review</description>
        <formula>AND(RecordType.DeveloperName = &apos;MAR&apos; , ISPICKVAL(Status,&apos;Pending Peer Review&apos;) , ISBLANK( Submitted_for_Peer_Review__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MAR Case Status Updated With Check %28Final Review%29</fullName>
        <actions>
            <name>Email_Alert_to_MAR_Review_Team</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Set_Case_Owner_MAR_Final_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates case owner to final review M&amp;D team</description>
        <formula>AND(RecordType.DeveloperName = &apos;MAR&apos; , ISPICKVAL(Status,&apos;Pending Final Review&apos;),ISCHANGED(Status), ISPICKVAL(Peer_Review_Passed_Picklist__c , &apos;Yes&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MAR Case Status Updated With Check %28Peer Review%29</fullName>
        <actions>
            <name>Set_Case_Owner_MAR_Pending_Peer_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates case owner for peer review</description>
        <formula>AND(RecordType.DeveloperName = &apos;MAR&apos;  , ISPICKVAL(Status,&apos;Pending Peer Review&apos;),ISCHANGED(Status),   NOT(ISPICKVAL(Peer_Review_Passed_Picklist__c , &apos;Yes&apos;)) ,  NOT(ISBLANK(Original_Case_Handler_ID__c)),  NOT(ISBLANK( Original_Case_Handler_Name__c )))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MadeSafe And HSE Perm Resolution None</fullName>
        <actions>
            <name>Update_HSE_Perm_Resolution_To_None</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Made_Safe_To_None</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Made Safe Status and HSE Perm Resolution to &apos;None&apos; when HSE Impact is  set to &apos;No&apos;</description>
        <formula>NOT(ISPICKVAL(HSE_Impact__c, &apos;Yes&apos;))&amp;&amp; (ISCHANGED(HSE_Impact__c) ||  ISNEW() ) &amp;&amp; (RecordType.DeveloperName = &apos;Formal_Complaint&apos; || RecordType.DeveloperName = &apos;Complex_Complaint&apos; || RecordType.DeveloperName = &apos;Urgent_Complaint&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MadeSafe _Update</fullName>
        <actions>
            <name>Update_Made_Safe_To_Pending_Assessment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Made Safe Status to &apos;Pending Assessment&apos;</description>
        <formula>((ISPICKVAL(HSE_Impact__c, &apos;Yes&apos;)&amp;&amp;  ISCHANGED(HSE_Impact__c)) || ( ISNEW()  &amp;&amp; ISPICKVAL(HSE_Impact__c, &apos;Yes&apos;)))&amp;&amp; (RecordType.DeveloperName = &apos;Formal_Complaint&apos;  ||  RecordType.DeveloperName = &apos;Complex_Complaint&apos; || RecordType.DeveloperName = &apos;Urgent_Complaint&apos;) &amp;&amp; ISBLANK(TEXT( Made_Safe_Status__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>NI Populate current date for email cases</fullName>
        <actions>
            <name>NI_Update_Config_Sent_to_Tech_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>NI_Update_Integration_CRQ_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>NI_Update_Scheduled_Date_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Network Integrations</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Network Integration: Default current date (NOW()) for 
1. Scheduled Date/Time
2. Integration CRQ Date
3. Config Sent to Tech Date</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Notification RM Operations on High Risk</fullName>
        <actions>
            <name>Notify_RM_Operations_Team</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND ( 3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Case.Case_Record_Type_Name__c</field>
            <operation>contains</operation>
            <value>Query,Complex_Query,Formal_Complaint,Urgent_Complaint,Complex_Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.HSE_Category__c</field>
            <operation>equals</operation>
            <value>Pedestrian Safety (Trips and Falls),Traffic and Vehicle Safety,Asbestos and Chemicals,Electrical and Fire Safety,Falling Objects,Emergency Services Communication,Noise and Nuisance,Pollution and Waste,&quot;Flora, Fauna or Heritage&quot;</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sub_Category__c</field>
            <operation>equals</operation>
            <value>HSE Incident</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.HSE_Impact__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>Notify Rm Operation when High Risk is populated</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify HSE Operations on HSE Impact</fullName>
        <actions>
            <name>Notify_HSE_Operations_Team</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2 OR 3 OR 4 OR 5) AND (6 OR 7)</booleanFilter>
        <criteriaItems>
            <field>Case.Case_Record_Type_Name__c</field>
            <operation>equals</operation>
            <value>Query</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Record_Type_Name__c</field>
            <operation>equals</operation>
            <value>Complex_Query</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Record_Type_Name__c</field>
            <operation>equals</operation>
            <value>Formal_Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Record_Type_Name__c</field>
            <operation>equals</operation>
            <value>Urgent_Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Record_Type_Name__c</field>
            <operation>equals</operation>
            <value>Complex_Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sub_Category__c</field>
            <operation>equals</operation>
            <value>HSE Incident</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.HSE_Impact__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>Rule to notify Rm Operations team when there&apos;s a HSE Impact</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Prevent duplication of cases</fullName>
        <actions>
            <name>Update_Case_concat</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Medical Alarm</value>
        </criteriaItems>
        <description>Duplication of cases.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Query Case Status Update to Close</fullName>
        <actions>
            <name>Query_Case_Status_Update_to_Close</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>IF(RecordType.DeveloperName == &apos;Query&apos;,  IF(ISPICKVAL(Origin,&apos;Web&apos;) &amp;&amp; ISNEW(),false,true)  ,false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ResponsibleEntityDescriptionEmailNotification</fullName>
        <actions>
            <name>Responsible_Entity_Description_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Responsible_Entity_Description__c ) &amp;&amp;   Responsible_Entity_Description_Email__c != NULL</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ResponsibleEntityOwnerEmailNotification</fullName>
        <actions>
            <name>Responsible_Entity_Owner_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Responsible_Entity_Owner__c) &amp;&amp;  Responsible_Entity_Owner_Email__c != NULL</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Rule to populate Assigned Date for Aged Cases</fullName>
        <actions>
            <name>FU_to_populate_Assigned_Date_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Cust Conn CM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Assigned_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>notEqual</operation>
            <value>Customer Connections Team</value>
        </criteriaItems>
        <description>US#4571: This workflow rule will populate the current date when the Aged Order case is assigned to a user from Customer Connections queue.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Email to CDM on Case Closed</fullName>
        <actions>
            <name>Send_email_to_case_owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>contains</operation>
            <value>Activations Jeopardy,Assurance Jeopardy,Escalations Triage</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>notEqual</operation>
            <value>GoCD Interface,Default Data Migration User,SAIL Interface</value>
        </criteriaItems>
        <description>When RSP Escalation cases are closed, notify the CDM</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Email to CDM on Case Resolved</fullName>
        <actions>
            <name>Send_email_to_case_owner_Resolved</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>contains</operation>
            <value>Activations Jeopardy,Assurance Jeopardy,Escalations Triage</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Resolved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>notEqual</operation>
            <value>GoCD Interface,Default Data Migration User,SAIL Interface</value>
        </criteriaItems>
        <description>When RSP Escalation cases are resolved, notify the CDM</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Email to SME User for Commercial Billing Case</fullName>
        <active>true</active>
        <booleanFilter>(1 OR 2 OR 3 OR 4 OR 5) AND 6</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Credit &amp; Collections</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Billing Enquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Billing Claim</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Credit Check</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Billing Dispute</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SME_Resolution_Status__c</field>
            <operation>notEqual</operation>
            <value>Activities Complete</value>
        </criteriaItems>
        <description>Notify SME User to Commercial Billing Case been assigned</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Case_SLA_expires</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.SLA_Date__c</offsetFromField>
            <timeLength>-48</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Send RSP Escalation Case Assignment Notification</fullName>
        <actions>
            <name>Send_Email_to_Customer_Delivery_Suppert_Team</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Reset_RSP_Escalated_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Notify CDS Team when RSP Escalated through Email</description>
        <formula>AND(ISCHANGED(OwnerId),( Owner:Queue.QueueName = &apos;Customer Delivery Support Team&apos;), OR ((RecordTypeName__c = &apos;Activations Jeopardy&apos;), (RecordTypeName__c = &apos;Assurance Jeopardy&apos;), (RecordTypeName__c = &apos;Escalations Triage&apos;)), RSP_Escalated__c = true)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send email when Athena Case is created</fullName>
        <actions>
            <name>Send_Athena_Case_Creation_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Athena</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Phone,Email</value>
        </criteriaItems>
        <description>Send email when Athena Case is created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Case status %28Supply chain%29</fullName>
        <actions>
            <name>Supply_case_status_assigned_as_Assigned</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set the status of the case to the Assigned when a case is created</description>
        <formula>AND(ISCHANGED(Case_Owner__c), Owner:Queue.DeveloperName  &lt;&gt;  &apos;Supply_Chain_Queue&apos;,  RecordType.DeveloperName = &apos;Supply_Chain&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set DP Resolution Status Change Date</fullName>
        <actions>
            <name>Update_DP_Status_Changed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set DP Resolution Status Change Date when DP Resolution Status field changes</description>
        <formula>ISCHANGED(  DP_Resolution_Status__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set SME Resolution Reason To Resolution Met For NBN Billing Credit and Collections Team</fullName>
        <actions>
            <name>Update_SME_Resolution_Reason</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISCHANGED(SME_Resolution_Status__c),ISPICKVAL(SME_Resolution_Status__c , &apos;Activities Complete&apos;), RecordType.DeveloperName = &apos;Billing_New_Development_Enquiry&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set SME Resolution Status Change Date</fullName>
        <actions>
            <name>Set_SME_Resolution_Change_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set SME Resolution Status Change Date when SME Resolution Status field changes</description>
        <formula>ISCHANGED( SME_Resolution_Status__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Supply Chain Reopen after closing a case</fullName>
        <actions>
            <name>Supply_Update_Isclosed_status_to_true</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Supply Chain</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Supply chain created</fullName>
        <actions>
            <name>Set_Case_Owner_Supply_chain_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Supply Chain</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Unassigned</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Supply chain-closed notification</fullName>
        <actions>
            <name>Supply_chain_closed_notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Supply Chain</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>High</value>
        </criteriaItems>
        <description>Supply chain closed notification for high priority cases</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Case Owners Manager Email in Athena Case</fullName>
        <actions>
            <name>Update_Athena_user_manager_email_in_case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Athena</value>
        </criteriaItems>
        <description>Update Case Owners Manager Email in Athena Case</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Case Record Type</fullName>
        <actions>
            <name>Store_Case_recordtype_name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Case_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow rule is required to update Case Record Type Name in the custom field</description>
        <formula>AND(OR(ISNEW(),NOT(ISNULL(Id))),NOT(OR( RecordType.DeveloperName = &apos;DP_SLA_Reporting&apos;,RecordType.DeveloperName = &apos;SME_SLA_Reporting&apos;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Child AthenaCase DateFields</fullName>
        <actions>
            <name>Update_Original_Case_Created_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Athena</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ParentId</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Update Child Athena Case Date Fields</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update NBNComplaintEscalations Team when TIO is Changes</fullName>
        <actions>
            <name>Notify_NBNComplaintEscalations_Team_and_CSGoldCoastSCCC_Team</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>TIO_Media_MP_issue_Alert</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Notify NBN Complaint Escalations Team when TIO is Changes.</description>
        <formula>ISCHANGED( TIO__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Site Loc Id on Case</fullName>
        <actions>
            <name>Update_Site_Address_on_case</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Site_Lock_Id_on_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Loc id on searchable text field so that it can be used for searching on UBER page.</description>
        <formula>AND( NOT(ISBLANK(Site__c)), OR(ISNEW(), ISCHANGED(Site__c)),NOT(OR( RecordType.DeveloperName = &apos;DP_SLA_Reporting&apos;,RecordType.DeveloperName = &apos;SME_SLA_Reporting&apos;)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Updating isClosedFAL to False</fullName>
        <actions>
            <name>Set_isClose_to_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( ISCHANGED(Status),ISPICKVAL(Status, &apos;Re-Opened&apos;), RecordTypeName__c=&apos;Fire &amp; Lift&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Updating isClosedFAL to True</fullName>
        <actions>
            <name>Set_isCloseFAL_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( ISCHANGED(Status),ISPICKVAL(Status, &apos;Closed&apos;), RecordTypeName__c=&apos;Fire &amp; Lift&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF_ToUnCheckSave%26CloseFlag</fullName>
        <actions>
            <name>FU_SaveAndCloseFlag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Is_Save_And_Close_Button_Clicked__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>LiFD</value>
        </criteriaItems>
        <description>During the LiFD Case creation from Search Wizard, If the Status is set to Closed, From Code we are setting the Flag &quot;Is Save and Close Button Clicked&quot; to True. This WF rule will set the Flag to False.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>New_Case_Assigned</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>3-General</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>New Case Assigned</subject>
    </tasks>
    <tasks>
        <fullName>TIO_Media_MP_issue_Alert</fullName>
        <assignedToType>owner</assignedToType>
        <description>Email Notification sent to NBNComplaintEscalations@nbnco.com.au;CSGoldCoastSCCC@nbnco.com.au</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.Now__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Closed</status>
        <subject>TIO/ Media/ MP issue Alert</subject>
    </tasks>
</Workflow>
