<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Alert_NBNComplaintEscalations_Team</fullName>
        <ccEmails>NBNComplaintEscalations@nbnco.com.au</ccEmails>
        <description>Alert NBNComplaintEscalations Team</description>
        <protected>false</protected>
        <recipients>
            <recipient>icrmprocessautomationuser@nbnco.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Templates/Notification_for_H_S_Considerations1</template>
    </alerts>
    <fieldUpdates>
        <fullName>UniqueContactCheckUpdate</fullName>
        <field>UniqueContactCheck__c</field>
        <formula>Contact__c+ Case__c</formula>
        <name>UniqueContactCheckUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Check for Unique Contact Association</fullName>
        <actions>
            <name>UniqueContactCheckUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow rule would be required to update UniqueContactCheck field with concatenation of Case Id and Contact Id</description>
        <formula>ISNEW() || NOT(ISNULL(Id))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notification for New H%26S Considerations for Case</fullName>
        <actions>
            <name>Alert_NBNComplaintEscalations_Team</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Case_Record_Type_Name__c</field>
            <operation>contains</operation>
            <value>Query,Complex_Query,Formal_Complaint,Urgent_Complaint,Complex_Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case_Contact__c.Health_Safety_Considerations__c</field>
            <operation>includes</operation>
            <value>Medical Alarm,Priority Assist,Life Threatening Condition</value>
        </criteriaItems>
        <description>Notify Case Owner when Health &amp; Safety Considerations changes for Query ,Complex Query,Formal complaint , Urgent Complaint.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
