<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>NewDev_Portal_Access_Code_Confirmation_Email_Alert</fullName>
        <description>NewDev Portal Access Code Confirmation Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>New_Developments_Emails/NewDev_Portal_Access_Code_Confirmation_Email_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>FU_ToUnCheckCreatedFromSearchWizard</fullName>
        <description>ICRM-448: This FU will uncheck the Created From SearchWizard flag if it is checked during the creation of contact.</description>
        <field>Created_From_SearchWizard__c</field>
        <literalValue>0</literalValue>
        <name>FU_ToUnCheckCreatedFromSearchWizard</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ICT_Populate_Contact_Source</fullName>
        <description>Assign &apos;ICT&apos; value to &apos;Contact Source&apos; field so that can be identified as ICT Contact</description>
        <field>Contact_Source__c</field>
        <literalValue>ICT</literalValue>
        <name>ICT Populate Contact Source</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Contact_Resend_Flag</fullName>
        <field>Resend_OBMsg__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck_Contact_Resend_Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>DCB_Contact_Updates</fullName>
        <actions>
            <name>Uncheck_Contact_Resend_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PA_SF_UpdateContact_v1_0</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Workflow to send an outbound notification to EBS to synchronise data.</description>
        <formula>OR( AND( IF( ISBLANK( AR_Contact_ID__c ), false, true ) , OR( ISCHANGED( FirstName ) , ISCHANGED( LastName ) , ISCHANGED( Salutation ) , ISCHANGED( Job_Title__c ) , ISCHANGED( Email ) , ISCHANGED( Phone ) , ISCHANGED( MobilePhone ) , ISCHANGED( HomePhone ) , ISCHANGED( OtherPhone ) , ISCHANGED( Assistant_Phone__c ) , ISCHANGED( Fax ) , ISCHANGED( Status__c ) ) ) , Resend_OBMsg__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ICT_Contact_Source_Assignment</fullName>
        <actions>
            <name>ICT_Populate_Contact_Source</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow will update the &apos;Contact Source&apos; field on Contact, if ICT Contact has created manually and not through the Lead Conversion</description>
        <formula>AND(  ISPICKVAL( LeadSource , &apos;&apos;),  CreatedBy.Profile.Name = &apos;BSM ICT&apos;,  ISPICKVAL( Contact_Source__c, &apos;&apos; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF_ToUnCheckCreatedFromSearchWizard</fullName>
        <actions>
            <name>FU_ToUnCheckCreatedFromSearchWizard</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Created_From_SearchWizard__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>ICRM-448: This WF rule will help to uncheck the Created From SearchWizard flag if it is checked during the creation of contact.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
