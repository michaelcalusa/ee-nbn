<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Contract_Resend_Uncheck_Flag</fullName>
        <field>Resend_OBMsg__c</field>
        <literalValue>0</literalValue>
        <name>Contract Resend Uncheck Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_Unckeck_Resend_Flag</fullName>
        <field>Resend_OBMsg__c</field>
        <literalValue>0</literalValue>
        <name>Contract_Unckeck_Resend_Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Contract_Status</fullName>
        <field>Status</field>
        <literalValue>Active</literalValue>
        <name>Update Contract Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>DCB_Contract_Address_and_Status_Updates</fullName>
        <actions>
            <name>Contract_Resend_Uncheck_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PA_SF_UpdateContract_v1_0</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Workflow to send an outbound notification to EBS to synchronise data.</description>
        <formula>OR( AND(  IF( ISBLANK( AR_Site_Use_ID__c ), false, true ),  OR( ISCHANGED( BillingCountry ),     ISCHANGED( BillingStreet ),        ISCHANGED( BillingCity ),      ISCHANGED( BillingPostalCode ),      ISCHANGED( BillingState ),     ISCHANGED( Status )       )   ), Resend_OBMsg__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>DCB_Contract_Contact_Updates</fullName>
        <actions>
            <name>Contract_Unckeck_Resend_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PA_SF_UpdateContactRole_v1_0</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Workflow to send an outbound notification to EBS to synchronise data.</description>
        <formula>(IF(ISBLANK(AR_Site_Use_ID__c),false,true) &amp;&amp; (ISCHANGED(  Billing_Contact__c) || ISCHANGED(Dunning_Contact__c))) ||  Resend_OBMsg__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Status to Active</fullName>
        <actions>
            <name>Update_Contract_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract.Record_Type_Name__c</field>
            <operation>equals</operation>
            <value>Recoverable Works,Recoverable Damages,NewDevs Class 1-2,NewDevs Class 3-4,Dunning</value>
        </criteriaItems>
        <description>On create, update status to Active if record type = Recoverable Works</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
