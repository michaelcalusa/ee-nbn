<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Held_Status_Age</fullName>
        <field>Held_Status_Age__c</field>
        <formula>IF(ISBLANK(Held_Status_Age__c),0,Held_Status_Age__c) + (TODAY() - DATEVALUE(PRIORVALUE(Last_Status_Change_Date__c)))</formula>
        <name>Update Held Status Age</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Held_Status_Count</fullName>
        <field>Held_Status_Number__c</field>
        <formula>IF(ISBLANK(Held_Status_Number__c),0,Held_Status_Number__c)+1</formula>
        <name>Update Held Status Count</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_InProgress_Status_Count</fullName>
        <field>InProgress_Status_Number__c</field>
        <formula>IF(ISBLANK(InProgress_Status_Number__c),0,InProgress_Status_Number__c)+1</formula>
        <name>Update InProgress Status Count</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_In_Progress_Status_Age</fullName>
        <field>In_Progress_Status_Age__c</field>
        <formula>IF(ISBLANK(In_Progress_Status_Age__c),0,In_Progress_Status_Age__c) + (TODAY() - DATEVALUE(PRIORVALUE(Last_Status_Change_Date__c)))</formula>
        <name>Update In Progress Status Age</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_IsRelated_R2T_on_Order_Completion</fullName>
        <field>IsRelatedCaseR2T__c</field>
        <literalValue>0</literalValue>
        <name>Update IsRelated R2T on Order Completion</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Last_Status_Change_Date</fullName>
        <field>Last_Status_Change_Date__c</field>
        <formula>NOW()</formula>
        <name>Update Last Status Change Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_PRD_Count</fullName>
        <field>Times_Plan_Remediation_Date_Changed__c</field>
        <formula>IF(ISBLANK(Times_Plan_Remediation_Date_Changed__c),0,Times_Plan_Remediation_Date_Changed__c)+1</formula>
        <name>Update PRD Count</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Pending_Status_Age</fullName>
        <field>Pending_Status_Age__c</field>
        <formula>IF(ISBLANK(Pending_Status_Age__c),0,Pending_Status_Age__c) + (TODAY() - DATEVALUE(PRIORVALUE(Last_Status_Change_Date__c)))</formula>
        <name>Update Pending Status Age</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Pending_Status_Count</fullName>
        <field>Pending_Status_Number__c</field>
        <formula>IF(ISBLANK(Pending_Status_Number__c),0,Pending_Status_Number__c)+1</formula>
        <name>Update Pending Status Count</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_field_with_current_datetime</fullName>
        <description>US#7420: This field update gets trigger by &apos;Rule to populate datetime when Order Status gets change&apos; workflow to populates the current datetime in Order Status Update field.</description>
        <field>Order_Status_Change__c</field>
        <formula>NOW()</formula>
        <name>Update field with current datetime</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Rule to populate datetime when Order Status gets change</fullName>
        <actions>
            <name>Update_field_with_current_datetime</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>US#7420: This workflow rule triggers when Status of the order gets change and then it populates the current datetime in Order Status Update field.</description>
        <formula>PRIORVALUE(Order_Status__c) != TEXT(Order_Status__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Held Order Age</fullName>
        <actions>
            <name>Update_Held_Status_Age</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(IsRelatedCaseR2T__c, OR(ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;Held&apos;), ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;Acknowledged - Held&apos;), ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;In progress - Held&apos;)) ,NOT(OR(ISPICKVAL(Order_Status__c,&apos;Held&apos;), ISPICKVAL(Order_Status__c,&apos;Acknowledged - Held&apos;), ISPICKVAL(Order_Status__c,&apos;In progress - Held&apos;))) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Held Status Count</fullName>
        <actions>
            <name>Update_Held_Status_Count</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  IsRelatedCaseR2T__c,  OR(  AND(  NOT(OR(ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;Held&apos;),ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;Acknowledged - Held&apos;),ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;In progress - Held&apos;))),(ISPICKVAL(Order_Status__c,&apos;Held&apos;)))  , AND(  NOT(OR(ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;Held&apos;),ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;Acknowledged - Held&apos;),ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;In progress - Held&apos;))),(ISPICKVAL(Order_Status__c,&apos;Acknowledged - Held&apos;)))  , AND(  NOT(OR(ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;Held&apos;),ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;Acknowledged - Held&apos;),ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;In progress - Held&apos;))),(ISPICKVAL(Order_Status__c,&apos;In progress - Held&apos;)))))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update In Progress Order Age</fullName>
        <actions>
            <name>Update_In_Progress_Status_Age</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(IsRelatedCaseR2T__c,ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;In progress&apos;),NOT(ISPICKVAL(Order_Status__c,&apos;In progress&apos;)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update InProgress Status Count</fullName>
        <actions>
            <name>Update_InProgress_Status_Count</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(IsRelatedCaseR2T__c,AND(NOT(ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;In progress&apos;)),(ISPICKVAL(Order_Status__c,&apos;In progress&apos;))  ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update IsRelated R2T on Order Completion</fullName>
        <actions>
            <name>Update_IsRelated_R2T_on_Order_Completion</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(OR(ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;Cancelled&apos;),ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;Complete&apos;))),OR(ISPICKVAL(Order_Status__c,&apos;Cancelled&apos;),ISPICKVAL(Order_Status__c,&apos;Complete&apos;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Last Status Change Date</fullName>
        <actions>
            <name>Update_Last_Status_Change_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( AND(IsRelatedCaseR2T__c,  OR(ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;Held&apos;),  ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;Acknowledged - Held&apos;),  ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;In progress - Held&apos;))  ,NOT(OR(ISPICKVAL(Order_Status__c,&apos;Held&apos;),  ISPICKVAL(Order_Status__c,&apos;Acknowledged - Held&apos;),  ISPICKVAL(Order_Status__c,&apos;In progress - Held&apos;)))  ), AND(IsRelatedCaseR2T__c,  OR(ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;Pending&apos;),  ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;Acknowledged - Pending&apos;),  ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;In progress - Pending&apos;))  ,NOT(OR(ISPICKVAL(Order_Status__c,&apos;Pending&apos;),  ISPICKVAL(Order_Status__c,&apos;Acknowledged - Pending&apos;),  ISPICKVAL(Order_Status__c,&apos;In progress - Pending&apos;)))  ), AND(IsRelatedCaseR2T__c,ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;In progress&apos;),NOT(ISPICKVAL(Order_Status__c,&apos;In progress&apos;))  ), AND(IsRelatedCaseR2T__c,  NOT(OR(ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;Held&apos;),  ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;Acknowledged - Held&apos;),  ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;In progress - Held&apos;), ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;Acknowledged - Pending&apos;), ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;In progress - Pending&apos;), ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;Pending&apos;), ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;In progress&apos;) ) ) , OR(ISPICKVAL(Order_Status__c,&apos;Held&apos;),  ISPICKVAL(Order_Status__c,&apos;Acknowledged - Held&apos;),  ISPICKVAL(Order_Status__c,&apos;In progress - Held&apos;), ISPICKVAL(Order_Status__c,&apos;Acknowledged - Pending&apos;), ISPICKVAL(Order_Status__c,&apos;In progress - Pending&apos;), ISPICKVAL(Order_Status__c,&apos;Pending&apos;), ISPICKVAL(Order_Status__c,&apos;In progress&apos;) ) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Pending Order Age</fullName>
        <actions>
            <name>Update_Pending_Status_Age</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(IsRelatedCaseR2T__c,  OR(ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;Pending&apos;),  ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;Acknowledged - Pending&apos;),  ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;In progress - Pending&apos;))  ,NOT(OR(ISPICKVAL(Order_Status__c,&apos;Pending&apos;),  ISPICKVAL(Order_Status__c,&apos;Acknowledged - Pending&apos;),  ISPICKVAL(Order_Status__c,&apos;In progress - Pending&apos;)))  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Pending Status Count</fullName>
        <actions>
            <name>Update_Pending_Status_Count</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  IsRelatedCaseR2T__c,  OR(  AND(  NOT(OR(ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;Pending&apos;),ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;Acknowledged - Pending&apos;),ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;In progress - Pending&apos;))),(ISPICKVAL(Order_Status__c,&apos;Pending&apos;)))  , AND(  NOT(OR(ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;Pending&apos;),ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;Acknowledged - Pending&apos;),ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;In progress - Pending&apos;))),(ISPICKVAL(Order_Status__c,&apos;Acknowledged - Pending&apos;)))  , AND(  NOT(OR(ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;Pending&apos;),ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;Acknowledged - Pending&apos;),ISPICKVAL(PRIORVALUE(Order_Status__c),&apos;In progress - Pending&apos;))),(ISPICKVAL(Order_Status__c,&apos;In progress - Pending&apos;)))))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Times PRD Changed</fullName>
        <actions>
            <name>Update_PRD_Count</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( IsRelatedCaseR2T__c, ISCHANGED(Plan_Remediation_Date__c), OR(DATEVALUE(PRIORVALUE(Plan_Remediation_Date__c))&lt;&gt;DATEVALUE(Plan_Remediation_Date__c),ISBLANK(Plan_Remediation_Date__c),ISBLANK(DATEVALUE(PRIORVALUE(Plan_Remediation_Date__c)))) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
