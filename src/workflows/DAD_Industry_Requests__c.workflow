<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Account_Id_populate_into_text_field_DAD</fullName>
        <description>This is used to populate account ID on text filed on  object.</description>
        <field>Dad_Industry_Requests_Unique__c</field>
        <formula>Company__r.Id</formula>
        <name>Account Id populate into text field_DAD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>DAD Industry Requests Unique per Account</fullName>
        <actions>
            <name>Account_Id_populate_into_text_field_DAD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>DAD_Industry_Requests__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This rule is used to populate account ID information into a text field(Unique) on DAD Industry Requests object to restrict to one Record per account</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
