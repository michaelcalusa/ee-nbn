<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Athena_Category_Update_to_Cand_Referral</fullName>
        <description>Update the Category field to Candidate Referral for Athena Record type</description>
        <field>Category__c</field>
        <literalValue>Candidate Referral</literalValue>
        <name>Athena Category Update to Cand Referral</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Athena_Subcategory_Update_to_Accreditati</fullName>
        <description>Athena Subcategory Update to Cand Query</description>
        <field>Sub_Category__c</field>
        <literalValue>Candidate_Query</literalValue>
        <name>Athena Subcategory Update to Accreditati</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Email_Received</fullName>
        <field>Damstra_New_E_mail__c</field>
        <literalValue>1</literalValue>
        <name>Case Email Received</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Status_update_from_Email_to_case</fullName>
        <field>Status</field>
        <literalValue>Re-Opened</literalValue>
        <name>Case Status update from Email to case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NI_Set_Case_status_to_Reopened</fullName>
        <field>Status</field>
        <literalValue>Re-Opened</literalValue>
        <name>NI Set Case status to Reopened</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NI_Set_Re_Opened_Date_Time</fullName>
        <field>Reopened_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>NI Set Re-Opened Date/Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Object_Email_Message</fullName>
        <field>Number_of_Messages_Received__c</field>
        <formula>IF(ISBLANK(Parent.Number_of_Messages_Received__c), 1, Parent.Number_of_Messages_Received__c+1)</formula>
        <name>Object:Email Message</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateLastModifiedChannel</fullName>
        <field>LastModifiedChannel__c</field>
        <literalValue>Email</literalValue>
        <name>UpdateLastModifiedChannel</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Last_Email_Received</fullName>
        <description>Update Last Email received field</description>
        <field>Last_Email_Received__c</field>
        <formula>NOW()</formula>
        <name>Update Last Email Received</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_New_Email_field</fullName>
        <field>New_Email__c</field>
        <literalValue>1</literalValue>
        <name>Update New Email field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_status_to_Information_Received</fullName>
        <field>Status</field>
        <literalValue>Information Received</literalValue>
        <name>Update status to Information Received</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Athena Case Email Received</fullName>
        <actions>
            <name>Update_Last_Email_Received</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_New_Email_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Athena</value>
        </criteriaItems>
        <description>When Email received on a Athena Case, update the Last Email Received field</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Status update from Email to case</fullName>
        <actions>
            <name>Case_Status_update_from_Email_to_case</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>UpdateLastModifiedChannel</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email To Case message received</fullName>
        <actions>
            <name>Object_Email_Message</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2 AND 3) OR ((3 OR 15) AND (4 OR 5 OR 6 OR 7 OR 8 OR 9 OR 10 OR 11 OR 12 OR 13 OR 14))</booleanFilter>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Query</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Facebook</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>App - nbn go</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Face to Face</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Fax</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Web</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Service Portal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Social Media Other</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Twitter</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Installation Survey</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Phone</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Formal Complaint</value>
        </criteriaItems>
        <description>Email To Case message received</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email message received</fullName>
        <actions>
            <name>Case_Email_Received</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Athena</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Escalated to Damstra</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>For Athena when email received on a Case and Priority is escalated to Damstra.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>For Athena Email Case update Category and Sub Category</fullName>
        <actions>
            <name>Athena_Category_Update_to_Cand_Referral</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Athena_Subcategory_Update_to_Accreditati</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Athena</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.ToAddress</field>
            <operation>equals</operation>
            <value>athenaCRMhelp@nbnco.com.au,eoi@nbnco.com.au</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email</value>
        </criteriaItems>
        <description>Emails received in the following mailboxes need to be automatically categorised: 
athenaCRMhelp@nbnco.com.au : Athena &gt; Cand Referral category 
Athena Subcategory Update to Cand Query 
eoi@nbnco.com.au = Athena &gt; CRM category</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>NI Reopen closed case on email response</fullName>
        <actions>
            <name>NI_Set_Case_status_to_Reopened</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>NI_Set_Re_Opened_Date_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Network Integrations</value>
        </criteriaItems>
        <description>Network Integration: When a user replies to a closed case in Salesforce, change the status to Re-Opened and update Reopened Date/Time fields on Case</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Supply chain-Information received</fullName>
        <actions>
            <name>Update_status_to_Information_Received</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Supply Chain</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.Subject</field>
            <operation>contains</operation>
            <value>RE:</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
