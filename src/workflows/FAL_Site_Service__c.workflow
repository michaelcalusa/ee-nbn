<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Record_Type_M_D</fullName>
        <description>Sets record type to M&amp;D to prevent updates</description>
        <field>RecordTypeId</field>
        <lookupValue>Migrations_Disconnections</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type - M&amp;D</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_Manual</fullName>
        <description>Sets record type to allow updates</description>
        <field>RecordTypeId</field>
        <lookupValue>Manual</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type - Manual</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>FAL Service Created or Updated %28MDDB Id Null%29</fullName>
        <actions>
            <name>Set_Record_Type_Manual</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>FAL_Site_Service__c.MDDB_Id__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Executes process changes for creates and updates</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FAL Service Created or Updated %28MDDB Id%29</fullName>
        <actions>
            <name>Set_Record_Type_M_D</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>FAL_Site_Service__c.MDDB_Id__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Executes process changes for creates and updates</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
