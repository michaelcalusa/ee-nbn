<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Knowledge_Email_Alert_For_Approval</fullName>
        <description>Knowledge Email Alert For Approval</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Knowledge_Management_Approvals/Knowledge_Article_Approval_Confirmation</template>
    </alerts>
    <alerts>
        <fullName>Knowledge_Email_Alert_For_Rejection</fullName>
        <description>Knowledge Email Alert For Rejection</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Knowledge_Management_Approvals/Knowledge_Article_Approval_Rejection</template>
    </alerts>
</Workflow>
