<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notify_Invoice_Credit_Memo_Owner_when_approved_internally</fullName>
        <description>Notify Invoice/Credit Memo Owner when approved internally</description>
        <protected>false</protected>
        <recipients>
            <field>Opportunity_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Recoverable_Works_Email_Templates/Invoice_Credit_Memo_Approved_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Notify_Invoice_Credit_Memo_Owner_when_rejected_internally</fullName>
        <description>Notify Invoice/Credit Memo Owner when rejected internally</description>
        <protected>false</protected>
        <recipients>
            <field>Opportunity_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Recoverable_Works_Email_Templates/Invoice_Credit_Memo_Rejected_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Notify_Owner_of_Invoice_Payment_Status_Change</fullName>
        <description>Notify Owner of Invoice Payment Status Change</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Recoverable_Works_Email_Templates/Invoice_Payment_Status_Change_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Notify_Owner_of_Invoice_Payment_Status_Change_RD</fullName>
        <ccEmails>nbnRDT@nbnco.com.au</ccEmails>
        <description>Notify Owner of Invoice Payment Status Change - RD</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Recoverable_Works_Email_Templates/Invoice_Payment_Status_Change_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Notify_Owner_of_Invoice_Status_Change</fullName>
        <description>Notify Owner of Invoice Status Change</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Recoverable_Works_Email_Templates/Invoice_Status_Change_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Notify_Owner_of_Invoice_Status_Change_RD</fullName>
        <ccEmails>nbnRDT@nbnco.com.au</ccEmails>
        <description>Notify Owner of Invoice Status Change - RD</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Recoverable_Works_Email_Templates/Invoice_Status_Change_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Send_Invoice_Approval_Request_Email_to_Opportunity_Owner</fullName>
        <description>Send Invoice Approval Request Email to Opportunity Owner</description>
        <protected>false</protected>
        <recipients>
            <field>Opportunity_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Recoverable_Works_Email_Templates/RW_Invoice_Approval_Submission_Template_HTML</template>
    </alerts>
    <fieldUpdates>
        <fullName>Sent_to_AR_date</fullName>
        <field>Sent_to_AR_date__c</field>
        <formula>now()</formula>
        <name>Sent to AR date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Fully_Paid_Date</fullName>
        <field>Fully_Paid_Date__c</field>
        <formula>Today()</formula>
        <name>Set Fully Paid Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_InvStatus_to_CancellationRequested</fullName>
        <description>Set Invoice Status to Cancellation Requested</description>
        <field>Status__c</field>
        <formula>&apos;Cancellation Requested&apos;</formula>
        <name>Set Status to Cancellation Requested</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Previous_Status_to_Blank</fullName>
        <field>Previous_Status__c</field>
        <name>Set Previous Status to Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Cancel_requested</fullName>
        <description>Set Status to Cancellation Requested</description>
        <field>Status__c</field>
        <formula>&apos;Cancellation Requested&apos;</formula>
        <name>Set status to Cancellation Rqstd NewDev</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Cancellation_Requested</fullName>
        <field>Status__c</field>
        <formula>&apos;Cancellation Requested&apos;</formula>
        <name>Set Status to Cancellation Requested</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Prior_Status</fullName>
        <field>Status__c</field>
        <formula>Previous_Status__c</formula>
        <name>Set Status to Prior Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Rejected</fullName>
        <field>Status__c</field>
        <formula>&apos;Rejected&apos;</formula>
        <name>Set Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Requested</fullName>
        <field>Status__c</field>
        <formula>&apos;Requested&apos;</formula>
        <name>Set Status to Requested</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Requested_NewDev</fullName>
        <description>Set Status to Requested for NewDev approval process</description>
        <field>Status__c</field>
        <formula>&apos;Requested&apos;</formula>
        <name>Set Status to Requested NewDev</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Invoice_Resend_Flag</fullName>
        <field>Resend_OBMsg__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck_Invoice_Resend_Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Invoice_Oppty_Owner_Email</fullName>
        <field>Opportunity_Owner_Email__c</field>
        <formula>Opportunity__r.Owner.Email</formula>
        <name>Update Invoice Oppty Owner Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Name</fullName>
        <field>Name</field>
        <formula>EBS_Number__c</formula>
        <name>Update_Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Unpaid_Balance</fullName>
        <field>Unpaid_Balance__c</field>
        <formula>Amount__c</formula>
        <name>Update Unpaid Balance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>DCB_CreditMemo_New</fullName>
        <actions>
            <name>Sent_to_AR_date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uncheck_Invoice_Resend_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PA_SF_CreateApplyCreditMemo_v1_0</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Workflow to send an outbound notification to EBS to synchronise data.</description>
        <formula>(RecordType.Name=&apos;Credit Memo&apos; &amp;&amp; Status__c=&apos;Requested&apos;) ||  Resend_OBMsg__c</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DCB_Invoice_Cancel</fullName>
        <actions>
            <name>Sent_to_AR_date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uncheck_Invoice_Resend_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PA_SF_CancelInvoice_v1_0</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Workflow to send an outbound notification to EBS to synchronise data.</description>
        <formula>(Record_Type_Name__c = &apos;Invoice&apos; &amp;&amp; Status__c=&apos;Cancellation Requested&apos;) ||  Resend_OBMsg__c</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DCB_Invoice_New</fullName>
        <actions>
            <name>Sent_to_AR_date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uncheck_Invoice_Resend_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PA_SF_CreateInvoice_v1_0</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2) OR 3</booleanFilter>
        <criteriaItems>
            <field>Invoice__c.Record_Type_Name__c</field>
            <operation>equals</operation>
            <value>Invoice</value>
        </criteriaItems>
        <criteriaItems>
            <field>Invoice__c.Status__c</field>
            <operation>equals</operation>
            <value>Requested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Invoice__c.Resend_OBMsg__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Workflow to send an outbound notification to EBS to synchronise data.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Invoice Payment Status Notification</fullName>
        <actions>
            <name>Notify_Owner_of_Invoice_Payment_Status_Change</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send notification every time payment status is changed.</description>
        <formula>ISCHANGED( Payment_Status__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Invoice Payment Status Notification - RD</fullName>
        <actions>
            <name>Notify_Owner_of_Invoice_Payment_Status_Change_RD</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send notification every time payment status is changed for RD related invoices.</description>
        <formula>AND(ISCHANGED(Payment_Status__c), Opportunity__r.RecordType.DeveloperName = &quot;Recoverable_Damage&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Invoice Status Notification</fullName>
        <actions>
            <name>Notify_Owner_of_Invoice_Status_Change</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send notification every time status is changed.</description>
        <formula>ISCHANGED( Status__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Invoice Status Notification - RD</fullName>
        <actions>
            <name>Notify_Owner_of_Invoice_Status_Change_RD</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send notification every time status is changed.</description>
        <formula>AND(ISCHANGED( Status__c ),Opportunity__r.RecordType.DeveloperName = &quot;Recoverable_Damage&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Invoice_EBS_Number</fullName>
        <actions>
            <name>Update_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update name to ebs number</description>
        <formula>AND(       NOT( ISBLANK( EBS_Number__c )  ),  	 ISCHANGED( EBS_Number__c )   	 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Payment Status Changed to Fully Paid</fullName>
        <actions>
            <name>Set_Fully_Paid_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Invoice__c.Payment_Status__c</field>
            <operation>equals</operation>
            <value>Fully Paid</value>
        </criteriaItems>
        <description>This rule fires when Invoice Payment Status is set to Fully Paid</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Regenerate invoice and send to SOA</fullName>
        <actions>
            <name>InvokeSalesForceInvoiceRePrint</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>regerate the invoice when the regenerate invoice button clicked</description>
        <formula>AND(ISCHANGED( Last_Invoice_Regeneration_Date__c ), OR(Status__c =&quot;Issued&quot;, Status__c = &quot;Cancelled&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Invoice Oppty Owner Email</fullName>
        <actions>
            <name>Update_Invoice_Oppty_Owner_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populate opportunity owner email field for use in approval process email alerts</description>
        <formula>ISNEW() || PRIORVALUE( Opportunity__c ) &lt;&gt; Opportunity__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
