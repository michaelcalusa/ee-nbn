<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Access_Seeker_Lead_Acknowledgement_Notification_End_User</fullName>
        <description>Access Seeker Lead Acknowledgement Notification End User</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Residential_Sales_Email_Templates/Access_Seeker_End_User_VisualForce_v2</template>
    </alerts>
    <alerts>
        <fullName>Access_Seeker_Lead_Information_Agreement_Thank_You_Notification</fullName>
        <description>Access Seeker Lead Information Agreement &apos;Thank You&apos; Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Residential_Sales_Email_Templates/Access_Seeker_Lead_Information_Agreement_Thank_You_Email_Template_End_User</template>
    </alerts>
    <alerts>
        <fullName>ICT_Welcome_Email</fullName>
        <description>ICT Welcome Email</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Business_Segment/Business_Segment_ICT_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>Assign_Initial_Status</fullName>
        <description>This action will set the Initial Status as Marketing Qualified</description>
        <field>Qualification__c</field>
        <literalValue>Marketing Qualified</literalValue>
        <name>Assign Initial Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Put_the_Lead_back_to_SMB_Que</fullName>
        <field>OwnerId</field>
        <lookupValue>SMB_Team</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Put the Lead back to SMB Que</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Lead_Reset_SLA_flag</fullName>
        <field>Reset_SLA__c</field>
        <literalValue>0</literalValue>
        <name>Reset Lead Reset SLA flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Partner_Accepted_Date</fullName>
        <field>Partner_Accepted_Date__c</field>
        <name>Reset Partner Accepted Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Partner_Declined_Date</fullName>
        <field>Partner_Declined_Date__c</field>
        <name>Reset Partner Declined Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_the_Decline_Reason</fullName>
        <description>Reset the Decline Reason value to null</description>
        <field>Decline_Reason__c</field>
        <name>Reset the Decline Reason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Lead_SLA_Expiry</fullName>
        <field>SLA_Expired__c</field>
        <literalValue>1</literalValue>
        <name>Set Lead SLA Expiry</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Reset_SLA_to_true</fullName>
        <field>Reset_SLA__c</field>
        <literalValue>1</literalValue>
        <name>Set Reset SLA to true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_SLA_Expiry_to_false</fullName>
        <description>Reset SLA Expiry as Lead is allocated to another Partner</description>
        <field>SLA_Expired__c</field>
        <literalValue>0</literalValue>
        <name>Set SLA Expiry to false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Allocated_Date</fullName>
        <field>Allocated_Date__c</field>
        <formula>NOW()</formula>
        <name>Update Allocated Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Disqualified_Date</fullName>
        <field>Disqualified_Date__c</field>
        <formula>NOW()</formula>
        <name>Update Disqualified Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type_to_Partner</fullName>
        <description>This will update the record Type to Partner</description>
        <field>RecordTypeId</field>
        <lookupValue>Business_Segment_ICT</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type to Partner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_for_ICT_Leads_from_WEB</fullName>
        <description>This rule Updates Status of Leads to Registered as those are ICT Leads</description>
        <field>Status</field>
        <literalValue>Registered</literalValue>
        <name>Update Status for ICT Leads from WEB</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Allocated</fullName>
        <field>Status</field>
        <literalValue>Allocated</literalValue>
        <name>Update Status to Allocated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Open</fullName>
        <field>Status</field>
        <literalValue>Open</literalValue>
        <name>Update Status to Open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Capture Lead Allocated Date</fullName>
        <actions>
            <name>Reset_Partner_Accepted_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reset_Partner_Declined_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reset_the_Decline_Reason</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Reset_SLA_to_true</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_SLA_Expiry_to_false</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Allocated_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Status_to_Allocated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Captures Lead&apos;s recent allocation date and update Lead status to Allocated when Lead Owner is changed from SMB or EnG to any other Queue</description>
        <formula>AND(RecordType.DeveloperName = &quot;Business_End_Customer&quot;, ISCHANGED( OwnerId ), AND(Lead_Owner_Queue__c &lt;&gt; &quot;SMB_Team&quot;, Lead_Owner_Queue__c &lt;&gt; &quot;E_G_Team&quot;, Lead_Owner_Queue__c &lt;&gt; &quot;Uncategorised&quot;, Lead_Owner_Queue__c &lt;&gt; &quot;Marketing_Unqualified&quot;, TEXT(Owner:User.UserType) &lt;&gt; &quot;Standard&quot;), ISBLANK(Partner_Accepted_Date__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Capture Lead Disqualified Date</fullName>
        <actions>
            <name>Update_Disqualified_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Business End Customer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Disqualified</value>
        </criteriaItems>
        <description>Captures the most recent Lead&apos;s Disqualified Date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Change Record type to Partner</fullName>
        <actions>
            <name>Update_Record_Type_to_Partner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Partner_Record__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This Workflow  Rule Changes Record type to Partner as its Created from ICT Channel Partner Webform</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead Status Update For ICT Partners</fullName>
        <actions>
            <name>ICT_Welcome_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Status_for_ICT_Leads_from_WEB</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Lead_registration_email_is_sent</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Partner_Record__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This rule is used to Change the Status to Registered for ICT Partner Records</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Unaccepted Allocated Lead</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Business End Customer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Reset_SLA__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This workflow is to put back the End Customer Lead to SMB queue for reallocation if not accepted up by the Partner within 48 hours</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Put_the_Lead_back_to_SMB_Que</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Reset_Lead_Reset_SLA_flag</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Set_Lead_SLA_Expiry</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Status_to_Open</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>48</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>WR_Set_Lead_Initial_Status</fullName>
        <actions>
            <name>Assign_Initial_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule will set the Lead Status to Marketing Qualified in Initial Status field whenever Lead Status becomes Marketing Qualified.</description>
        <formula>AND( RecordType.DeveloperName = &quot;Business_End_Customer&quot; , OR( AND( ISNEW() , ISPICKVAL( Status , &quot;Marketing Qualified&quot;) ) , AND( NOT(ISNEW()),  ISCHANGED(Status), ISPICKVAL( Status , &quot;Marketing Qualified&quot;) ) ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Lead_registration_email_is_sent</fullName>
        <assignedTo>unassigned@nbnco.com.au</assignedTo>
        <assignedToType>user</assignedToType>
        <description>This is a activity log for the email sent when user registers from the nbn website</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Low</priority>
        <protected>false</protected>
        <status>Closed</status>
        <subject>Lead registration confirmation email is sent</subject>
    </tasks>
</Workflow>
