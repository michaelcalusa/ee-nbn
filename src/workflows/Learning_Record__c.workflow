<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Alert_to_send_email_to_contact_once_fundamental_training_is_certified</fullName>
        <description>Email Alert to send email to contact once fundamental training is certified</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>ictchannel@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Business_Segment/ICT_Fundamental_Training_Certification_acknowledgement</template>
    </alerts>
    <rules>
        <fullName>Rule to send email when fundamental training Learning record status is changes to certified</fullName>
        <actions>
            <name>Email_Alert_to_send_email_to_contact_once_fundamental_training_is_certified</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_acknowledgement_confirmation_for_Fundamentals_training</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>US#8330: This workflow rule will send an email to Contact once it&apos;s learning record gets certified.</description>
        <formula>ISPICKVAL(Course_Status__c, &apos;Certified&apos;) &amp;&amp;  Course__r.Name = &apos;ICT Channel Program Fundamentals training&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Email_acknowledgement_confirmation_for_Fundamentals_training</fullName>
        <assignedToType>owner</assignedToType>
        <description>Email acknowledgement has been sent to the Contact for completion of the nbn™ ICT Channel Program Fundamentals training.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Email acknowledgement confirmation for Fundamentals training</subject>
    </tasks>
</Workflow>
