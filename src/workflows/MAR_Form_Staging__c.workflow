<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>MAR_Single_Form_Alternative_Emails</fullName>
        <description>MAR Single Form Alternative Emails</description>
        <protected>false</protected>
        <recipients>
            <field>Alt_Email_Address__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Online_Notification_Template_Folder/MAR_Single_Form_Confirmation_Alternative_Emails</template>
    </alerts>
    <alerts>
        <fullName>MAR_Single_Form_Email</fullName>
        <description>MAR Single Form Email</description>
        <protected>false</protected>
        <recipients>
            <field>Email_Address__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Online_Notification_Template_Folder/MAR_Single_Form_Confirmation_Emails</template>
    </alerts>
    <alerts>
        <fullName>MAR_unMonitor_Single_Form_Emai</fullName>
        <description>MAR unMonitor Single Form Emai</description>
        <protected>false</protected>
        <recipients>
            <field>Email_Address__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Online_Notification_Template_Folder/MAR_Unmonitor_Single_Form_Confirmation_Emails</template>
    </alerts>
    <alerts>
        <fullName>MAR_unmonitor_Single_Form_Alternative_Emails</fullName>
        <description>MAR unmonitor Single Form Alternative Emails</description>
        <protected>false</protected>
        <recipients>
            <field>Alt_Email_Address__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Online_Notification_Template_Folder/MAR_Unmonitor_Single_Form_Confirmation_Alternative_Emails</template>
    </alerts>
    <rules>
        <fullName>MAR Single Form Confirmation Emails</fullName>
        <actions>
            <name>MAR_Single_Form_Alternative_Emails</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MAR_Single_Form_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND (       Or (  NOT(ISBLANK(Alt_Email_Address__c )),            NOT(ISBLANK(Email_Address__c ))            ),      or (           Alarm_Type_F__c &lt;&gt; &quot;Unmonitored&quot;,           DATEVALUE(CreatedDate) &lt; DATEVALUE($Label.MARUmapEmailStart)       )  )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>MAR Unmonitor Single Form Confirmation Emails</fullName>
        <actions>
            <name>MAR_unMonitor_Single_Form_Emai</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MAR_unmonitor_Single_Form_Alternative_Emails</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND (  Or ( NOT(ISBLANK(Alt_Email_Address__c )),      NOT(ISBLANK(Email_Address__c ))  ),  AND(  Alarm_Type_F__c = &quot;Unmonitored&quot;,  DATEVALUE(CreatedDate) &gt;= DATEVALUE($Label.MARUmapEmailStart)  )   )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
