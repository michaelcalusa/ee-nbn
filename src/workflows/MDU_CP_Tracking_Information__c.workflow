<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Site_Id_with_AllowOneTrack</fullName>
        <field>AllowOneTrackinfo__c</field>
        <formula>CASESAFEID(Site__r.Id)</formula>
        <name>Update Site Id with AllowOneTrack</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update AllowOneTrackinfo</fullName>
        <actions>
            <name>Update_Site_Id_with_AllowOneTrack</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Site__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Update AllowOneTrackinfo with parent record information.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
