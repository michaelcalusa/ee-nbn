<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Alert_on_OB_step1_4_Complete</fullName>
        <ccEmails>opsworkshop@nbnco.com.au</ccEmails>
        <description>Email Alert on OB step1.4 Complete</description>
        <protected>false</protected>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Onboarding_Step_1_4_Complete_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_on_OB_step1_6_Complete</fullName>
        <ccEmails>ContractManager@nbnco.com.au; Sales@nbnco.com.au</ccEmails>
        <description>Email Alert on OB step1.6 Complete</description>
        <protected>false</protected>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Onboarding_Step_1_6_Complete_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_on_OB_step1_7_Complete</fullName>
        <ccEmails>Sales@nbnco.com.au; ContractManager@nbnco.com.au; opsworkshop@nbnco.com.au</ccEmails>
        <description>Email Alert on OB step1.7 Complete</description>
        <protected>false</protected>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Onboarding_Step_1_7_Complete_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_on_OB_step2_10_Complete</fullName>
        <ccEmails>OpsWorkshop@nbnco.com.au; Solutions@nbnco.com.au</ccEmails>
        <description>Email Alert on OB step2.10 Complete</description>
        <protected>false</protected>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Onboarding_Step_2_10_Complete_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_on_OB_step2_13_Complete</fullName>
        <ccEmails>OpsWorkshop@nbnco.com.au; Sales@nbnco.com.au; Solutions@nbnco.com.au</ccEmails>
        <description>Email Alert on OB step2.13 Complete</description>
        <protected>false</protected>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Onboarding_Step_2_13_Complete_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_on_OB_step2_1_Complete</fullName>
        <ccEmails>OpsWorkshop@nbnco.com.au; AccountDevelopment@nbnco.com.au; Solutions@nbnco.com.au</ccEmails>
        <description>Email Alert on OB step2.1 Complete</description>
        <protected>false</protected>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Onboarding_Step_2_1_Complete_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_on_OB_step2_6_Complete</fullName>
        <ccEmails>Solutions@nbnco.com.au; AccountDevelpment@nbnco.com.au; OpsWorkshop@nbnco.com.au</ccEmails>
        <description>Email Alert on OB step2.6 Complete</description>
        <protected>false</protected>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Onboarding_Step_2_6_Complete_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_on_OB_step3_11_Complete</fullName>
        <ccEmails>AccountDevelopment@nbnco.com.au; OpsWorkshop@nbnco.com.au</ccEmails>
        <description>Email Alert on OB step3.11 Complete</description>
        <protected>false</protected>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Onboarding_Step_3_11_Complete_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_on_OB_step4_5_Complete</fullName>
        <ccEmails>Sales@nbnco.com.au; OpsWorkshop@nbnco.com.au</ccEmails>
        <description>Email Alert on OB step4.5 Complete</description>
        <protected>false</protected>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Onboarding_Step_4_5_Complete_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_on_OB_step5_1_Complete</fullName>
        <ccEmails>RSPListings@nbnco.com.au; Sales@nbnco.com.au; OpsWorkshop@nbnco.com.au</ccEmails>
        <description>Email Alert on OB step5.1 Complete</description>
        <protected>false</protected>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Onboarding_Step_5_1_Complete_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_on_OB_step5_3_Complete</fullName>
        <ccEmails>Sales@nbnco.com.au</ccEmails>
        <description>Email Alert on OB step5.3 Complete</description>
        <protected>false</protected>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Onboarding_Step_5_3_Complete_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Completion_Date</fullName>
        <field>Process_Completion_Date__c</field>
        <formula>IF(NOT(ISNULL(Date_Ended__c)),DATEVALUE(Date_Ended__c),TODAY())</formula>
        <name>Update Completion Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Related_Product__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Update Process Completion Date%09 on Process</fullName>
        <actions>
            <name>Update_Completion_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(ISNEW()) &amp;&amp; (TEXT(Status__c ) = &apos;Completed&apos; || TEXT(Status__c ) = &apos;Not Required&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
