<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Opportunity_Owner_when_invoicing_status_changes</fullName>
        <description>Email Opportunity Owner when invoicing status changes</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Recoverable_Works_Email_Templates/Invoicing_Status_Change_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_to_Opportunity_Owner_When_Payment_Status_is_Partially_Paid_or_Paid_In_Fu</fullName>
        <description>Email to Opportunity Owner When Payment Status is Partially Paid or Paid In Full</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Recoverable_Works_Email_Templates/Payment_Status_Change_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_to_Opportunity_Owner_When_Stage_changed_to_Waiting_For_Payment</fullName>
        <description>Email to Opportunity Owner When Stage changed to Waiting For Payment</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Recoverable_Works_Email_Templates/Opportunity_Stage_Changed</template>
    </alerts>
    <alerts>
        <fullName>Notify_Opportunity_Owner_when_approved_internally</fullName>
        <description>Notify Opportunity Owner when approved internally</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Recoverable_Works_Email_Templates/Opportunity_Approved_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Notify_Opportunity_Owner_when_rejected_internally</fullName>
        <description>Notify Opportunity Owner when rejected internally</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Recoverable_Works_Email_Templates/Opportunity_Rejected_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_CW_Opp_Owner_on_Cost_Phase_Update</fullName>
        <description>Send Email to CW Opp Owner on Cost Phase Update</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>commercialworks@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Recoverable_Works_Email_Templates/Cost_Phase_Change_Notification_Email2</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_Analysing_Status_Requesting</fullName>
        <description>Set Analysing Status as Requesting</description>
        <field>Analysing_Status__c</field>
        <literalValue>Requesting</literalValue>
        <name>Set Analysing Status - Requesting</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approved_By_With_Approver</fullName>
        <field>Approved_By__c</field>
        <formula>Approved_By__c + &apos;, &apos; + LastModifiedBy.FirstName + &apos; &apos; +  LastModifiedBy.LastName</formula>
        <name>Set Approved By - With Approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_ApprovingStatus_InternalApprovalRece</fullName>
        <description>Set Approving Status - Internal Approval Received</description>
        <field>Approving_Status__c</field>
        <literalValue>Internal Approval Received</literalValue>
        <name>Set ApprovingStatus-InternalApprovalRece</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approving_Status_Internal_Approval</fullName>
        <description>Set Approving Status - Internal Approval Requested</description>
        <field>Approving_Status__c</field>
        <literalValue>Internal Approval Requested</literalValue>
        <name>Set Approving Status - Internal Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approving_Status_Revision_Required</fullName>
        <description>Set Approving Status - Revision Required</description>
        <field>Approving_Status__c</field>
        <literalValue>Revision Required</literalValue>
        <name>Set Approving Status - Revision Required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Assessing_Status_Gathering_Evidenc</fullName>
        <description>Set Assessing Status as Gathering Evidence</description>
        <field>Assesing_Status__c</field>
        <literalValue>Gathering Evidence</literalValue>
        <name>Set Assessing Status - Gathering Evidenc</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Completing_Status_Work_Requested</fullName>
        <description>Set Completing Status as Work Requested</description>
        <field>Completing_Status__c</field>
        <literalValue>Work Requested</literalValue>
        <name>Set Completing Status - Work Requested</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Costing_Status_Preparing_Quote_Req</fullName>
        <description>Set Costing Status - Preparing Quote Request</description>
        <field>Costing_Status__c</field>
        <literalValue>Preparing Quote Request</literalValue>
        <name>Set Costing Status - Preparing Quote Req</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Costing_Status_Waiting_For_Costs</fullName>
        <description>Set Costing Status as Waiting For Costs</description>
        <field>Costing_Status__c</field>
        <literalValue>Waiting For Costs</literalValue>
        <name>Set Costing Status - Waiting For Costs</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Estimating_Status_Requesting</fullName>
        <description>Set Estimating Status as Requesting</description>
        <field>Estimating_Status__c</field>
        <literalValue>Requesting</literalValue>
        <name>Set Estimating Status - Requesting</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Invoicing_Status_Pending</fullName>
        <description>Set Invoicing Status as Pending</description>
        <field>Invoicing_Status__c</field>
        <literalValue>Pending</literalValue>
        <name>Set Invoicing Status - Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Last_Approved_Date_Current</fullName>
        <description>Set Last Approved Date to Current Date</description>
        <field>Last_Approved_Date__c</field>
        <formula>Today()</formula>
        <name>Set Last Approved Date - Current</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Owner_RW_New_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>relocationworks@nbnco.com.au</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set Owner - RW New Queue</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Payment_Status_None</fullName>
        <description>Set Payment Status - None</description>
        <field>Payment_Status__c</field>
        <name>Set Payment Status - None</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Payment_Status_Waiting</fullName>
        <description>Set Payment Status as Waiting</description>
        <field>Payment_Status__c</field>
        <literalValue>Waiting</literalValue>
        <name>Set Payment Status - Waiting</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Qualifying_Status_Commencing</fullName>
        <description>Set Qualifying Status as Commencing</description>
        <field>Qualifying_Status__c</field>
        <literalValue>Commencing</literalValue>
        <name>Set Qualifying Status - Commencing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Quote_Requested_Date_Current</fullName>
        <description>Set Quote Requested Date - Current Date</description>
        <field>Quote_Requested_Date__c</field>
        <formula>Today()</formula>
        <name>Set Quote Requested Date - Current</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Quoting_Status_Sent</fullName>
        <description>Set Quoting Status as Sent</description>
        <field>Quoting_Status__c</field>
        <literalValue>Sent</literalValue>
        <name>Set Quoting Status - Sent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_RecordType_Damage</fullName>
        <description>Set RecordType - Damage</description>
        <field>RecordTypeId</field>
        <lookupValue>Recoverable_Works_Damage</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set RecordType - Damage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_RecordType_Major_Complex</fullName>
        <description>Set RecordType - Major/Complex</description>
        <field>RecordTypeId</field>
        <lookupValue>Recoverable_Works_Major_Complex</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set RecordType - Major/Complex</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_RecordType_Small</fullName>
        <description>Set RecordType - Small</description>
        <field>RecordTypeId</field>
        <lookupValue>Recoverable_Works_Small</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set RecordType - Small</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_Simple</fullName>
        <description>Set Record Type - Simple</description>
        <field>RecordTypeId</field>
        <lookupValue>Recoverable_Works_Simple</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type - Simple</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Recordtype_Variation</fullName>
        <description>Set Recordtype - Variation</description>
        <field>RecordTypeId</field>
        <lookupValue>Recoverable_Works_Variation</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Recordtype - Variation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Reference_Number</fullName>
        <description>AUto Number is copied for access by ClickApprove</description>
        <field>Reference_Number_Copy__c</field>
        <formula>Reference_Number__c</formula>
        <name>Set Reference Number Copy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_StageName_Wait_For_Payment</fullName>
        <field>StageName</field>
        <literalValue>Waiting For Payment</literalValue>
        <name>Set StageName - Wait For Payment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_Notifying</fullName>
        <description>Set Stage - Notifying</description>
        <field>StageName</field>
        <literalValue>Notifying</literalValue>
        <name>Set Stage - Notifying</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_to_Approving</fullName>
        <description>Opportunity stage is set to Approving</description>
        <field>StageName</field>
        <literalValue>Approving</literalValue>
        <name>Set Stage to Approving</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_to_Internally_Approved</fullName>
        <field>StageName</field>
        <literalValue>Internally Approved</literalValue>
        <name>Set Stage to Internally Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_to_Internally_Rejected</fullName>
        <field>StageName</field>
        <literalValue>Internally Rejected</literalValue>
        <name>Set Stage to Internally Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Close_Date_for_BSM_RSP_Opportunit</fullName>
        <description>current date when Opportunity stage is changed to Closed (Won or Lost)</description>
        <field>CloseDate</field>
        <formula>TODAY()</formula>
        <name>Update Close Date for BSM RSP Opportunit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_actual_cost</fullName>
        <field>Actual_Cost__c</field>
        <formula>Temp_Fix_Cost_Total__c  +  Perm_Fix_Cost_Total__c  +  Other_Misc_Cost_Total__c</formula>
        <name>Update actual cost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Costing Status Changed to Quote Requested</fullName>
        <actions>
            <name>Set_Quote_Requested_Date_Current</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Costing_Status__c</field>
            <operation>equals</operation>
            <value>Quote Requested</value>
        </criteriaItems>
        <description>Costing Status Changed to Quote Requested then populate the Quote Requested Date with Todays date.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Create Cost Estimate Task</fullName>
        <actions>
            <name>Cost_Estimate</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Estimating</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Recoverable Works Simple,Recoverable Works Small,Recoverable Works Damage,Recoverable Works Major/Complex,Recoverable Works</value>
        </criteriaItems>
        <description>When Opp stage is Estimating then create a task for Cost Estimate and assign to Opp owner</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Create Impact Assignment Task</fullName>
        <actions>
            <name>Impact_Assignment</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>When Opp stage is Impact Assessment then create a task for Impact assignment and assign to Opp woner</description>
        <formula>TRUE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Invoice Status Changed To Issued</fullName>
        <actions>
            <name>Set_StageName_Wait_For_Payment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Invoicing_Status__c</field>
            <operation>equals</operation>
            <value>Issued</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Recoverable Works Archived,Damage Archived</value>
        </criteriaItems>
        <description>When Invoice Status is Changed to Issued this rule fires</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Invoicing Status Changed To Cancelled</fullName>
        <actions>
            <name>Set_Payment_Status_None</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Invoicing_Status__c</field>
            <operation>equals</operation>
            <value>Cancelled</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Recoverable Works Archived,Damage Archived</value>
        </criteriaItems>
        <description>When Invoicing Status changed to Cancelled set payment Status to None</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Invoicing Status Changed To Cancelled%2FIssued</fullName>
        <actions>
            <name>Email_Opportunity_Owner_when_invoicing_status_changes</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Invoicing_Status__c</field>
            <operation>equals</operation>
            <value>Issued,Cancelled</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Recoverable Works Archived,Damage Archived</value>
        </criteriaItems>
        <description>Send email to opportunity owner when Invoicing Status is changed to Cancelled/Issued</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Payment Status Changed to Paid In Full</fullName>
        <actions>
            <name>Email_to_Opportunity_Owner_When_Payment_Status_is_Partially_Paid_or_Paid_In_Fu</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Payment_Status__c</field>
            <operation>equals</operation>
            <value>Paid In Full</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Recoverable Works Archived,Damage Archived</value>
        </criteriaItems>
        <description>Payment Status Changed to Paid In Full</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Payment Status Changed to Partially Paid</fullName>
        <actions>
            <name>Email_to_Opportunity_Owner_When_Payment_Status_is_Partially_Paid_or_Paid_In_Fu</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Payment_Status__c</field>
            <operation>equals</operation>
            <value>Partially Paid</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Recoverable Works Archived,Damage Archived</value>
        </criteriaItems>
        <description>Payment Status Changed to Partially Paid</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Payment Status Changed to Partially Paid or Paid In Full</fullName>
        <actions>
            <name>Email_to_Opportunity_Owner_When_Payment_Status_is_Partially_Paid_or_Paid_In_Fu</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Payment_Status__c</field>
            <operation>equals</operation>
            <value>Partially Paid,Paid In Full</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Recoverable Works Simple,Recoverable Works Small,Recoverable Works Damage,Recoverable Works Major/Complex,Recoverable Works Variation</value>
        </criteriaItems>
        <description>Payment Status Changed to Partially Paid or Paid In Full</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RW Type Changed to Damage</fullName>
        <actions>
            <name>Set_RecordType_Damage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When RW Type Set to Damange then Set the Record Type as Recoverable Works Damage</description>
        <formula>IF( (ISPICKVAL(PRIORVALUE( RW_Type__c ), &apos;&apos;) ||  ISNEW() ) &amp;&amp; ISPICKVAL(RW_Type__c,&apos;Damage&apos;) ,  TRUE , FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>RW Type Changed to MajororComplex</fullName>
        <actions>
            <name>Set_RecordType_Major_Complex</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When RW Type Set to Major Infrastructure,ND Rearrangement,Aerial Relocation,Aerial to Underground,CP Rearrangement,FDH Relocation,MDF Relocation,MDU Rearrangement,Pillar Reloc and TFAN Relocation then Set the Record Type as Recoverable Works Major/Complex</description>
        <formula>IF( (ISPICKVAL(PRIORVALUE( RW_Type__c ), &apos;&apos;) ||  ISNEW() ) &amp;&amp; (ISPICKVAL(RW_Type__c,&apos;Major Infrastructure Relocation&apos;) || ISPICKVAL(RW_Type__c,&apos;ND Rearrangement&apos;) || ISPICKVAL(RW_Type__c,&apos;Aerial Relocation&apos;) || ISPICKVAL(RW_Type__c,&apos;Aerial to Underground&apos;) || ISPICKVAL(RW_Type__c,&apos;CP Rearrangement&apos;) || ISPICKVAL(RW_Type__c,&apos;FDH Relocation&apos;) || ISPICKVAL(RW_Type__c,&apos;MDF Relocation&apos;) || ISPICKVAL(RW_Type__c,&apos;MDU Rearrangement&apos;) || ISPICKVAL(RW_Type__c,&apos;Pillar Relocation&apos;) || ISPICKVAL(RW_Type__c,&apos;TFAN Relocation&apos;) ||  ISPICKVAL(RW_Type__c,&apos;Node Relocation&apos;) ||  ISPICKVAL(RW_Type__c,&apos;Additional Connection&apos;) ||  ISPICKVAL(RW_Type__c,&apos;Network Extension&apos;)) ,  TRUE , FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>RW Type Changed to Simple</fullName>
        <actions>
            <name>Set_Record_Type_Simple</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When RW Type Set to PCD Removal, NTD Replacement, PCD Replacement then Set the Record Type as Recoverable Works Simple</description>
        <formula>IF( (ISPICKVAL(PRIORVALUE( RW_Type__c ), &apos;&apos;) ||  ISNEW() ) &amp;&amp; (ISPICKVAL(RW_Type__c,&apos;PCD Removal&apos;) || ISPICKVAL(RW_Type__c,&apos;NTD Replacement&apos;) || ISPICKVAL(RW_Type__c,&apos;PCD Replacement&apos;)) ,  TRUE , FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>RW Type Changed to Small</fullName>
        <actions>
            <name>Set_RecordType_Small</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When RW Type Set to Defect, Leadn-In Relocation,PCD Relocation, NTD Relocation NS Installation, Lead-In Replacement and Pit or Duct Alteration then Set the Record Type as Recoverable Works Small</description>
        <formula>IF( (ISPICKVAL(PRIORVALUE( RW_Type__c ), &apos;&apos;) ||  ISNEW() ) &amp;&amp; (ISPICKVAL(RW_Type__c,&apos;Defect&apos;) || ISPICKVAL(RW_Type__c,&apos;Lead-In Relocation&apos;) || ISPICKVAL(RW_Type__c,&apos;Lead-In Replacement&apos;) || ISPICKVAL(RW_Type__c,&apos;NS Installation&apos;) || ISPICKVAL(RW_Type__c,&apos;PCD Relocation&apos;) || ISPICKVAL(RW_Type__c,&apos;NTD Relocation&apos;) || ISPICKVAL(RW_Type__c,&apos;Pit or Duct Alteration&apos;)) ,  TRUE , FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>RW Type Changed to Variation</fullName>
        <actions>
            <name>Set_Recordtype_Variation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When RW Type Set to Variation then Set the Record Type as Recoverable Works Variation</description>
        <formula>IF( (ISPICKVAL(PRIORVALUE( RW_Type__c ), &apos;&apos;) ||  ISNEW() ) &amp;&amp; ISPICKVAL(RW_Type__c,&apos;Variation&apos;) ,  TRUE , FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Source Equals Telstra</fullName>
        <actions>
            <name>Set_Owner_RW_New_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When a Recoverable Works Opportunity is Created with Source as Telstra, the opportunity will be assigned to user &apos;RW New Queue&apos;</description>
        <formula>(RecordType.Name  = &apos;Recoverable Works&apos; || RecordType.Name  = &apos;Recoverable Works Simple&apos; || RecordType.Name  = &apos;Recoverable Works Small&apos; || RecordType.Name  = &apos;Recoverable Works Damage&apos; || RecordType.Name  = &apos;Recoverable Works Major/Complex&apos;) &amp;&amp;  ISPICKVAL( Source__c , &apos;Telstra&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>StageName Changed To Analysing</fullName>
        <actions>
            <name>Set_Analysing_Status_Requesting</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Analysing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Recoverable Works Major/Complex</value>
        </criteriaItems>
        <description>When Opportunity Stage is Analysing  then set the Analysing Status as Requesting</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>StageName Changed To Completing</fullName>
        <actions>
            <name>Set_Completing_Status_Work_Requested</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Completing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Recoverable Works Archived,Damage Archived</value>
        </criteriaItems>
        <description>When Opportunity Stage is Completing then set the Completing Status as Work Requested</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>StageName Changed To Costing</fullName>
        <actions>
            <name>Set_Costing_Status_Preparing_Quote_Req</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Costing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Recoverable Works Simple,Recoverable Works Small,Recoverable Works Major/Complex,Recoverable Works,Recoverable Works Variation</value>
        </criteriaItems>
        <description>When Opportunity Stage is Costing then set the Costing Status as Work Preparing Quote Request</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>StageName Changed To Costing for Damage</fullName>
        <actions>
            <name>Set_Costing_Status_Waiting_For_Costs</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Costing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Recoverable Works Damage</value>
        </criteriaItems>
        <description>When Opportunity Stage is Costing and Record type is Damage then set the Costing Status as Work Waiting For Costs</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>StageName Changed To Estimating</fullName>
        <actions>
            <name>Set_Estimating_Status_Requesting</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Estimating</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Recoverable Works Major/Complex</value>
        </criteriaItems>
        <description>When Opportunity Stage is Estimating  then set the Estimating Status as Requesting</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>StageName Changed To Qualifying</fullName>
        <actions>
            <name>Set_Qualifying_Status_Commencing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Qualifying</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Recoverable Works Simple,Recoverable Works Small,Recoverable Works Damage,Recoverable Works Major/Complex,Recoverable Works,Recoverable Works Variation</value>
        </criteriaItems>
        <description>When Opportunity Stage is Qualifying then set the Qualify Status as Commencing</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>StageName Changed To Quoting</fullName>
        <actions>
            <name>Set_Quoting_Status_Sent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Quoting</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Recoverable Works Simple,Recoverable Works Small,Recoverable Works Major/Complex,Recoverable Works,Recoverable Works Variation,Recoverable Works Archived,Damage Archived</value>
        </criteriaItems>
        <description>When Opportunity Stage is Quoting and Record type is not Damage then set the Quoting Status as Sent</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>StageName Changed to Assessing</fullName>
        <actions>
            <name>Set_Assessing_Status_Gathering_Evidenc</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Assessing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Recoverable Works Simple,Recoverable Works Small,Recoverable Works Damage,Recoverable Works Major/Complex,Recoverable Works,Recoverable Works Variation</value>
        </criteriaItems>
        <description>When Opportunity Stage is Assessing then set the Assessing Status as Gathering Evidence</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>StageName Changed to Invoicing</fullName>
        <actions>
            <name>Set_Invoicing_Status_Pending</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Invoicing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Recoverable Works,Recoverable Works Damage,Recoverable Works Major/Complex,Recoverable Works Simple,Recoverable Works Small,Recoverable Works Variation</value>
        </criteriaItems>
        <description>When Opportunity Stage is Invoicing then set the Invoicing Status as Pending</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>StageName Changed to Waiting For Payment</fullName>
        <actions>
            <name>Email_to_Opportunity_Owner_When_Stage_changed_to_Waiting_For_Payment</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Set_Payment_Status_Waiting</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Waiting For Payment</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Recoverable Works Archived,Damage Archived</value>
        </criteriaItems>
        <description>When Opportunity Stage is Waiting For Payment then set the Payment Status as Waiting</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sync Opportunity Reference</fullName>
        <actions>
            <name>Set_Reference_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>When the AutoNumber field Opportunity Reference is created sync it with a copy (so that Click Approve can see it as Click Approve can&apos;t map to AutoNumbers)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Actualcost for Damage Restoration</fullName>
        <actions>
            <name>Update_actual_cost</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Recoverable Damage</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF BSMRSP Closed Date Update</fullName>
        <actions>
            <name>Update_Close_Date_for_BSM_RSP_Opportunit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>BSM RSP Opportunity</value>
        </criteriaItems>
        <description>To be populated with current date when Opportunity stage is changed to Closed (Won or Lost)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Cost_Estimate</fullName>
        <assignedToType>owner</assignedToType>
        <description>Please perform Cost Estimate</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Cost Estimate</subject>
    </tasks>
    <tasks>
        <fullName>Impact_Assignment</fullName>
        <assignedToType>owner</assignedToType>
        <description>Please perform Impact assignment.</description>
        <dueDateOffset>10</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Impact Assignment</subject>
    </tasks>
</Workflow>
