<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Opportunity_Team_Notification</fullName>
        <description>BSM RSP Opportunity Team Notification</description>
        <protected>false</protected>
        <recipients>
            <field>UserId</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Business_Segment/Business_Segment_RSP_Opportunity_Team_Notification</template>
    </alerts>
    <rules>
        <fullName>Notify Opportunity Team Member for BSM RSP Opportunity</fullName>
        <actions>
            <name>Opportunity_Team_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityTeamMember.UserId</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>BSM RSP</value>
        </criteriaItems>
        <description>need to receive an email notification with the details of Opportunity on which I am added an Opportunity team member for BSM RSP Opportunity</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
