<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Alert_to_Contract_Manager_on_step_CQ04_Complete</fullName>
        <ccEmails>ContractManager@nbnco.com.au; AccountDevelopment@nbnco.com.au; InfoAgreement@nbnco.com.au</ccEmails>
        <description>Alert to &apos;Contract Manager&apos; when a Customer qualification step &apos;CQ04. Downstream Information Agreement requested&apos; Completed</description>
        <protected>false</protected>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Customer_Qualification_Step_CQ04_Complete_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_Contract_Manager_on_step_CQ07_Complete</fullName>
        <ccEmails>ContractManager@nbnco.com.au; AccountDevelopment@nbnco.com.au</ccEmails>
        <description>Alert to &apos;Contract Manager&apos; when a Customer qualification step &apos;CQ07. Two-way NDA requested&apos; Completed</description>
        <protected>false</protected>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Customer_Qualification_Step_CQ07_Complete_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_Contract_Manager_on_step_CQ11_Complete</fullName>
        <ccEmails>ContractManager@nbnco.com.au; AccountDevelopment@nbnco.com.au</ccEmails>
        <description>Alert to &apos;Contract Manager&apos; when a Customer qualification step &apos;CQ11. Prospective Customer Agreement requested&apos; Completed</description>
        <protected>false</protected>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Customer_Qualification_Step_CQ11_Complete_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_Contract_Manager_on_step_CQ17_Complete</fullName>
        <ccEmails>ContractManager@nbnco.com.au; AccountDevelopment@nbnco.com.au</ccEmails>
        <description>Alert to &apos;Contract Manager&apos; when a Customer qualification step &apos;CQ17. WBA contract requested from Wholesale Supply&apos; Completed</description>
        <protected>false</protected>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Customer_Qualification_Step_CQ17_Complete_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_Credit_Officer_on_step_CQ14_Complete</fullName>
        <ccEmails>CreditOfficer@nbnco.com.au; AccountDevelopment@nbnco.com.au</ccEmails>
        <description>Alert to &apos;Credit Officer&apos; when a Customer qualification step &apos;CQ14. Valid Commitment Plan and Credit Application received&apos; Completed</description>
        <protected>false</protected>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Customer_Qualification_Step_CQ14_Complete_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_InfoAgreement_on_step_CQ03_Complete</fullName>
        <ccEmails>InfoAgreement@nbnco.com.au</ccEmails>
        <description>Alert to &apos;InfoAgreement&apos; when a Customer qualification step &apos;CQ03. Request received for Downstream Information Agreement&apos; Completed</description>
        <protected>false</protected>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Customer_Qualification_Step_CQ03_Complete_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_SEO_Customer_Solutions_on_step_CQ01_Complete</fullName>
        <ccEmails>AccountDevelopment@nbnco.com.au</ccEmails>
        <description>Alert to &apos;SEO Customer Solutions&apos; when a Customer qualification step &apos;CQ01. Enquiry received&apos; Completed</description>
        <protected>false</protected>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Customer_Qualification_Step_CQ01_Complete_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_SEO_Customer_Solutions_on_step_CQ05_Complete</fullName>
        <ccEmails>InfoAgreement@nbnco.com.au; AccountDevelopment@nbnco.com.au; ContractManager@nbnco.com.au</ccEmails>
        <description>Alert to &apos;SEO Customer Solutions&apos; when a Customer qualification step &apos;CQ05. Downstream Information Agreement executed&apos; Completed</description>
        <protected>false</protected>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Customer_Qualification_Step_CQ05_Complete_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_SEO_Customer_Solutions_on_step_CQ08_Complete</fullName>
        <ccEmails>AccountDevelopment@nbnco.com.au; ContractManager@nbnco.com.au</ccEmails>
        <description>Alert to &apos;SEO Customer Solutions&apos; when a Customer qualification step &apos;CQ08. Two-way NDA executed&apos; Completed</description>
        <protected>false</protected>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Customer_Qualification_Step_CQ08_Complete_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_SEO_Customer_Solutions_on_step_CQ12_Complete</fullName>
        <ccEmails>AccountDevelopment@nbnco.com.au; ContractManager@nbnco.com.au</ccEmails>
        <description>Alert to &apos;SEO Customer Solutions&apos; when a Customer qualification step &apos;CQ12. Prospective Customer Agreement executed&apos; Completed</description>
        <protected>false</protected>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Customer_Qualification_Step_CQ12_Complete_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_SEO_Customer_Solutions_on_step_CQ15_Complete</fullName>
        <ccEmails>AccountDevelopment@nbnco.com.au; CreditOfficer@nbnco.com.au</ccEmails>
        <description>Alert to &apos;SEO Customer Solutions&apos; when a Customer qualification step &apos;CQ15. Credit Application Accepted/Rejected&apos; Completed</description>
        <protected>false</protected>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Customer_Qualification_Step_CQ15_Complete_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_SEO_Customer_Solutions_on_step_CQ20_Complete</fullName>
        <ccEmails>AccountDevelopment@nbnco.com.au; ContractManager@nbnco.com.au</ccEmails>
        <description>Alert to &apos;SEO Customer Solutions&apos; when a Customer qualification step &apos;CQ20. WBA contract executed by nbn&apos; Completed</description>
        <protected>false</protected>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Customer_Qualification_Step_CQ20_Complete_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_opsworkshop_on_step_CQ21_Complete</fullName>
        <ccEmails>OpsWorkshop@nbnco.com.au</ccEmails>
        <description>Alert to &apos;opsworkshop&apos; when a Customer qualification step &apos;CQ21. EUAP Master Account created for nbn Customer Centre Access&apos; Completed</description>
        <protected>false</protected>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Customer_Qualification_Step_CQ21_Complete_Template</template>
    </alerts>
</Workflow>
