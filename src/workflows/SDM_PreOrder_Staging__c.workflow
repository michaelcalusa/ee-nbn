<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Processed</fullName>
        <field>Processed__c</field>
        <literalValue>0</literalValue>
        <name>Update Processed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update to be processed records</fullName>
        <actions>
            <name>Update_Processed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Checks if a staging record needs to move to opportunity</description>
        <formula>OR(ISCHANGED( SDM_Account__c ), ISCHANGED(  SDM_Business_Trading_Name__c ),  ISCHANGED(  SDM_RSP_CutOver_Status__c ),  ISCHANGED( SDM_Customer_Site_Address__c ),  ISCHANGED( SDM_nbn_install_date__c ),  ISCHANGED( SDM_NBN_Location_ID__c ),  ISCHANGED( SDM_NBN_Order_ID__c ),  ISCHANGED( SDM_Primary_Contact_Name__c ),  ISCHANGED( SDM_Primary_Contact_Number__c ),  ISCHANGED( SDM_Project_Code__c ),  ISCHANGED( SDM_RSP_Actual_Cutover_Date__c ),  ISCHANGED(  SDM_RSP_Site_ID__c ),  ISCHANGED(  RSP_Planned_Cutover_Date__c ),  ISCHANGED(  SDM_Secondary_Contact_Name__c ),  ISCHANGED(  SDM_Secondary_Contact_Number__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
