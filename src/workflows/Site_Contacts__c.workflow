<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Site_Contact_Unique</fullName>
        <field>Site_Contact_Unique__c</field>
        <formula>CASESAFEID( Related_Contact__c ) +&apos; &apos; + CASESAFEID( Related_Site__c)</formula>
        <name>Update Site Contact Unique</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>SiteCon - Update Site Contact Unique</fullName>
        <actions>
            <name>Update_Site_Contact_Unique</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Site_Contacts__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This WF rule will update the SiteContact Unique check field during sitecontact creation</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
