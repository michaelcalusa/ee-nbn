<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_email_to_LASE_Admin_informing_about_MDUCP_site_creation</fullName>
        <description>Send email to LASE Admin informing about MDUCP site creation</description>
        <protected>false</protected>
        <recipients>
            <recipient>altheasen@nbnco.com.au</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>brycemohr@nbnco.com.au</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jennaward@nbnco.com.au</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mariaabramova@nbnco.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Templates/Notification_to_LASE_Admin_on_creation_of_MDUCP_site_from_sharePoint</template>
    </alerts>
    <fieldUpdates>
        <fullName>FU_ToUnCheckIsSiteCreatedThroughCode</fullName>
        <field>isSiteCreationThroughCode__c</field>
        <literalValue>0</literalValue>
        <name>FU_ToUnCheckIsSiteCreatedThroughCode</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_UpdateSiteAddressField</fullName>
        <description>Update Unverified Site Address when Site Name is updated</description>
        <field>Site_Address__c</field>
        <formula>Name</formula>
        <name>FU_UpdateSiteAddressField</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_UpdateSiteNameAndAddress</fullName>
        <field>Site_Address__c</field>
        <formula>LEFT( 
TRIM(If(Lot_Number__c!=&apos;&apos;,&apos;LOT &apos;+Lot_Number__c,&apos;&apos;)+If(Unit_Number__c!=&apos;&apos;,if(Unit_Type_Code__c!=&apos;&apos;,&apos; &apos;+Unit_Type_Code__c+&apos; &apos;+Unit_Number__c, &apos; &apos;+Unit_Number__c),&apos;&apos;)+If(Level_Number__c!=&apos;&apos;,if(Level_Type_Code__c!=&apos;&apos;,&apos; &apos;+Level_Type_Code__c+&apos; &apos;+Level_Number__c, &apos; &apos;+Level_Number__c),&apos;&apos;)+If(Road_Number_1__c!=&apos;&apos;,&apos; &apos;+Road_Number_1__c,&apos;&apos;)+If(Road_Number_2__c!=&apos;&apos;, if(Road_Number_1__c!=&apos;&apos;,&apos;-&apos;+Road_Number_2__c, &apos; &apos;+Road_Number_2__c),&apos;&apos;)+If(Road_Name__c!=&apos;&apos;,&apos; &apos;+Road_Name__c,&apos;&apos;)+If(NOT(ISPICKVAL(Road_Suffix_Code__c,&apos;&apos;)),&apos; &apos;+TEXT(Road_Suffix_Code__c),&apos;&apos;)+If(Road_Type_Code__c!=&apos;&apos;,&apos; &apos;+Road_Type_Code__c,&apos;&apos;)+If(Locality_Name__c!=&apos;&apos;,&apos; &apos;+Locality_Name__c,&apos;&apos;)+If(Post_Code__c!=&apos;&apos;,&apos; &apos;+Post_Code__c,&apos;&apos;)+If(NOT(ISPICKVAL(State_Territory_Code__c,&apos;&apos;)),&apos; &apos;+TEXT(State_Territory_Code__c),&apos;&apos;)),
LEN(TRIM(If(Lot_Number__c!=&apos;&apos;,&apos;LOT &apos;+Lot_Number__c,&apos;&apos;)+If(Unit_Number__c!=&apos;&apos;,if(Unit_Type_Code__c!=&apos;&apos;,&apos; &apos;+Unit_Type_Code__c+&apos; &apos;+Unit_Number__c, &apos; &apos;+Unit_Number__c),&apos;&apos;)+If(Level_Number__c!=&apos;&apos;,if(Level_Type_Code__c!=&apos;&apos;,&apos; &apos;+Level_Type_Code__c+&apos; &apos;+Level_Number__c, &apos; &apos;+Level_Number__c),&apos;&apos;)+If(Road_Number_1__c!=&apos;&apos;,&apos; &apos;+Road_Number_1__c,&apos;&apos;)+If(Road_Number_2__c!=&apos;&apos;, if(Road_Number_1__c!=&apos;&apos;,&apos;-&apos;+Road_Number_2__c, &apos; &apos;+Road_Number_2__c),&apos;&apos;)+If(Road_Name__c!=&apos;&apos;,&apos; &apos;+Road_Name__c,&apos;&apos;)+If(NOT(ISPICKVAL(Road_Suffix_Code__c,&apos;&apos;)),&apos; &apos;+TEXT(Road_Suffix_Code__c),&apos;&apos;)+If(Road_Type_Code__c!=&apos;&apos;,&apos; &apos;+Road_Type_Code__c,&apos;&apos;)+If(Locality_Name__c!=&apos;&apos;,&apos; &apos;+Locality_Name__c,&apos;&apos;)+If(Post_Code__c!=&apos;&apos;,&apos; &apos;+Post_Code__c,&apos;&apos;)+If(NOT(ISPICKVAL(State_Territory_Code__c,&apos;&apos;)),&apos; &apos;+TEXT(State_Territory_Code__c),&apos;&apos;)))
)</formula>
        <name>FU_UpdateSiteNameAndAddress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_UpdateSiteNameField</fullName>
        <description>Update Unverified Site Address when Site Name is updated</description>
        <field>Name</field>
        <formula>Site_Address__c</formula>
        <name>FU_UpdateSiteNameField</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Location_Id_into_MDU_CP_On_Demand_Id</fullName>
        <field>MDU_CP_On_Demand_Id__c</field>
        <formula>Location_Id__c</formula>
        <name>Location Id into MDU/CP On Demand Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Make_Location_Id_Upper_Case</fullName>
        <field>Location_Id__c</field>
        <formula>UPPER(Location_Id__c)</formula>
        <name>Make Location Id Upper Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Rule to send email when MDUCP site is created from sharePoint</fullName>
        <actions>
            <name>Send_email_to_LASE_Admin_informing_about_MDUCP_site_creation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>NBN Interface User Profile</value>
        </criteriaItems>
        <criteriaItems>
            <field>Site__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>MDU/CP</value>
        </criteriaItems>
        <description>US#807: This workflow rule will send an email to LASE Admin when an MDUCP site is created from sharePoint. So that LASE Admin will be able to re-qualify the site.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Site - Copy Location Id into MDU%2FCP On Demand Id for Verified Records</fullName>
        <actions>
            <name>Location_Id_into_MDU_CP_On_Demand_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Site__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Verified</value>
        </criteriaItems>
        <description>DA 06/11 - this workflow will copy the Location Id into MDU/CP On Demand Id for Verified Records so that P2P can check the site records created in Salesforce</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Site - Make Location Id Upper Case</fullName>
        <actions>
            <name>Make_Location_Id_Upper_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow will make the Location Id upper case</description>
        <formula>NOT(ISBLANK(Location_Id__c)) &amp;&amp;  UPPER(Location_Id__c) &lt;&gt;Location_Id__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF_ToUnCheckIsSiteCreatedThroughCode</fullName>
        <actions>
            <name>FU_ToUnCheckIsSiteCreatedThroughCode</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Site__c.isSiteCreationThroughCode__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF_UpdateUnverifiedSite</fullName>
        <actions>
            <name>FU_UpdateSiteNameAndAddress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If there is any changes to Address fields on a Unverified site, this will update the Site address.</description>
        <formula>(RecordType.DeveloperName = &apos;Unverified&apos; || RecordType.DeveloperName = &apos;MDU_CP&apos;) &amp;&amp; ((ISNEW() &amp;&amp; (Lot_Number__c != &apos;&apos; || Unit_Type_Code__c != &apos;&apos; || Unit_Number__c != &apos;&apos; || Level_Type_Code__c != &apos;&apos; || Level_Number__c != &apos;&apos; || Road_Number_1__c != &apos;&apos; || Road_Number_2__c != &apos;&apos; || Road_Name__c != &apos;&apos; || !ISPICKVAL(Road_Suffix_Code__c, &apos;&apos;) || Road_Type_Code__c != &apos;&apos; || Locality_Name__c != &apos;&apos; || Post_Code__c != &apos;&apos; || !ISPICKVAL(State_Territory_Code__c, &apos;&apos;))) || (!ISNEW() &amp;&amp; (ISCHANGED(Lot_Number__c) || ISCHANGED(Unit_Type_Code__c) || ISCHANGED(Unit_Number__c) || ISCHANGED(Level_Type_Code__c) || ISCHANGED(Level_Number__c) || ISCHANGED(Road_Number_1__c) || ISCHANGED(Road_Number_2__c) || ISCHANGED(Road_Name__c) || ISCHANGED(Road_Suffix_Code__c) || ISCHANGED(Road_Type_Code__c) || ISCHANGED(Locality_Name__c) || ISCHANGED(Post_Code__c) || ISCHANGED(State_Territory_Code__c))))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF_UpdateUnverifiedSiteAddress</fullName>
        <actions>
            <name>FU_UpdateSiteAddressField</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If Unverified Site Name is modified then we need to update the Site Adress with Site Name</description>
        <formula>AND(  RecordType.DeveloperName = &apos;Unverified&apos;,  NOT(ISNEW()),  PRIORVALUE(Name)!= Name,  Site_Address__c!=&apos;&apos;  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF_UpdateUnverifiedSiteName</fullName>
        <actions>
            <name>FU_UpdateSiteNameField</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If Unverified Site Address is modified then we need to update the Site Name with Site Address</description>
        <formula>AND(OR(RecordType.DeveloperName = &apos;Unverified&apos;,RecordType.DeveloperName = &apos;MDU_CP&apos;), NOT(ISNEW()), PRIORVALUE(Site_Address__c)!= Site_Address__c, Name!=&apos;&apos;, Site_Address__c!=&apos;&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
