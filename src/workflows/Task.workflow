<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>MAR_Email_Reminder_for_Task</fullName>
        <description>MAR Email Reminder for Task</description>
        <protected>false</protected>
        <recipients>
            <field>Receive_Task_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>MAR_Email_Templates/MAR_Task_Reminder</template>
    </alerts>
    <alerts>
        <fullName>MAR_Task_Assignment_Email</fullName>
        <description>MAR Task Assignment Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>MAR_Email_Templates/MAR_Task_Assignment</template>
    </alerts>
    <alerts>
        <fullName>Send_Task_Notification_to_Team</fullName>
        <description>Send Task Notification to Team</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_Team_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Templates/Task_Assignment</template>
    </alerts>
    <alerts>
        <fullName>Send_task_reminder_email_24_hrs</fullName>
        <description>Send task reminder email - 24 hrs</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Customer_operations_templates/Task_reminder_email_24_hrs</template>
    </alerts>
    <alerts>
        <fullName>Send_task_reminder_email_48_hrs</fullName>
        <description>Send task reminder email - 48 hrs</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Customer_operations_templates/Task_reminder_email_48_hrs</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Receive_Task_Email_Field</fullName>
        <field>Receive_Task_Email__c</field>
        <formula>IF(ISBLANK( Team__c),
     Owner:User.Email,
       Team__r.Email__c 
  )</formula>
        <name>Update Receive Task Email Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>MAR Send Task Notification to Team</fullName>
        <actions>
            <name>MAR_Task_Assignment_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send notifications to MAR team when a task is assigned.</description>
        <formula>AND(   OR(ISNEW(),Team__c !=  PRIORVALUE(Team__c)) , (Owner:User.Profile.Name = &quot;NBN Migrations &amp; Disconnections&quot;), RecordType.DeveloperName = &apos;Task&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MAR Users%3A Send Task Reminder Email</fullName>
        <active>true</active>
        <description>Workflow rule for MAR M&amp;D to receive email notification on task creation and reminders</description>
        <formula>AND (or (Owner:User.Profile.Name = &quot;NBN Migrations &amp; Disconnections&quot;,Team__r.Name = &quot;M&amp;D Case Management Team&quot; ),RecordType.DeveloperName = &apos;Task&apos; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>MAR_Email_Reminder_for_Task</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Task.Due_Date__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Send Task Notification to Team</fullName>
        <actions>
            <name>Send_Task_Notification_to_Team</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Send notifications to relevant team when a task is assigned.</description>
        <formula>AND(!ISBLANK(Team__c),   OR(  ISNEW(),   Team__c !=  PRIORVALUE(Team__c)  ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Task Reminder Email</fullName>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3  AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>NBN Customer Delivery Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>NBN Operations Management</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.ActivityDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Outcome__c</field>
            <operation>notEqual</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>Workflow rule to send email to customer operation user for task reminder</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_task_reminder_email_24_hrs</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Task.ActivityDate</offsetFromField>
            <timeLength>-24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Send_task_reminder_email_48_hrs</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Task.ActivityDate</offsetFromField>
            <timeLength>-48</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Receive Task Email</fullName>
        <actions>
            <name>Update_Receive_Task_Email_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( ISCHANGED(OwnerId),      ISCHANGED(Team__c),      ISNEW()     )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
