<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Alert_to_Account_Owner</fullName>
        <description>Email Alert to Account Owner</description>
        <protected>false</protected>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Delivery Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Customer_Service_Templates/Contact_Matrix_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_ContactMatrix_FieldUpdate_Flag</fullName>
        <field>Is_This_Contact_Matrix_Field_Update__c</field>
        <literalValue>0</literalValue>
        <name>Update ContactMatrix FieldUpdate Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>WF_ContactMatrixNotification</fullName>
        <actions>
            <name>Email_Alert_to_Account_Owner</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_ContactMatrix_FieldUpdate_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Team_Member__c.Is_This_Contact_Matrix_Field_Update__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Team_Member__c.Updated_Fields_Info__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
