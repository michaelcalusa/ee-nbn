<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_MileStone_Completion_Date_as_Today</fullName>
        <field>Close_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set MileStone Completion Date as Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Milestone_as_Completed</fullName>
        <field>Status__c</field>
        <literalValue>Completed</literalValue>
        <name>Set Milestone as Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set MileStone Actual Completion Date if Status is Completed</fullName>
        <actions>
            <name>Set_MileStone_Completion_Date_as_Today</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Transition_Engagement_Milestones__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Transition_Engagement_Milestones__c.Close_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>if the Status of a Milestone is “Completed” and Actual Completion Date is not set than set today&apos;s date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Milestone Completed if Actual Completion Date is given</fullName>
        <actions>
            <name>Set_Milestone_as_Completed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Transition_Engagement_Milestones__c.Status__c</field>
            <operation>notEqual</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Transition_Engagement_Milestones__c.Close_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>If the Status of a Milestone Plan is not “Completed” and Actual Completion Date is give set the status as Completed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
