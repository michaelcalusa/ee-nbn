<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_notification_to_owner_that_Transition_has_been_assigned</fullName>
        <description>Email notification to owner that Transition has been assigned</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_To__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Business_Segment/Business_Segment_E_G_Transition_Assignment_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_Completion_Date_as_Today</fullName>
        <field>Engagement_Completed_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Completion Date as Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Milestone_Plan_as_Completed</fullName>
        <field>Status__c</field>
        <literalValue>Completed</literalValue>
        <name>Set Milestone Plan as Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>EnG Notify Transition Engagement Assigned User</fullName>
        <actions>
            <name>Email_notification_to_owner_that_Transition_has_been_assigned</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Send email to new owner that Transition has been assigned</description>
        <formula>OR( ISNEW(), ISCHANGED( Assigned_To__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set MP Actual Completion Date if Status is Completed</fullName>
        <actions>
            <name>Set_Completion_Date_as_Today</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Transition_Engagement__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Transition_Engagement__c.Engagement_Completed_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>If the Status of a Milestone Plan is “Completed” and Actual Completion Date is not set than set today&apos;s date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set MP Completed if Actual Completion Date is given</fullName>
        <actions>
            <name>Set_Milestone_Plan_as_Completed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Transition_Engagement__c.Status__c</field>
            <operation>notEqual</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Transition_Engagement__c.Engagement_Completed_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>If the Status of a Milestone Plan is not “Completed” and Actual Completion Date is give set the status as Completed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
