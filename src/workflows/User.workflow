<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CCP_to_PC_Migration_Email_Alert</fullName>
        <description>CCP to PC Migration Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Customer_to_Partner_community_migration/Communities_ICT_Migrate_from_CCP_to_PC_Email</template>
    </alerts>
    <alerts>
        <fullName>ICT_User_Creation_Confirmation_Email</fullName>
        <description>ICT User Creation Confirmation Email</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@nbnco.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Business_Segment/ICT_User_Creation_Confirmation_Email</template>
    </alerts>
    <fieldUpdates>
        <fullName>Activation_Date</fullName>
        <field>CFS_Activation_Date__c</field>
        <formula>Today()</formula>
        <name>Activation Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CFS Activation Date</fullName>
        <actions>
            <name>Activation_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.lmscons__CFS_Status__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ICT Confirmation Email when Community User Created</fullName>
        <actions>
            <name>ICT_User_Creation_Confirmation_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>ICT Customer Community Plus User</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.IsPortalEnabled</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>ICT Confirmation Email when Community User Created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
